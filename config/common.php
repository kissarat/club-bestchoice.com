<?php

if (empty($_COOKIE['lang'])) {
    $language = empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? '' : $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    $language = preg_match('/(ru|uk|be|ky|ab|mo|et|lv)/i', $language) ? 'ru' : 'en';
}
else {
    $language = $_COOKIE['lang'];
}

$admin = isset($_SERVER['HTTP_HOST']) && false !== strpos($_SERVER['HTTP_HOST'], 'admin');

$config = [
    'id' => 'extravaganza.systems',
    'name' => 'extravaganza.systems',
    #'timeZone' => 'Europe/Moscow',
    'basePath' => __DIR__ . '/..',
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/' . ($admin ? 'statistics' : 'index'),
    'language' => $language,
    'charset' => 'utf-8',
    'layout' => $admin ? 'admin' : 'main',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\ApcCache',
            'keyPrefix' => 'evg'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'enabled' => false
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<name:(fast\-start|energy|super\-energy)>/<user:[\w_\-\.]+>' => 'matrix/matrix/index',
                'plans/<user:[\w_\-\.]+>' => 'matrix/matrix/plans',
                'open/<id:\w+>' => 'matrix/matrix/open',

                'register/<ref_name:[\w_\-\.]+>' => 'user/signup',
                'journal/user/<user:[\w_\-\.]+>' => 'journal/index',
                'feedback/template/<template:\w+>' => 'feedback/create',
                '<scenario:(withdraw|payment)>/user/<user:[\w_\-\.]+>' => 'invoice/invoice/index',
                '<scenario:(withdraw|payment)>/<amount:[\d\.]+>' => 'invoice/invoice/create',
                '<scenario:(withdraw|payment)>' => 'invoice/invoice/index',
                'invoices/user/<user:[\w_\-\.]+>' => 'invoice/invoice/index',
                'invoice/<id:[\w\-]+>' => 'invoice/invoice/view',
                'transfers/user/<user:[\w_\-\.]+>' => 'invoice/transfer/index',
                'transfers' => 'invoice/transfer/index',
                'pay/<id:[\w\-]+>' => 'invoice/invoice/pay',
                'success/<id:[\w\-]+>' => 'invoice/invoice/success',
                'fail/<id:[\w\-]+>' => 'invoice/invoice/fail',
                'journal/<id:\d+>' => 'journal/view',
                'password/<name:[\w_\-\.]+>' => 'user/password',
                'reset/<code:[\w_\-]{64}>' => 'user/password',
                'inform/<user:[\w_\-\.]+>' => 'message/create',
                'dialog/<user:[\w_\-\.]+>' => 'message/dialog',
                'page/<name:.*>' => 'article/article/page',
                'compose/<scenario:(page|default)>' => 'article/article/create',
                'edit/<id:\d+>' => 'article/article/update',
                'visits/<user:[\w_\-\.]+>' => 'admin/visit',
                'settings/<category:[\w_\-\.]+>' => 'setting/edit',
                'profile/<name:[\w_\-\.]+>' => 'user/view',
                'referral/<ref_name:[\w_\-\.]+>' => 'user/index',
                'ticket/<ticket:\w+>' => 'feedback/feedback/ticket',
                'tickets' => 'feedback/feedback/index',
                'answers' => 'faq/faq/index',
                'marketing' => 'matrix/matrix/marketing',
                'settings' => 'setting/index',
                'reviews' => 'review/index',
                'compose' => 'article/article/create',
                'pages' => 'article/article/pages',
                'news' => 'article/article/index',
                'login' => 'user/login',
                'register' => 'user/signup',
                'cabinet' => 'user/view',
                'payments' => 'invoice/invoice/index',
                'translations' => 'lang/lang/index',
                'profit' => 'user/account',
                'contact-us' => 'feedback/feedback/create',
                'statistics' => 'home/statistics',
                'visits' => 'admin/visit',
                '/' => 'home/index',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'enableCaching' => true
                ]
            ]
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'name' => 'auth'
        ],
        'backup' => [
            'class' => 'app\components\Backup',
        ],
    ],
    'params' => null,
];
