CREATE OR REPLACE FUNCTION recount(_id INT) RETURNS SETOF INT AS $$
DECLARE
  _transfer_id INT;
  _node RECORD;
  _referral RECORD;
  _sender_name VARCHAR(24);
  _node_id INT;
  _type INT;
  _event VARCHAR(16);
BEGIN
  SELECT * FROM matrix_node WHERE id = _id INTO _node;
  FOR _referral IN SELECT * FROM referral WHERE root_id = _id AND level > 0 LOOP
    INSERT INTO journal ("type", "event", "user_name", object_id, ip, "time")
    VALUES ('account', '+', _node.user_name, _referral.id, null, _node.time) RETURNING id INTO _transfer_id;
    INSERT INTO transfer VALUES (_transfer_id, _referral.user_name, _referral.interest, null, _node.id);
    RETURN NEXT _transfer_id;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION recount_blocked(_id INT) RETURNS SETOF INT AS $$
DECLARE
  _income RECORD;
  _transfer_id INT;
  _sender_name VARCHAR(24);
  _object_id INT;
  _event VARCHAR(16);
  _amount DECIMAL(8,2);
BEGIN
  FOR _income IN SELECT * FROM transfer_income WHERE object_id = _id::VARCHAR(40) ORDER BY "level", pos LOOP
    IF (_income.level = 3 AND _income.pos < 53) OR (_income.level = 4 AND _income.pos < 50) THEN
      UPDATE journal SET "type" = 'blocked' WHERE id = _income.id;
      RETURN NEXT _income.id;
    END IF;

    IF (_income.level = 3 AND _income.pos = 3) THEN
      _sender_name = _income.receiver_name;
      _event = '-';
      _amount = 100;
      SELECT id FROM matrix_node WHERE type_id = 2 AND reinvest_from = _id INTO _object_id;
    END IF;

    IF (_income.level = 3 AND _income.pos = 32) THEN
      _sender_name = _income.receiver_name;
      _event = '-';
      _amount = 1000;
      SELECT id FROM matrix_node WHERE type_id = 3 AND reinvest_from = _id INTO _object_id;
    END IF;

    IF (_income.level = 4 AND _income.pos = 49) THEN
      _sender_name = _income.receiver_name;
      _event = 'fix';
      _amount = 1000;
    END IF;

    IF _sender_name IS NOT NULL THEN
      INSERT INTO journal ("type", "event", "user_name", object_id, ip, "time")
      VALUES ('blocked', _event, _sender_name, _object_id, null, _income.time) RETURNING id INTO _transfer_id;
      INSERT INTO transfer VALUES (_transfer_id, _sender_name, _amount, null, _income.node_id);
      RETURN NEXT _income.id;

      IF _event = '-' AND _amount = 1000 THEN
        INSERT INTO journal("type", "event", "user_name", object_id, ip, "time")
        VALUES ('account', 'remainder', _sender_name, _id, null, _income.time) RETURNING id INTO _transfer_id;
        INSERT INTO transfer VALUES (_transfer_id, _sender_name, 20, null, _income.node_id);
        RETURN NEXT _income.id;
      END IF;
    END IF;

    _sender_name := null;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION replace(_id INT, _parent_id INT) RETURNS SETOF INT AS $$
  BEGIN
    UPDATE matrix_node SET parent_id = _parent_id WHERE id = _id;
    DELETE FROM journal WHERE id in (SELECT id FROM transfer WHERE node_id = _id);
    PERFORM recount(_id);
    PERFORM recount_blocked(_id);
  END;
$$ LANGUAGE plpgsql;
