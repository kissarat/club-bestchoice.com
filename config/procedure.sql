CREATE OR REPLACE FUNCTION transfer(
  _sender_name   VARCHAR(24),
  _receiver_name VARCHAR(24),
  _amount        DECIMAL(8, 2),
  memo           TEXT DEFAULT NULL,
  ip             INET DEFAULT NULL,
  node_id        INT DEFAULT NULL,
  debit          BOOL DEFAULT TRUE,
  _type          VARCHAR(16) DEFAULT 'account',
  _event         VARCHAR(16) DEFAULT '+',
  _object_id     INT DEFAULT NULL
)
  RETURNS INT AS $$
DECLARE
  _id     INT;
  _remain DECIMAL(8, 2);
BEGIN
  IF _amount < 0
  THEN
    _event = '-';
    _amount = -_amount;
  END IF;
  INSERT INTO journal ("type", "event", "user_name", object_id, ip)
  VALUES (_type, _event, _sender_name, _object_id, ip)
  RETURNING id
    INTO _id;
  INSERT INTO transfer VALUES (_id, _receiver_name, _amount, memo, node_id);

  IF debit
  THEN
    IF 'account' = _type
    THEN
      UPDATE "user"
      SET account = account - _amount
      WHERE "name" = _sender_name;
    END IF;
    IF 'blocked' = _type
    THEN
      UPDATE "user"
      SET blocked = blocked - _amount
      WHERE "name" = _sender_name;
    END IF;
  END IF;
  IF 'account' = _type
  THEN
    UPDATE "user"
    SET account = account + _amount
    WHERE "name" = _receiver_name;
  END IF;
  IF 'blocked' = _type
  THEN
    UPDATE "user"
    SET blocked = blocked + _amount
    WHERE "name" = _receiver_name;
  END IF;
  RETURN _id;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE RULE income_insert AS ON INSERT TO transfer_journal DO INSTEAD (
  INSERT INTO journal ("type", "event", "user_name", object_id, ip)
  VALUES (NEW.type, NEW.event, NEW.sender_name, NEW.object_id, NEW.ip);
  INSERT INTO transfer VALUES (currval('journal_id_seq'), NEW.receiver_name, NEW.amount,
                               NEW.memo, NEW.node_id);
);


CREATE OR REPLACE FUNCTION enter(
  _user_name     VARCHAR(24),
  tid            SMALLINT,
  _reinvest_from INT,
  _parent_id     INT,
  ip             INET)
  RETURNS INT AS $$
DECLARE
  _receiver_name  VARCHAR(24);
  _id             INT;
  _user_id        INT;
  _root_id        INT;
  _target_id      INT;
  _number         INT;
  _level          INT;
  _max_level      INT;
  _level_referral INT;
  _amount         DECIMAL(8, 2);
  _count          INT;
  _memo           TEXT;
  _type           VARCHAR(16);
  _bool           BOOLEAN;
  _type_2         BOOL;
  _type_3         BOOL;
BEGIN
  SELECT id
  FROM matrix_node
  WHERE reinvest_from = _reinvest_from AND type_id = tid
  INTO _id;
  IF _id IS NOT NULL
  THEN
    RETURN _id;
  END IF;

  SELECT count(*)
  FROM matrix_node
  WHERE user_name = _user_name AND type_id = tid
  INTO _count;
  INSERT INTO matrix_node (user_name, type_id, parent_id, reinvest_from, number)
  VALUES (_user_name, tid, _parent_id, _reinvest_from, _count)
  RETURNING id
    INTO _id;

  SELECT price
  FROM matrix_type
  WHERE id = tid
  INTO _amount;
  IF _reinvest_from IS NOT NULL
  THEN
    _type = 'blocked';
    UPDATE "user"
    SET blocked = blocked - _amount
    WHERE "name" = _user_name;
  ELSE
    _type = 'account';
    UPDATE "user"
    SET account = account - _amount
    WHERE "name" = _user_name;
  END IF;
  INSERT INTO transfer_journal VALUES (_amount, _type, '-', _user_name, _user_name, _id, _reinvest_from);

  SELECT max(level)
  FROM matrix_level
  WHERE type_id = tid
  INTO _max_level;

  _type_2 := EXISTS(SELECT *
                    FROM matrix_node
                    WHERE type_id = 2 AND user_name = _user_name);
  _type_3 := EXISTS(SELECT *
                    FROM matrix_node
                    WHERE type_id = 3 AND user_name = _user_name);

  FOR i IN 1.._max_level LOOP
    SELECT
      number,
      root_id,
      level
    FROM matrix
    WHERE id = _id AND level = i
    ORDER BY level DESC
    INTO _number, _root_id, _level;
    SELECT count(*)
    FROM matrix
    WHERE root_id = _root_id AND level = _level
    INTO _count;
    SELECT
      user_name,
      interest,
      level
    FROM referral
    WHERE
      level = i AND root_id = _id
    INTO _receiver_name, _amount, _level_referral;

    IF _amount > 0
    THEN
      SELECT to_json(t)
      FROM (SELECT
              _count   AS count,
              _level   AS level,
              _root_id AS id,
              _number  AS number) t
      INTO _memo;
      _type := 'account';
      IF _level = 3 AND (1 = tid AND (_count <= 3 OR (_count <= 16 AND NOT _type_2))
                         OR 2 = tid AND (_count <= 3 OR (_count <= 16 AND NOT _type_3)))
      THEN
        _type := 'blocked';
      END IF;
      SELECT transfer(_user_name,
                      _receiver_name,
                      _amount,
                      _memo,
                      ip,
                      _id,
                      FALSE,
                      _type,
                      '+' :: VARCHAR(16),
                      _root_id)
      INTO _target_id;
    END IF;

    IF tid = 1 AND _level = 3 AND _count = 3
    THEN
      INSERT INTO reinvest (user_name, type_id, reinvest_from, node_id)
      VALUES (_receiver_name, 1 :: SMALLINT, _root_id, _id);
    END IF; -- tid = 1

    IF tid = 2 AND _level = 3 AND _count = 3
    THEN
      INSERT INTO reinvest (user_name, type_id, reinvest_from, node_id)
      VALUES (_receiver_name, 2 :: SMALLINT, _root_id, _id);
    END IF; -- tid = 2
  END LOOP;

  RETURN _id;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION cancel(_id INT, charge BOOL DEFAULT TRUE)
  RETURNS VOID AS $$
DECLARE
  _sender_name   VARCHAR(24);
  _receiver_name VARCHAR(24);
  _amount        DECIMAL(8, 2);
BEGIN
  SELECT
    sender_name,
    receiver_name,
    amount
  FROM transfer_journal
  WHERE id = _id
  INTO _sender_name, _receiver_name, _amount;
  UPDATE "user"
  SET account = account - _amount
  WHERE "name" = _receiver_name;
  IF charge
  THEN
    UPDATE "user"
    SET account = account + _amount
    WHERE "name" = _sender_name;
  END IF;
  DELETE FROM transfer
  WHERE id = _id;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION visit(
  _ip       INET,
  _agent    VARCHAR(200),
  "path"    VARCHAR(80),
  spend     SMALLINT,
  "data"    TEXT,
  user_name VARCHAR(24) DEFAULT NULL)
  RETURNS INT AS $$
DECLARE
  _id INT;
BEGIN
  SELECT id
  FROM visit_agent
  WHERE ip = _ip AND agent = _agent
  INTO _id;
  IF _id IS NULL
  THEN
    INSERT INTO visit_agent (ip, agent) VALUES (_ip, _agent)
    RETURNING id
      INTO _id;
  END IF;
  INSERT INTO visit_path (agent_id, "path", spend, "data") VALUES (_id, "path", spend, "data")
  RETURNING id
    INTO _id;
  IF user_name IS NOT NULL
  THEN
    UPDATE "user"
    SET "last_access" = CURRENT_TIMESTAMP
    WHERE "name" = user_name;
  END IF;
  RETURN _id;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION descend(_root_id INT)
  RETURNS INT AS $$
DECLARE
  _count         INT;
  _level         INT;
  _matrix_degree BIGINT;
  _id            INT;
  _type          RECORD;
BEGIN
  SELECT t.*
  FROM matrix_node n JOIN matrix_type t ON n.type_id = t.id
  WHERE n.id = _root_id
  INTO _type;
  SELECT min(level)
  FROM degree
  WHERE root_id = _root_id
  INTO _level;
  IF _level IS NOT NULL
  THEN
    SELECT min(id)
    FROM
      (
        SELECT id
        FROM matrix
        WHERE root_id = _root_id AND level = _level - 1
        EXCEPT
        SELECT parent_id
        FROM matrix
        WHERE root_id = _root_id AND level = _level
        GROUP BY parent_id
        HAVING count(*) >= _type.degree
      ) m
    INTO _id;
  ELSE
    SELECT max(level)
    FROM matrix
    WHERE root_id = _root_id
    INTO _level;
    SELECT min(id)
    FROM matrix
    WHERE root_id = _root_id AND level = _level
    INTO _id;
  END IF;
  RETURN _id;
END;
$$ LANGUAGE plpgsql STABLE;
