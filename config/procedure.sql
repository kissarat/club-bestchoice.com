START TRANSACTION;

CREATE OR REPLACE FUNCTION transfer(
  sender_name VARCHAR(24),
  receiver_name VARCHAR(24),
  amount DECIMAL(8,2),
  memo TEXT,
  ip INET,
  node_id INT,
  debit BOOL DEFAULT TRUE)
  RETURNS INT AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO journal ("type", "event", "user_name", ip) VALUES ('transfer', 'do', sender_name, ip) RETURNING id INTO _id;
  INSERT INTO transfer VALUES (_id, receiver_name, amount, memo, node_id);
  IF debit THEN
    UPDATE "user" SET account = account - amount WHERE "name" = sender_name;
  END IF;
  UPDATE "user" SET account = account + amount WHERE "name" = receiver_name;
  RETURN _id;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION cancel(_id INT, charge BOOL DEFAULT TRUE)
  RETURNS VOID AS $$
DECLARE
  _sender_name VARCHAR(24);
  _receiver_name VARCHAR(24);
  _amount DECIMAL(8,2);
BEGIN
  SELECT sender_name, receiver_name, amount FROM transfer_journal WHERE id = _id
  INTO _sender_name, _receiver_name, _amount;
  UPDATE "user" SET account = account - _amount WHERE "name" = _receiver_name;
  IF charge THEN
    UPDATE "user" SET account = account + _amount WHERE "name" = _sender_name;
  END IF;
  DELETE FROM transfer WHERE id = _id;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION enter(sponsor VARCHAR(24), tid SMALLINT, ip INET) RETURNS INT AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO matrix_node(user_name, type_id) VALUES (sponsor, tid) RETURNING id INTO _id;
  CREATE TEMPORARY TABLE _transfer AS SELECT user_name, interest FROM referral
  WHERE root = sponsor AND type_id = tid;
  PERFORM transfer(sponsor, user_name, interest::DECIMAL(8,2), NULL, ip, _id, false) FROM _transfer;
  DROP TABLE _transfer;
  RETURN _id;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION exit(_id INT) RETURNS INT AS $$
DECLARE
  number INT;
BEGIN
  CREATE TEMPORARY TABLE _transfer AS SELECT id, receiver_name, amount FROM transfer WHERE node_id = _id;
  PERFORM cancel(id, false) FROM _transfer;
  DELETE FROM matrix_node WHERE id = _id;
  SELECT count(*) FROM _transfer INTO number;
  DROP TABLE _transfer;
  RETURN number;
END
$$ LANGUAGE plpgsql;

COMMIT;
