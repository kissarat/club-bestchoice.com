/* Yii English source translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "source_message" (
  id       SERIAL PRIMARY KEY,
  category VARCHAR(32) DEFAULT 'app',
  message  VARCHAR(256)
);
CREATE UNIQUE INDEX message_id ON "source_message" USING BTREE ("id");


/* Yii Russian target translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "message" (
  "id"          INT,
  "language"    VARCHAR(16) DEFAULT 'ru',
  "translation" VARCHAR(256),
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
);


/* English-Russian translation view */
CREATE VIEW "translation" AS
  SELECT
    s.id,
    message,
    translation
  FROM source_message s JOIN message t ON s.id = t.id
  WHERE "language" = 'ru';


/* User account, see app\models\User */
CREATE TABLE "user" (
  id             SERIAL PRIMARY KEY NOT NULL,
  /* login/nickname */
  name           VARCHAR(24)        NOT NULL UNIQUE,
  account        DECIMAL(8, 2)      NOT NULL DEFAULT '0.00',
  email          VARCHAR(48)        NOT NULL,
  /* hash of password */
  hash           CHAR(60),
  /* cookie authorization token */
  auth           CHAR(64) UNIQUE,
  /* used for email registration and password recovery */
  code           CHAR(64),
  /* user privileges */
  status         SMALLINT           NOT NULL DEFAULT 2,
  /* last activity of user */
  last_access    TIMESTAMP                   DEFAULT CURRENT_TIMESTAMP,
  /* php-serialized data  */
  data           BYTEA,
  /* name of referral user */
  ref_name       VARCHAR(24),
  /* Perfect Money wallet */
  wallet_perfect VARCHAR(9),
  /* NixMoney wallet */
  wallet_nix     VARCHAR(64),
  /* Bank card number */
  wallet_card    VARCHAR(64),
  forename       VARCHAR(24),
  surname        VARCHAR(24),
  city           VARCHAR(24),
  phone          VARCHAR(24),
  skype          VARCHAR(24)
);
CREATE UNIQUE INDEX user_id ON "user" USING BTREE ("id");
CREATE UNIQUE INDEX user_name ON "user" USING BTREE ("name");

INSERT INTO "user" (name, email, status) VALUES ('admin', 'lab_tas@ukr.net', 1);


/* User action log, see app\models\Record */
CREATE TABLE "journal" (
  id        SERIAL PRIMARY KEY NOT NULL,
  type      VARCHAR(16)        NOT NULL,
  event     VARCHAR(16)        NOT NULL,
  object_id VARCHAR(40),
  data      TEXT,
  user_name VARCHAR(24),
  time      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip        INET,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE transfer (
  id            INT           NOT NULL,
  receiver_name VARCHAR(24)   NOT NULL,
  amount        DECIMAL(8, 2) NOT NULL,
  memo          TEXT,
  node_id       INT,
  CONSTRAINT transfer_id FOREIGN KEY (id)
  REFERENCES "journal" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_receiver FOREIGN KEY (receiver_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_amount CHECK (amount > 0),
  CONSTRAINT transfer_memo CHECK (length(memo) >= 1 AND length(memo) < 600)
);


CREATE VIEW transfer_journal AS
  SELECT
    j.id,
    j.user_name AS sender_name,
    receiver_name,
    amount,
    memo,
    "time",
    ip,
    node_id
  FROM journal j JOIN transfer t ON j.id = t.id;


CREATE TYPE invoice_type AS ENUM ('manual', 'perfect', 'nix', 'card');

/* Payment (if amount > 0) and withdrawal (if amount < 0) table, see app\modules\invoice\models\Invoice */
CREATE TABLE "invoice" (
  id        UUID PRIMARY KEY NOT NULL,
  user_name VARCHAR(24)      NOT NULL,
  amount    DECIMAL(8, 2)    NOT NULL,
  "type"    invoice_type     NOT NULL,
  batch     BIGINT,
  status    VARCHAR(16) DEFAULT 'create',
  number    SERIAL           NOT NULL,
  wallet    VARCHAR(40),
  "time"    BIGINT,
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0),
  CONSTRAINT "time" CHECK ("time" > 0)
);
CREATE UNIQUE INDEX invoice_id ON "invoice" USING BTREE ("id");


CREATE VIEW invoice_sum AS
  SELECT
    user_name,
    payment,
    - withdraw as withdraw,
    payment + withdraw AS balance
  FROM
    (
      SELECT
        user_name,
        coalesce((SELECT sum(amount)
                  FROM invoice p
                  WHERE p.user_name = i.user_name AND p.amount > 0), 0) AS payment,
        coalesce((SELECT sum(amount)
                  FROM invoice p
                  WHERE p.user_name = i.user_name AND p.amount < 0), 0) AS withdraw
      FROM invoice i
      GROUP BY user_name
    ) s;

CREATE TABLE "matrix_type" (
  id      SMALLINT PRIMARY KEY,
  price   INT  NOT NULL,
  name    VARCHAR(24),
  enabled BOOL NOT NULL
);

INSERT INTO "matrix_type" VALUES (1, 10, 'Fast Start', TRUE);
INSERT INTO "matrix_type" VALUES (2, 100, 'Energy', TRUE);
INSERT INTO "matrix_type" VALUES (3, 1000, 'Super Energy', FALSE);


CREATE TABLE "matrix_level" (
  id      SMALLINT PRIMARY KEY,
  "range" INT8RANGE NOT NULL,
  income  FLOAT     NOT NULL
);

INSERT INTO "matrix_level" VALUES (1, '[0, 4]' :: INT8RANGE, 0.05);
INSERT INTO "matrix_level" VALUES (2, '[5, 20]' :: INT8RANGE, 0.1);
INSERT INTO "matrix_level" VALUES (3, '[21, 84]' :: INT8RANGE, 0.35);
INSERT INTO "matrix_level" VALUES (4, '[85, 340]' :: INT8RANGE, 0.2);
INSERT INTO "matrix_level" VALUES (5, '[341, 1364]' :: INT8RANGE, 0.2);


CREATE TABLE "matrix_node" (
  id        SERIAL PRIMARY KEY,
  type_id   SMALLINT                            NOT NULL,
  user_name VARCHAR(24)                         NOT NULL,
  "time"    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);


CREATE VIEW matrix AS
  WITH RECURSIVE r(id, type_id, user_name, ref_name, "level", user_id, root, "time") AS (
    SELECT
      n.id,
      type_id,
      user_name,
      ref_name,
      0         AS "level",
      u.id      AS user_id,
      user_name AS root,
      "time"
    FROM matrix_node n
      JOIN "user" u ON u.name = n.user_name
    UNION
    SELECT
      n.id,
      n.type_id,
      n.user_name,
      u.ref_name,
      r."level" + 1 AS "level",
      u.id          AS user_id,
      r.root,
      n."time"
    FROM matrix_node n
      JOIN "user" u ON n.user_name = u.name
      JOIN r ON r.user_name = u.ref_name
  )
  SELECT
    r.id,
    row_number()
    OVER (PARTITION BY root, type_id
      ORDER BY user_id) AS "number",
    type_id,
    user_name,
    ref_name,
    "level",
    user_id,
    root,
    price * income      AS interest,
    "time"
  FROM r
    JOIN matrix_type t ON r.type_id = t.id
    JOIN matrix_level l ON r."level" = l.id
  WHERE "level" > 0;


CREATE VIEW referral AS
  WITH RECURSIVE r(id, type_id, user_name, ref_name, "level", user_id, root, "time") AS (
    SELECT
      n.id,
      type_id,
      user_name,
      ref_name,
      0         AS "level",
      u.id      AS user_id,
      user_name AS root,
      "time"
    FROM matrix_node n
      JOIN "user" u ON u.name = n.user_name
    UNION
    SELECT
      n.id,
      n.type_id,
      n.user_name,
      u.ref_name,
      r."level" + 1 AS "level",
      u.id          AS user_id,
      r.root,
      n."time"
    FROM matrix_node n
      JOIN "user" u ON n.user_name = u.name
      JOIN r ON r.ref_name = u.name
  )
  SELECT
    r.id,
    row_number()
    OVER (PARTITION BY root, type_id
      ORDER BY user_id) AS "number",
    type_id,
    user_name,
    ref_name,
    "level",
    user_id,
    root,
    price * income      AS interest,
    "time"
  FROM r
    JOIN matrix_type t ON r.type_id = t.id
    JOIN matrix_level l ON r."level" = l.id
  WHERE "level" > 0;

/* User and guest feedback table for app\modules\feedback\models\Feedback */
CREATE TABLE "feedback" (
  id      SERIAL PRIMARY KEY NOT NULL,
  ticket  CHAR(24) UNIQUE    NOT NULL,
  email   VARCHAR(48),
  subject VARCHAR(256)       NOT NULL,
  open    BOOL               NOT NULL DEFAULT TRUE
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING BTREE ("id");
CREATE UNIQUE INDEX feedback_ticket ON "feedback" USING BTREE ("ticket");

CREATE TABLE "feedback_message" (
  id          SERIAL PRIMARY KEY NOT NULL,
  feedback_id INT                NOT NULL,
  user_name   VARCHAR(24),
  answer      BOOL               NOT NULL DEFAULT FALSE,
  "content"   TEXT               NOT NULL,
  "time"      TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip          INET,
  CONSTRAINT feedback_user FOREIGN KEY (user_name)
  REFERENCES "user" ("name") ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE "article" (
  "id"       SERIAL PRIMARY KEY,
  "name"     VARCHAR(24),
  "title"    VARCHAR(256) NOT NULL,
  "keywords" VARCHAR(192),
  "summary"  TEXT,
  "content"  TEXT         NOT NULL
);
CREATE UNIQUE INDEX article_id ON "article" USING BTREE ("id");
INSERT INTO article (name, title, content) VALUES
  ('main', 'Best Choice', ''),
  ('about', 'О нас', ''),
  ('marketing', 'Маркетинг', ''),
  ('rules', 'Правила', ''),
  ('contacts', 'О нас', '');


CREATE TABLE "review" (
  id        SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24)        NOT NULL,
  content   TEXT               NOT NULL,
  CONSTRAINT review_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX review_id ON "review" USING BTREE ("id");


CREATE TABLE "faq" (
  "id"       SERIAL PRIMARY KEY NOT NULL,
  "number"   SMALLINT,
  "question" VARCHAR(256)       NOT NULL,
  "answer"   TEXT               NOT NULL
);


CREATE TABLE "setting" (
  "category" VARCHAR(24) NOT NULL,
  "name"     VARCHAR(24) NOT NULL,
  "type"     VARCHAR(24),
  "value"    TEXT
);

INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'email', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'login_fails', 5);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'skype', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'google', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'yandex', '33309878');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'heart', NULL);
INSERT INTO "setting" VALUES ('common', 'withdrawal_confirmation', 'bool', TRUE);
INSERT INTO "setting" VALUES ('common', 'perfect', 'bool', TRUE);
INSERT INTO "setting" VALUES ('common', 'nix', 'bool', TRUE);
INSERT INTO "setting" VALUES ('common', 'card', 'bool', TRUE);

INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'id', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'password', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'wallet', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'secret', NULL);

INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'server', 'http://dev.nixmoney.com/');
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'id', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'password', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'wallet', NULL);

/* see web/visit.php */
/* Visitors list */
CREATE TABLE "visit_agent" (
  "id"    SERIAL PRIMARY KEY,
  "agent" VARCHAR(200),
  "ip"    INET
);
CREATE INDEX visit_agent_agent ON "visit_agent" USING BTREE ("agent");
CREATE INDEX visit_agent_ip ON "visit_agent" USING BTREE ("ip");


/* Visitors log */
CREATE TABLE "visit_path" (
  "id"       SERIAL PRIMARY KEY,
  "agent_id" INT         NOT NULL,
  "path"     VARCHAR(80) NOT NULL,
  "spend"    SMALLINT,
  "time"     TIMESTAMP DEFAULT current_timestamp,
  "data"     TEXT,
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES "visit_agent" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE
);


/* Visitors with user agent log view */
CREATE VIEW "visit" AS
  SELECT
             p.id AS id,
    agent_id,
    spend,
    (
      SELECT user_name
      FROM journal
      WHERE ip = a.ip
      ORDER BY id DESC
      LIMIT 1
    ),
    "path",
    p."time",
    a.ip,
    agent
  FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id;
