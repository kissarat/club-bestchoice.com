/* Yii English source translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "source_message" (
  id       SERIAL PRIMARY KEY,
  category VARCHAR(32) DEFAULT 'app',
  message  VARCHAR(256)
);
CREATE UNIQUE INDEX message_id ON "source_message" USING BTREE ("id");


/* Yii Russian target translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "message" (
  "id"          INT,
  "language"    VARCHAR(16) DEFAULT 'ru',
  "translation" VARCHAR(256),
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
);


/* User account, see app\models\User */
CREATE TABLE "user" (
  id             SERIAL PRIMARY KEY NOT NULL,
  /* login/nickname */
  name           VARCHAR(24) NOT NULL,
  account        DECIMAL(8, 2) NOT NULL DEFAULT '0.00',
  blocked        DECIMAL(8, 2) NOT NULL DEFAULT '0.00',
  email          VARCHAR(48) NOT NULL,
  /* hash of password */
  hash           CHAR(60),
  /* cookie authorization token */
  auth           CHAR(64),
  /* used for email registration and password recovery */
  code           CHAR(64),
  /* user privileges */
  status         SMALLINT NOT NULL DEFAULT 2,
  /* last activity of user */
  last_access    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  /* php-serialized data  */
  data           BYTEA,
  /* Perfect Money wallet */
  wallet_perfect VARCHAR(9),
  /* NixMoney wallet */
  wallet_nix     VARCHAR(64),
  /* Bank card number */
  wallet_advcash VARCHAR(64),
  forename       VARCHAR(24),
  surname        VARCHAR(24),
  city           VARCHAR(24),
  phone          VARCHAR(24),
  skype          VARCHAR(24),
  pin CHAR(4),
  avatar VARCHAR(24),
  /* name of referral user */
  ref_name       VARCHAR(24),
  ref_number    INT DEFAULT 0,
  CONSTRAINT unique_user_id UNIQUE ("id"),
  CONSTRAINT unique_user_name UNIQUE ("name"),
  CONSTRAINT unique_user_auth UNIQUE ("auth"),
  CONSTRAINT unique_user_code UNIQUE ("code")
);
CREATE UNIQUE INDEX user_id ON "user" USING BTREE ("id");
CREATE UNIQUE INDEX user_name ON "user" USING BTREE ("name");
CREATE UNIQUE INDEX auth ON "user" USING BTREE (auth);
CREATE INDEX user_email ON "user" USING BTREE (email);
CREATE INDEX ref_name ON "user" USING BTREE (ref_name);



/* User action log, see app\models\Record */
CREATE TABLE "journal" (
  id        SERIAL PRIMARY KEY NOT NULL,
  type      VARCHAR(16)        NOT NULL,
  event     VARCHAR(16)        NOT NULL,
  object_id VARCHAR(40),
  data      TEXT,
  user_name VARCHAR(24),
  time      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip        INET,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TYPE invoice_type AS ENUM ('manual', 'perfect', 'nix', 'advcash');


/* Payment (if amount > 0) and withdrawal (if amount < 0) table, see app\modules\invoice\models\Invoice */
CREATE TABLE "invoice" (
  id        UUID PRIMARY KEY NOT NULL,
  user_name VARCHAR(24)      NOT NULL,
  amount    DECIMAL(8, 2)    NOT NULL,
  "type"    invoice_type     NOT NULL,
  batch     BIGINT,
  status    VARCHAR(16) DEFAULT 'create',
  number    SERIAL           NOT NULL,
  wallet    VARCHAR(40),
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT batch CHECK (batch > 0),
  CONSTRAINT amount CHECK (amount <> 0),
  CONSTRAINT number UNIQUE (number)
);
CREATE UNIQUE INDEX invoice_id ON "invoice" USING BTREE ("id");
CREATE INDEX invoice_user ON invoice USING BTREE (user_name);
CREATE UNIQUE INDEX invoice_number ON invoice USING BTREE (number);


CREATE TABLE "matrix_type" (
  id      SMALLINT PRIMARY KEY,
  price   INT  NOT NULL,
  name    VARCHAR(24),
  degree  BIGINT NOT NULL,
  enabled BOOL NOT NULL DEFAULT true
);

INSERT INTO "matrix_type"(id, price, name, degree) VALUES (1, 20, 'Silver', 4::BIGINT);
INSERT INTO "matrix_type"(id, price, name, degree) VALUES (2, 100, 'Gold', 4::BIGINT);
INSERT INTO "matrix_type"(id, price, name, degree) VALUES (3, 1000, 'Diamond', 4::BIGINT);


CREATE TABLE "matrix_level" (
  id SERIAL PRIMARY KEY,
  type_id SMALLINT NOT NULL,
  level SMALLINT NOT NULL,
  income FLOAT NOT NULL,
  CONSTRAINT type_id FOREIGN KEY (type_id) REFERENCES matrix_type (id)
    ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO "matrix_level"(type_id, level, income) VALUES
  (1, 0, 0),
  (1, 1, 0.25),
  (1, 2, 0.25),
  (1, 3, 0.40);

INSERT INTO "matrix_level"(type_id, level, income) VALUES
  (2, 0, 0),
  (2, 1, 0.05),
  (2, 2, 0.10),
  (2, 3, 0.35),
  (2, 4, 0.20),
  (2, 5, 0.20);

INSERT INTO "matrix_level"(type_id, level, income) VALUES
  (3, 0, 0),
  (3, 1, 0.25),
  (3, 2, 0.25),
  (3, 3, 0.40);


CREATE SEQUENCE matrix_node_id_seq;

CREATE TABLE matrix_node (
  id            INT PRIMARY KEY NOT NULL DEFAULT nextval('matrix_node_id_seq'),
  type_id       SMALLINT NOT NULL,
  parent_id     INT,
  user_name     VARCHAR(24) NOT NULL,
  reinvest_from INT,
  number        INT NOT NULL DEFAULT 0,
  "time"        TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT unique_reinvest_from UNIQUE (type_id, reinvest_from),
  CONSTRAINT user_name FOREIGN KEY (user_name)
  REFERENCES "user"(name)
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT type_id FOREIGN KEY (type_id) REFERENCES matrix_type (id)
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT parent_id FOREIGN KEY (parent_id) REFERENCES matrix_node(id)
    ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX matrix_node_id ON matrix_node USING BTREE ("id");
CREATE INDEX matrix_node_type_id ON matrix_node USING BTREE (type_id);
CREATE INDEX matrix_node_user_name ON matrix_node USING BTREE (user_name);
CREATE INDEX matrix_node_parent_id ON matrix_node USING BTREE (parent_id);


CREATE TABLE reinvest_type (
  id INT PRIMARY KEY,
  source_type_id SMALLINT NOT NULL REFERENCES matrix_type(id),
  source_level INT NOT NULL,
  target_type_id SMALLINT NOT NULL REFERENCES matrix_type(id),
  pos INT,
  block INT8RANGE
);
INSERT INTO reinvest_type VALUES
  (1, 2, 3, 2, 23, '[20,23]'::INT8RANGE),
  (2, 2, 3, 3, 52, '[24,52]'::INT8RANGE),
  (3, 2, 4, 3, 113, '[94,113]'::INT8RANGE);


CREATE TABLE transfer (
  id            INT NOT NULL,
  receiver_name VARCHAR(24),
  amount        DECIMAL(8, 2) NOT NULL,
  memo          TEXT,
  node_id       INT,
  CONSTRAINT transfer_id FOREIGN KEY (id)
  REFERENCES "journal" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_receiver FOREIGN KEY (receiver_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_node_id FOREIGN KEY (node_id)
  REFERENCES matrix_node (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_amount CHECK (amount > 0)
);
CREATE UNIQUE INDEX transfer_id ON transfer USING BTREE ("id");
CREATE INDEX receiver_name ON transfer USING BTREE (receiver_name);
CREATE INDEX transfer_node_id ON transfer USING BTREE (node_id);


/* User and guest feedback table for app\modules\feedback\models\Feedback */
CREATE TABLE "feedback" (
  id      SERIAL PRIMARY KEY NOT NULL,
  ticket  CHAR(24) UNIQUE    NOT NULL,
  email   VARCHAR(48),
  subject VARCHAR(256)       NOT NULL,
  open    BOOL               NOT NULL DEFAULT TRUE
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING BTREE ("id");
CREATE UNIQUE INDEX feedback_ticket ON "feedback" USING BTREE ("ticket");


CREATE TABLE "feedback_message" (
  id          SERIAL PRIMARY KEY NOT NULL,
  feedback_id INT                NOT NULL,
  user_name   VARCHAR(24),
  answer      BOOL               NOT NULL DEFAULT FALSE,
  "content"   TEXT               NOT NULL,
  "time"      TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip          INET,
  CONSTRAINT feedback_user FOREIGN KEY (user_name)
  REFERENCES "user" ("name") ON DELETE CASCADE ON UPDATE NO ACTION
);


CREATE TABLE "article" (
  "id"       SERIAL PRIMARY KEY,
  "name"     VARCHAR(24),
  "title"    VARCHAR(256) NOT NULL,
  "keywords" VARCHAR(192),
  "summary"  TEXT,
  "content"  TEXT         NOT NULL
);
CREATE UNIQUE INDEX article_id ON "article" USING BTREE ("id");
INSERT INTO article (name, title, content) VALUES
  ('main', 'Best Choice', ''),
  ('about', 'О нас', ''),
  ('marketing', 'Маркетинг', ''),
  ('rules', 'Правила', ''),
  ('contacts', 'О нас', '');


CREATE TABLE "setting" (
  "id"       SERIAL PRIMARY KEY,
  "category" VARCHAR(24) NOT NULL,
  "name"     VARCHAR(24) NOT NULL,
  "type"     VARCHAR(24),
  "value"    TEXT
);

INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'email', 'zhmurko1990@yandex.ru');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'login_fails', 5);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'skype', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'google', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'yandex', '34013255');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'heart', NULL);
INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('common', 'withdrawal_confirmation', 'bool', TRUE);

INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('common', 'perfect', 'bool', TRUE);
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'id', '1473523');
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'password', null);
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'wallet', 'E10605724');
INSERT INTO "setting" ("category", "name", "value") VALUES ('perfect', 'secret', 'vasja1');
INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('perfect', 'verify', 'bool', TRUE);

INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('common', 'nix', 'bool', TRUE);
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'server', 'https://www.nixmoney.com/');
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'ip', '176.9.161.160/27');
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'email', 'extravaganza.systems@gmail.com');
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'password', null);
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'wallet', 'E20215688427398');
INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('nix', 'verify', 'bool', FALSE);

INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('common', 'advcash', 'bool', FALSE);
INSERT INTO "setting" ("category", "name", "value") VALUES ('advcash', 'email', 'zhmurko90@gmail.com');
INSERT INTO "setting" ("category", "name", "value") VALUES ('advcash', 'secret', '_fZ8Nh396p');
INSERT INTO "setting" ("category", "name", "value") VALUES ('advcash', 'wallet', 'E858009509939');
INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('advcash', 'verify', 'bool', FALSE);

ALTER TABLE setting ADD COLUMN visible BOOLEAN DEFAULT TRUE;
UPDATE setting SET visible = FALSE WHERE "name" in ('password', 'secret');

/* see web/visit.php */
/* Visitors list */
CREATE TABLE "visit_agent" (
  "id"    SERIAL PRIMARY KEY,
  "ip"    INET,
  "agent" VARCHAR(200),
  CONSTRAINT agent UNIQUE (ip, agent)
);
CREATE UNIQUE INDEX visit_agent_id ON "visit_agent" USING BTREE (id);
CREATE UNIQUE INDEX visitor ON "visit_agent" USING BTREE (ip, agent);


/* Visitors log */
CREATE TABLE "visit_path" (
  "id"       SERIAL PRIMARY KEY,
  "agent_id" INT         NOT NULL,
  "path"     VARCHAR(80) NOT NULL,
  "spend"    SMALLINT,
  "time"     TIMESTAMP DEFAULT current_timestamp,
  "data"     TEXT,
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES "visit_agent" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE
);


/* Visitors with user agent log view */
CREATE VIEW "visit" AS
  SELECT p.id AS id,
    agent_id,
    spend,
    (
      SELECT user_name
      FROM journal
      WHERE ip = a.ip
      ORDER BY id DESC
      LIMIT 1
    ),
    "path",
    p."time",
    a.ip,
    agent
  FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id;


CREATE TABLE reinvest(
  id SERIAL,
  user_name VARCHAR(24),
  type_id SMALLINT,
  reinvest_from INT,
  node_id INT
);


CREATE TABLE generation_log (
  id SERIAL,
  user_name VARCHAR(24),
  ref_name TEXT,
  type_id SMALLINT,
  number INT
);
