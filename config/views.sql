/* English-Russian translation view */
CREATE VIEW "translation" AS
  SELECT
    s.id,
    message,
    translation
  FROM source_message s JOIN message t ON s.id = t.id
  WHERE "language" = 'ru';


CREATE VIEW matrix AS
    WITH RECURSIVE r(id, type_id, user_name, "level", root_id, parent_id, reinvest_from, number, "time") AS (
      SELECT
        n.id,
        type_id,
        user_name,
        0    AS "level",
        n.id AS root_id,
        n.parent_id,
        n.reinvest_from,
        n.number,
        n."time"
      FROM matrix_node n JOIN "user" u ON user_name = "name"
      UNION
      SELECT
        n.id,
        n.type_id,
        n.user_name,
        r."level" + 1 AS "level",
        r.root_id,
        n.parent_id,
        n.reinvest_from,
        n.number,
        n."time"
      FROM matrix_node n
        JOIN r ON r.id = n.parent_id
    )
    SELECT
      r.id,
      row_number()
      OVER (PARTITION BY root_id
        ORDER BY r.id) - 1 AS "number",
      row_number()
      OVER (PARTITION BY root_id, r."level"
        ORDER BY r.id) AS "pos",
      r.type_id,
      r.user_name,
      r."level",
      r.root_id,
      t.price * l.income       AS interest,
      r.parent_id,
      r.reinvest_from,
      r."time"
    FROM r
      JOIN matrix_type t ON r.type_id = t.id
      LEFT JOIN matrix_level l ON r."level" = l.level AND r.type_id = l.type_id
    ORDER BY type_id, root_id, r.number;


CREATE VIEW degree AS
  SELECT root_id, level, count(*) as count FROM matrix m
    JOIN matrix_type t ON m.type_id = t.id
  GROUP BY root_id, level, t.degree HAVING count(*) < power(t.degree, level);


CREATE VIEW income AS
  SELECT root_id, sum(m.interest) as amount
  FROM matrix m JOIN matrix_level l ON m.type_id = l.type_id AND m.level = l.level
  GROUP BY root_id;


CREATE VIEW level_income AS
  SELECT root_id, m.level, sum(m.interest) as amount, count(*) as count FROM matrix m
    JOIN matrix_level l ON m.type_id = l.type_id AND m.level = l.level
    WHERE m.level > 0
  GROUP BY root_id, m.level;


CREATE VIEW referral AS
  WITH RECURSIVE r(id, type_id, user_name, ref_name, "level", root_id, parent_id, "time") AS (
    SELECT
      n.id,
      n.type_id,
      user_name,
      ref_name,
      0    AS "level",
      n.id AS root_id,
      n.parent_id,
      "time"
    FROM matrix_node n JOIN "user" u ON user_name = "name"
    UNION
    SELECT
      n.id,
      n.type_id,
      n.user_name,
      r.ref_name,
      r."level" + 1 AS "level",
      r.root_id,
      n.parent_id,
      n."time"
    FROM matrix_node n
      JOIN r ON r.parent_id = n.id
  )
  SELECT
    r.id,
    row_number() OVER (PARTITION BY root_id ORDER BY r.id) - 1 AS "number",
    r.type_id,
    user_name,
    ref_name,
    r."level",
    (price * income)::DECIMAL(8,2) AS interest,
    root_id,
    parent_id,
    "time"
  FROM r
    JOIN matrix_type t ON r.type_id = t.id
    JOIN matrix_level l ON r."level" = l.level AND r.type_id = l.type_id
  ORDER BY r.type_id, root_id;


CREATE VIEW referral_matrix AS
  WITH RECURSIVE r(id, "name", ref_name, root, level) AS (
    SELECT
      id,
      "name",
      ref_name,
      "name" AS root,
      0 as level
    FROM "user"
    UNION
    SELECT
      u.id,
      u.name,
      u.ref_name,
      r.root,
      r.level + 1 as level
    FROM "user" u JOIN r ON u.ref_name = r.name
  )
  SELECT
    id,
    "name",
    ref_name,
    root,
    level
  FROM r;


CREATE VIEW referral_referral AS
  WITH RECURSIVE r(id, "name", ref_name, root, level) AS (
    SELECT
      id,
      "name",
      ref_name,
      "name" AS root,
      0 as level
    FROM "user"
    UNION
    SELECT
      u.id,
      u.name,
      u.ref_name,
      r.root,
      r.level + 1 as level
    FROM "user" u JOIN r ON u.name = r.ref_name
  )
  SELECT
    id,
    "name",
    ref_name,
    root,
    level,
    (SELECT max(level) FROM r rr WHERE rr.root = r.root) - level as matrix_level
  FROM r
  ORDER BY level;


CREATE VIEW referral_count AS
  SELECT u.name, u.ref_name, (SELECT count(*) FROM "user" WHERE ref_name = u.name) FROM "user" u;


CREATE VIEW transfer_journal AS
  SELECT
    t.amount,
    j.type,
    j.event,
    j.user_name AS sender_name,
    t.receiver_name,
    j.object_id::INT,
    t.node_id,
    t.memo,
    j."time",
    j.id,
    j.ip
  FROM journal j JOIN transfer t ON j.id = t.id;


CREATE VIEW transfer_income AS
  SELECT j.id,
    j.user_name as sender_name,
    t.node_id,
    t.receiver_name,
    j.object_id,
    m.level,
    t.amount,
    j.type,
    m.pos,
    j.event,
    j.time
  FROM journal j JOIN transfer t ON j.id = t.id
    LEFT JOIN matrix m ON m.id = t.node_id AND m.root_id::VARCHAR(40) = j.object_id
  WHERE j.event <> 'transfer';
