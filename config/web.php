<?php

use app\behaviors\Journal;

$config['components']['user'] = [
    'identityClass' => 'app\models\User',
//    'enableAutoLogin' => true,
    'loginUrl' => ['user/login'],
    'on afterLogin' => function($event) {
        /* @var $user \app\models\User */
        $user = Yii::$app->user->identity;
        $event->name = 'login';
        if ($user->isManager()) {
            $ips = file_get_contents(Yii::getAlias('@app/config/hosts.allow'));
            $ips = explode("\n", $ips);
            if (!in_array($_SERVER['REMOTE_ADDR'], $ips)) {
                $event->name = 'blocked';
            }
        }
        $event->sender = $user;
        Journal::writeEvent($event);
        if ($user->isManager()) {
            setcookie('role', 'manage');
        }
        if ('blocked' == $event->name) {
            Yii::$app->session->addFlash('error', 'Доступ запрещен');
            header('Location: /login');
            exit();
        }
    },
    'on beforeLogout' => function($event) {
        $event->name = 'logout';
        $event->sender = Yii::$app->user->identity;
        Journal::writeEvent($event);
    },
];

$config['components']['errorHandler'] = [
    'errorAction' => 'home/error',
];

$config['components']['formatter'] = [
    'class' => 'yii\i18n\Formatter',
    'dateFormat' => 'php:d-m-Y',
    'datetimeFormat' => 'php:d-m-Y H:i',
    'timeFormat' => 'php:H:i:s',
];

$config['modules']['lang'] = 'app\modules\lang\Module';
$config['modules']['invoice'] = 'app\modules\invoice\Module';
$config['modules']['feedback'] = 'app\modules\feedback\Module';
$config['modules']['article'] = 'app\modules\article\Module';
$config['modules']['matrix'] = 'app\modules\matrix\Module';
//$config['modules']['test'] = 'app\modules\test\Module';
$config['bootstrap'][] = 'lang';
$config['bootstrap'][] = 'invoice';
$config['bootstrap'][] = 'feedback';
$config['bootstrap'][] = 'article';
$config['bootstrap'][] = 'matrix';
//$config['bootstrap'][] = 'test';


if (YII_ENV_DEV) {
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '194.44.188.18'],
    ];
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['bootstrap'][] = 'gii';
    $config['bootstrap'][] = 'debug';
}
