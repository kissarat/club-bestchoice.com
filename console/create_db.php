db<?php

require_once __DIR__ . '/../config/pdo.php';

$name = $config['components']['db']['username'];
$password = $config['components']['db']['password'];

pg_connect('host=/var/run/postgresql');

try {
    pg_query("DROP DATABASE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

try {
    pg_query("DROP ROLE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

pg_query("CREATE ROLE \"$name\" LOGIN PASSWORD '$password'");
pg_query("CREATE DATABASE \"$name\" OWNER \"$name\"");

pg_close();

$schema = file_get_contents(ROOT . '/config/tables.sql');
$schema = explode(';', $schema);
$views = file_get_contents(ROOT . '/config/views.sql');
$schema = array_merge($schema, explode(';', $views));
$schema[] = file_get_contents(ROOT . '/config/procedure.sql');
$schema[] = file_get_contents(ROOT . '/data/01-source_message.sql');
$schema[] = file_get_contents(ROOT . '/data/02-message.sql');
$schema[] = 'INSERT INTO "user"("name", "email", "hash", "status", "ref_name") VALUES
            (\'money\', \'money@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 1, NULL),
            (\'millionere\', \'millionere@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 1, \'money\'),
            (\'planeta\', \'planeta@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 1, \'millionere\'),
            (\'forlife\', \'monforlifeey@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 1, \'millionere\')
            ';
$schema[] = 'INSERT INTO "matrix_node"("id", "user_name", "type_id", "parent_id", "number", "reinvest_from") VALUES
              (1, \'money\', 2, NULL, 0, NULL),
              (2, \'money\', 2, 1, 1, 1),
              (3, \'money\', 3, NULL, 0, NULL),

              (61, \'money\',      1, NULL, 0, NULL),
              (62, \'millionere\', 1, 61,    0, NULL),
              (63, \'planeta\',    1, 62,    0, NULL),
              (64, \'forlife\',    1, 62,    0, NULL),
              (65, \'money\',      1, 62,    1, 61)
            ';
$schema[] = 'select setval(\'matrix_node_id_seq\', 66)';
$pdo = connect();
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$pdo->beginTransaction();
foreach($schema as $sql) {
    $sql = trim($sql);
//    if (0 === strpos($sql, '--')) {
//        continue;
//    }
    if (!empty($sql)) {
        $sql = preg_replace('|/\*[^\*]+\*/|', '', $sql);
        $sql = preg_replace('|[\t ]+|', ' ', $sql);
        $sql = trim($sql);
//        echo $sql . "\n";
        $pdo->exec($sql);
    }
}
$pdo->commit();
apc_clear_cache();
