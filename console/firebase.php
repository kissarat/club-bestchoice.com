<?php
use Firebase\FirebaseLib;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/pdo.php';

$params = [
    'action:',
    'table:',
    'columns::',
    'host::',
    'id::',
];

$options = getopt('a:t:c:h::i::');

$fb = new FirebaseLib('https://back.firebaseio.com/', 'exwkFWw6GWtNCqYoQAK8cS5ATzSvFyENdB4AnioH');
$pdo = connect();

function id($id) {
    return str_replace('.', '_', $id);
}

define('DOMAIN', id($config['id']));

function path($id) {
    global $options;
    return DOMAIN . "/" . $options['t'] . "/" . id($id);
}

switch($options['a']) {
    case 'up':
        $columns = isset($options['c']) ? $options['c'] : '*';
        $r = $pdo->query('SELECT ' . $columns . ' FROM "' . $options['t'] . '" ORDER BY id');
        $rows = [];
	while($record = $r->fetch(PDO::FETCH_ASSOC)) {
            $object = [];
            $id = $record['id'];
            foreach($record as $key => $value) {
                if (null !== $value && '' !== $value) {
                    if (is_numeric($value)) {
                        $value = (float) $value;
                    }
                    $object[$key] = $value;
                }
            }
            $rows[$id] = $object;
        }
        $fb->set(DOMAIN . "/" . $options['t'], $rows);
        break;

    case 'get':
        $object = $fb->get(path($options['i']));
        $object = json_decode($object, true);
        $object['id'] = $argv[3];
        echo json_encode($object, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\n";
        break;

    case 'delete':
        $fb->delete(path($options['i']));
}
