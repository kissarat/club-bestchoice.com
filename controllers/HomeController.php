<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;


use app\behaviors\NoTokenValidation;
use app\modules\article\models\Article;
use app\modules\invoice\models\Invoice;
use app\models\User;
use app\helpers\SQL;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class HomeController extends Controller {

    public function behaviors() {
        return [
            'no_csrf' => [
                'class' => NoTokenValidation::class,
                'only' => ['index'],
            ]
        ];
    }

    public function actionIndex() {

        if ('admin' == Yii::$app->layout) {
            return $this->redirect(['/matrix/matrix/nodes']);
        }
        else {
            return $this->render('index', [
                'article' => Yii::$app->view->render('@app/modules/article/views/article/view', [
                    'model' => Article::find()->where(['name' => 'main'])->one()
                ])
            ]);
        }
    }

    public static function tryLogin() {
        if (Yii::$app->user->getIsGuest() && isset($_POST['auth'])
            && $user = User::findOne(['auth' => $_POST['auth']])) {
            Yii::$app->user->login($user);
        }
        return !Yii::$app->user->getIsGuest();
    }

    public function actionStatistics() {
        static::tryLogin();
        return $this->render('statistics', ['statistics' => static::statistics()]);
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        if ($exception instanceof ForbiddenHttpException) {
            Yii::$app->view->title = 'Отказано в доступе';
        }
        elseif ($exception instanceof NotFoundHttpException) {
            Yii::$app->view->title = 'Не найдено';
        }
        return $this->render('error', [
            'exception' => $exception
        ]);
    }

    public static function statistics() {
        $started = strtotime(SQL::queryCell('SELECT "time" FROM "journal" WHERE id = 1'));
        $invested = (int) SQL::queryCell('SELECT count(*) FROM matrix_node');
        $internal = (int) SQL::queryCell('SELECT sum(amount) FROM transfer');
        return [
            'Started' => date('d-m-Y', $started),
            'Running days' => floor((time() - $started)/(3600 * 24)),
            'Users' => User::find()->count(),
            'Total deposited' => Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount > 0')->sum('amount'),
            'Total withdraw' => - Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount < 0')->sum('amount'),
            'Number of investments' => $invested,
            'Внутренних переводов' => $internal,
            'Online' => User::online()->count()
        ];
    }

    protected function renderTable($sql, $headers) {
        return $this->render('table', [
            'rows' => SQL::queryAll($sql),
            'headers' => $headers
        ]);
    }
}
