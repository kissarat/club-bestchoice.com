<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;

use app\behaviors\Access;
use app\behaviors\Journal;
use app\models\LoginForm;
use app\models\Password;
use app\models\Record;
use app\models\ResetRequest;
use app\helpers\SQL;
use app\modules\invoice\controllers\TransferController;
use app\modules\matrix\models\Node;
use PDO;
use Yii;
use app\models\User;
use app\models\search\User as UserSearch;
use yii\bootstrap\Html;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'update', 'cabinet', 'clear'],
                'manager' => ['account'],
                'admin' => ['delete']
            ]
        ];
    }

    public function actionIndex()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $level = 1000 * 1000;
        if (!$user->isManager()) {
            if (empty($_GET['ref_name'])) {
                throw new ForbiddenHttpException('Вы не можете видеть список всех пользователей');
            }
            $level = SQL::queryCell('SELECT level FROM referral_referral WHERE root = :root AND "name" = :name', [
                ':root' => $_GET['ref_name'],
                ':name' => $user->name
            ]);
            if (false === $level) {
                throw new ForbiddenHttpException('Вы можете просматривать только своих рефералов');
            }
            if ($level > 5) {
                throw new ForbiddenHttpException('Вы можете просматривать только 5 уровней рефералов');
            }
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $vars = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];

        if (isset($_GET['ref_name'])) {
            $ref_name = $_GET['ref_name'];
            $vars['sponsors'] = SQL::queryAll(
                'SELECT "name", "count" FROM referral_count WHERE ref_name = :name',
                [':name' => $ref_name],
                PDO::FETCH_KEY_PAIR);
            $referrals = SQL::queryAll('SELECT level, name FROM referral_referral WHERE root = :root
                AND level <= :level
                ORDER BY "level"', [
                ':root' => $ref_name,
                ':level' => $level
            ], PDO::FETCH_KEY_PAIR);
            $vars['referrals'] = [];
            foreach ($referrals as $level => $referral) {
                if ($level > 0) {
                    $vars['referrals'][] = $referral;
                }
            }
            sort($vars['referrals'], SORT_DESC);
        } else {
            $vars['sponsors'] = SQL::queryAll(
                'SELECT "name", "count" FROM referral_count',
                null,
                PDO::FETCH_KEY_PAIR);
        }

        if (!Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('index', $vars);
    }

    public function actionView($name = null)
    {
        if (Yii::$app->user->getIsGuest()) {
            Yii::$app->session->addFlash('error', 'Только зарегистрированные пользователи могут видеть профили');
            return $this->redirect(['/user/login']);
        }

        $model = $name ? $this->findModel($name) : Yii::$app->user->identity;

        $_GET['user'] = $model->name;
        $this->layout = 'cabinet';
        return $this->render('@app/modules/invoice/views/transfer/index',
            TransferController::index('income', $model->name));
    }

    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'name' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($name)
    {
        $model = $this->findModel($name);

        if ($model->name != Yii::$app->user->identity->name && !Yii::$app->user->identity->isAdmin()) {
            throw new ForbiddenHttpException();
        }

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->pin)) {
                $model->pin = $model->getOldAttribute('pin');
            }

            $image = UploadedFile::getInstance($model, 'image');
            if ($image) {
                $model->avatar = $image->getExtension();
                move_uploaded_file($image->tempName, $model->getAvatarPath());
            }

            if ($model->delete_avatar && $model->avatar) {
                unlink($model->getAvatarPath());
                $model->avatar = null;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'name' => $model->name]);
            }
        }
        $model->pin = null;

        if (!Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($name)
    {
        $this->findModel($name)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param string $name
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if (($model = User::findOne(['name' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSignup($ref_name = null)
    {
        $model = new User([
            'scenario' => 'signup'
        ]);
        if (!$ref_name && Yii::$app->session->has('ref_name')) {
            $ref_name = Yii::$app->session->get('ref_name');
        }
        if ($ref_name) {
            if (!Yii::$app->session->has('ref_name')) {
                Yii::$app->session->set('ref_name', $ref_name);
            }
        } else {
            $ref_name = 'money';
        }
        $ref_number = 0;
        if (preg_match('|^([\w_\-]+)\-(\d+)$|i', $ref_name, $matches)) {
            $count = SQL::queryCell('SELECT count(*) FROM matrix_node WHERE user_name = :name AND number = :number', [
                ':name' => $matches[1],
                ':number' => (int)$matches[2],
            ]);
            if ($count < 1) {
                throw new NotFoundHttpException("Рефрельная инвестиция №$ref_number пользователя $ref_name не существует");
            }
            $ref_name = $matches[1];
            $ref_number = (int)$matches[2];
        }
        if (empty($ref_name)) {
            $ref_name = 'money';
        }
        $model->ref_name = $ref_name;
        $model->ref_number = $ref_number;
        if (preg_match(User::PERFECT, $model->name)) {
            $model->addError('name', Yii::t('app', Yii::t('app', 'Username cannot be in the format of Perfect Money wallet')));
        } elseif ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->ref_name && Node::find()->where(['user_name' => $model->ref_name])->count() <= 0) {
                $model->ref_name = null;
            }
            $model->generateAuthKey();
            $model->status = User::PLAIN;
            if ($model->save(false) && Yii::$app->user->login($model)) {
                $lines = [
                    'Поздравляем с регистрацией!',
                    "Логин: $model->name",
                    "Пароль: $model->password",
                    "Транзакционний PIN: $model->pin",
                ];
                if (!$model->sendEmail([
                    'subject' => Yii::$app->name,
                    'content' => implode('<br/>', $lines)
                ])) {
                    Yii::$app->session->setFlash('error', 'Невозможно отправить письмо');
                }
                return $this->redirect(['/user/view']);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        $model = new LoginForm(['scenario' => Yii::$app->layout]);
        if ('admin' == $model->getScenario()) {
            $model->duration = 60;
        }

        if ($model->load(Yii::$app->request->post())) {
            $user = $model->getUser();
            $count = $user->count();
            if ($count > 1) {
                $model->addError('name', 'На этот email зарегистрировано более одного аккаунта,
                вы можете зайти только используя логин');
            } elseif (1 == $count) {
                $user = $user->one();
                if (empty($user->hash)) {
                    Yii::$app->session->setFlash('error', Yii::t('app',
                        Yii::t('app', 'Your account is not activated. Check your email')));
                } else {
                    $can = $user->canLogin();
                    if ($can && $user->validatePassword($model->password)) {
                        if ($user->status > 0) {
                            if (empty($user->auth)) {
                                $user->generateAuthKey();
                            }
                            $user->last_access = date('Y-m-d H:i:s');

                            if ('admin' == Yii::$app->layout && !$user->isManager()) {
                                Yii::$app->session->addFlash('error', Yii::t('app', 'You cannot manage site'));
                            } elseif ($user->save(true, ['auth', 'last_access']) && Yii::$app->user->login($user,
                                    'admin' == Yii::$app->layout && $model->remember ? $model->duration * 60 : 0)
                            ) {
                                return $this->redirect('admin' == Yii::$app->layout ? ['/home/statistics'] : ['/user/view']);
                            } else {
                                Yii::$app->session->addFlash('error', Yii::t('app', 'Something wrong happened'));
                            }
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', Yii::t('app', 'Your account is blocked')));
                        }
                    } else {
                        Journal::write('user', 'login_fail', $user->id);
                        if ($can) {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
                        } else {
                            $record = Record::find()->where([
                                'object_id' => $user->id,
                                'event' => 'login_fail'
                            ])->orderBy(['time' => SORT_DESC])->one();
                            Yii::$app->session->setFlash('error',
                                Yii::t('app', 'You have exceeded the maximum number of login attempts, you will be able to enter after {time}', [
                                    'time' => $record->time
                                ]));
                        }
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['home/index']);
    }

    public function actionEmail($code)
    {
        /** @var User $user */
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
            $user = User::findOne(['name' => $code]);
        } else {
            $user = preg_match('|^[\w\-_]{64}$|', $code) ? User::findOne(['code' => $code]) : null;
        }
        if ($user) {
            $bundle = $user->getBundle();
            $message = empty($user->hash) ? 'Congratulations. You have successfully activated!' : 'Your email changed!';
            foreach ($bundle as $name => $value) {
                $user->$name = $value;
            }
            $user->code = null;
            $user->setBundle(null);
            if ($user->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', $message));
                Yii::$app->user->login($user);
                return $this->redirect(['user/view']);
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Something wrong happened'));
            }
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid code'));
        }
        return $this->redirect(['home/index']);
    }

    public function actionPassword($code = null, $name = null)
    {
        /** @var User $user */
        $message = null;
        $model = null;
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        }

        $user = null;

        if ($name) {
            if (!Yii::$app->user->getIsGuest()
                && (Yii::$app->user->identity->isAdmin() || $name == Yii::$app->user->identity->name)
            ) {
                $user = User::findOne(['name' => $name]);
            } else {
                throw new ForbiddenHttpException('Вы можете изменить только свой пароль');
            }
        } elseif ($code) {
            $user = User::findOne(['code' => $code]);
        }

        if ($user) {
            $scenario = ($code || $name) ? 'reset' : 'default';
            if (!Yii::$app->user->getIsGuest() && $name == Yii::$app->user->identity->name) {
                $scenario = 'default';
            }
            $model = new Password([
                'scenario' => $scenario,
                'user' => $user
            ]);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ('reset' == $model->scenario) {
                    $user->code = null;
                    $user->generateAuthKey();
                    $user->setPassword($model->new_password);
                    if ($user->save()) {
                        Yii::$app->session->addFlash('success', Yii::t('app', 'Password saved'));
                        return Yii::$app->user->isGuest
                            ? $this->redirect(['user/login'])
                            : $this->redirect(['user/view', 'name' => $user->name]);
                    } else {
                        $message = json_encode($user->errors, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
                    }
                } else {
                    if ($user->validatePassword($model->password)) {
                        $user->setPassword($model->new_password);
                        $user->generateAuthKey();
                        if ($user->save()) {
                            Yii::$app->session->addFlash('success', Yii::t('app', 'Password saved'));
                            return $this->redirect(['user/view', 'name' => $user->name]);
                        } else {
                            $message = json_encode($user->errors, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
                        }
                    } else {
                        $model->addError('password', Yii::t('app', 'Invalid password'));
                    }
                }
            }
        } else {
            $message = Yii::t('app', 'Invalid code');
        }

        if (!Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('password', [
            'model' => $model,
            'message' => $message,
            'user' => $user
        ]);
    }

    public function actionRequest()
    {
        /** @var User $user */
        $model = new ResetRequest();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $condition = [];
            $error = 'Введите логин или пароль';
            if ($model->name) {
                $condition['name'] = $model->name;
                $error = 'Пользователь с таким логином не найден';
            }
            if ($model->email) {
                $condition['email'] = $model->email;
                $error = 'Пользователь с таким email не найден';
            }
            $count = User::find()->where($condition)->count();
            if ($count > 1) {
                Yii::$app->session->setFlash('error', 'Введите логин');
            }
            elseif ($count < 1) {
                Yii::$app->session->setFlash('error', 'Пользователь с таким логином и email не найден');
            }
            elseif ($user = User::findOne($condition)) {
                $user->generateCode();
                if ($user->save(true, ['code'])) {
                    $url = Url::to(['password', 'code' => $user->code], true);
                    $lines = [
                        "Здраствуйте $user!",
                        "Для восстановления пароля перейдите по ссылке " . Html::a($url, $url)
                    ];
                    if ($user->sendEmail([
                        'subject' => Yii::$app->name . ' восстановление пароля',
                        'content' => implode('<br/>', $lines)
                    ])) {
                        Yii::$app->session->setFlash('info', Yii::t('app', 'Check your email'));
                        return $this->redirect(['home/index']);
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Failed to send mail'));
                    }
                } else {
                    Yii::$app->session->setFlash('error', json_encode($user->errors));
                }
            } else {
                Yii::$app->session->setFlash('error', $error);
            }
        }

        render:
        return $this->render('request', [
            'model' => $model
        ]);
    }

    public function actionComplete($search)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return User::find()
            ->select('name')
            ->where('name like :name', [
                ':name' => "$search%"
            ])
            ->limit(10)
            ->column();
    }

    public function actionExists($name)
    {
        Yii::$app->response->setStatusCode(User::find()->where(['name' => $name])->count() > 0
            ? 302 : 404);
    }

    public function actionInfo()
    {
        $info = ['isGuest' => Yii::$app->user->getIsGuest()];
        if (!$info['isGuest']) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $info['name'] = $user->name;
            $info['isManager'] = $user->isManager();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $info;
    }

    public function actionList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return SQL::queryAll('SELECT "name", "ref_name" FROM "user" ORDER BY id', null, PDO::FETCH_KEY_PAIR);
    }

    public function actionClear() {
        if (Yii::$app->user->identity->isAdmin()) {
            SQL::execute('UPDATE "user" SET auth = NULL');
            Yii::$app->cache->flush();
        }
        else {
            SQL::execute('UPDATE "user" SET auth = NULL WHERE "name" = :name', [
                ':name' => Yii::$app->user->identity->name
            ]);
            Yii::$app->user->logout(true);
        }
        return $this->redirect(['/user/login']);
    }
}
