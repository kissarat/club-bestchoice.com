<?php

namespace app\helpers;


use app\models\Settings;
use Yii;
use yii\i18n\Formatter;

class God {
    public static function generateRandomLiteral($length = 32) {
        $bytes = openssl_random_pseudo_bytes($length);
        return strtr(substr(base64_encode($bytes), 0, $length), '+/', 'A_');
    }

    public static function format() {
        return new Formatter();
    }

    public static function mail($email, $template, $params) {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Settings::get('common', 'email') => Yii::$app->name])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->render("@app/mails/$template", $params))
            ->send();
    }
}
