<?php

namespace app\helpers;


use DOMDocument;
use DOMText;
use DOMXPath;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\Html;
use yii\web\ServerErrorHttpException;

class Soap extends Component {
    const SCHEMA = 'http://schemas.xmlsoap.org/soap/envelope/';
    const USER_AGENT = 'wget/1.18';
    public $namespace;
    public $url;
    public $doc;

    public function createDocument($name, $params) {
        $doc = new DOMDocument();
        $envelop = $doc->createElementNS(static::SCHEMA, 'soapenv:Envelope');
        $doc->appendChild($envelop);
        $envelop->appendChild($doc->createElement('soapenv:Header'));
        $method = $doc->createElementNS($this->namespace, 'wsm:' . $name);
        for($i = 0; $i < count($params); $i++) {
            $param = $params[$i];
            $arg = $doc->createElement('arg' . $i);
            if (is_array($param)) {
                foreach($param as $key => $value) {
                    $arg->appendChild($doc->createElement($key, $value));
                }
            }
            $method->appendChild($arg);
        }
        $body = $doc->createElementNS(static::SCHEMA, 'soapenv:Body');
        $body->appendChild($method);
        $envelop->appendChild($body);
        return $doc;
    }

    public function request($name, $params) {
        $data = $this->createDocument($name, $params)->saveXML();
        $curl  = curl_init($this->url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_USERAGENT, static::USER_AGENT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
        curl_close($curl);
        file_put_contents(\Yii::getAlias('@app/log/send' . time() . '.txt'), $content);
        $doc = new DOMDocument();
        $doc->loadXML($content);
        $xpath = new DOMXPath($doc);
        $list = $xpath->evaluate('//return');
        if ($list && $list->length > 0) {
            if (1 == $list->length) {
                return $list->item(0)->firstChild->data;
            }
            return $list;
        }
        $list = $xpath->evaluate('//faultstring');
        if ($list && $list->length > 0) {
            throw new ServerErrorHttpException($list->item(0)->firstChild->data);
        }
        return $doc;
    }
}
