<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$to      = 'kissarat@gmail.com';
$subject = 'the subject';
$message = 'hello';
$from = 'test@mail.com';
$headers = implode("\r\n", [
    'From: ' . $from,
    'Reply-To: ' . $from,
    'X-Mailer: PHP/' . phpversion()
]);

mail($to, $subject, $message, $headers);
