<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\base\Model;
use ZipArchive;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * @property \yii\web\UploadedFile[] $files
 */
class ImportForm extends Model {
    public $files;

    public function rules() {
        return [
            ['files', 'file', 'skipOnEmpty' => false, 'extensions' => 'zip', 'maxFiles' => 6]
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            $info = [];
            foreach($this->files as $file) {
                $zip = new ZipArchive();
                $zip->open($file->tempName);
                $files_info = [];
                for($i = 0; $i < $zip->numFiles; $i++) {
                    $stat = $zip->statIndex($i);
                    $sql = $zip->getFromIndex($i);
                    $result = 0;
                    foreach(explode(";\n\n", $sql) as $query) {
                        $command = Yii::$app->db->createCommand($query);
                        $command->execute();
                        $result += $command->pdoStatement->rowCount();
                    }
                    $files_info[$stat['name']] = $result;
                }
                $info[$file->name] = $files_info;
            }
            $transaction->commit();
            return $info;
        }
        return false;
    }
}
