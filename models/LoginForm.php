<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\base\Model;


/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class LoginForm extends Model {
    public $name;
    public $password;
    public $remember;
    public $duration;

    public function rules() {
        return [
            [['name', 'password'], 'required'],
            ['name', 'string', 'min' => 4, 'max' => 24],
            ['password', 'string', 'min' => 1],
            ['remember', 'boolean'],
            ['remember', 'default', 'value' => true],
            ['duration', 'number', 'min' => 60],
        ];
    }

    public function scenarios() {
        return [
            'main' => ['name', 'password'],
            'admin' => ['name', 'password', 'remember', 'duration'],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'Логин / e-mail'),
            'password' => Yii::t('app', 'Password'),
            'remember' => 'Запомнить меня',
        ];
    }

    public static function login(User $user) {
        Yii::$app->user->login($user);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        $query = User::find();
        return strpos($this->name, '@') === false
            ? $query->where(['name' => $this->name])
            : $query->where(['email' => $this->name]);
    }
}
