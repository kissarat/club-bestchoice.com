<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models;


use Yii;
use yii\base\Model;

class ResetRequest extends Model {
    public $name;
    public $email;


    public function rules() {
        return [
            ['name', 'string', 'length' => [4, 24]],
            ['name', 'exist',
                'targetClass' => 'app\models\User',
                'message' => 'Пользователь не существует'],
            ['email', 'email'],
            [['name', 'email'], 'filter', 'filter' => 'trim'],
            ['email', 'exist',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'User with this e-mail does not exist')],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => 'Логин',
            'email' => 'Email'
        ];
    }
}
