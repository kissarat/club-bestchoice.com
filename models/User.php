<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use app\behaviors\Journal;
use app\helpers\God;
use app\helpers\SQL;
use app\modules\invoice\models\Transfer;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\User as WebUser;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * Class User
 * @property integer id
 * @property string name
 * @property string auth
 * @property string email
 * @property string code
 * @property string hash
 * @property string forename
 * @property string surname
 * @property string skype
 * @property integer pin
 * @property string wallet_perfect
 * @property string wallet_nix
 * @property string wallet_advcash
 * @property string repeat
 * @property integer status
 * @property number account
 * @property number blocked
 * @property integer last_access
 * @property string avatar
 * @property string ref_name
 * @property integer ref_number
 * @property string vip
 * @property resource data
 * @property array bundle
 *
 * @property User referral
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface {

    const BLOCKED = 0;
    const ADMIN = 1;
    const PLAIN = 2;
    const MANAGER = 3;
    const TEAM = 4;

    const PERFECT = '/^E\d{7,8}$/';
    const NIX = '/^E\d+$/';

    private $_password;
    private $_info;
    public $repeat;
    public $adult;
    public $agreement;
    public $image;
    public $delete_avatar;

    public static function primaryKey() {
        return ['id'];
    }

    public static function statuses() {
        return [
            User::BLOCKED => Yii::t('app', 'Blocked'),
            User::ADMIN => Yii::t('app', 'Admin'),
            User::PLAIN => Yii::t('app', 'Registered'),
            User::MANAGER => Yii::t('app', 'Manager'),
            User::TEAM => Yii::t('app', 'Team')
        ];
    }

    public static $events = [
        'login' => 'Вход',
        'logout' => 'Выход',
        'login_fail' => 'Неудачная попытка входа',
    ];

    public function traceable() {
        return ['status', 'email', 'account'];
    }

    public function url() {
        return ['user/view', 'name' => $this->name];
    }

    public function rules() {
        return [
            ['id', 'integer'],
            [['name', 'email', 'forename', 'surname', 'city', 'phone', 'skype'], 'required'],
            [['password', 'repeat', 'pin'], 'required', 'on' => 'signup'],
            ['name', 'string', 'min' => 4, 'max' => 24],
            ['name', 'match', 'pattern' => '/^[a-z][a-z0-9_]+$/', 'on' => 'signup'],
            ['name', 'unique',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'This value has already been taken')],
            ['email', 'email'],
            ['wallet_advcash', 'email'],
            ['repeat', 'compare', 'compareAttribute' => 'password'],
            ['wallet_perfect', 'match', 'pattern' => static::PERFECT, 'message' =>
                'Должен быть кошелек евро в формате E1234567'],
            ['wallet_nix', 'match', 'pattern' => static::NIX, 'message' =>
                'Должен быть кошелек евро в формате E12345678901234'],
            ['ref_number', 'integer'],
            [['account', 'blocked'], 'default', 'value' => 0],
            ['pin', 'match', 'pattern' => '/^\d{4}$/', 'message' => 'Введите 4 цифры'],
            [['image', 'avatar'], 'string'],
            ['ref_name', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'name',
                'message' => 'Такого пользователя не существует'],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['email', 'wallet_perfect', 'wallet_nix', 'wallet_advcash',
                'forename', 'surname', 'city', 'phone', 'skype', 'pin', 'image', 'avatar'],
            'signup'  => ['name', 'email', 'password', 'repeat', 'wallet_perfect', 'ref_name', 'ref_number',
                'wallet_perfect', 'wallet_nix', 'wallet_advcash', 'forename', 'surname', 'city',
                'phone', 'skype', 'perfect', 'pin', 'delete_avatar'],
            'admin'   => ['name', 'email', 'status', 'wallet_perfect', 'ref_name', 'ref_number',
                'wallet_perfect', 'wallet_nix', 'wallet_advcash', 'forename', 'surname', 'city', 'phone', 'skype',
                'pin', 'image', 'avatar', 'account', 'blocked', 'delete_avatar'],
        ];
    }

    public function attributeLabels() {
        $perfect = 'Евро кошелек Perfect Money';
        return [
            'name' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'repeat' => Yii::t('app', 'Repeat'),
            'status' => Yii::t('app', 'Status'),
            'account' => Yii::t('app', 'Account'),
            'last_access' => 'Последняя активность',
            'ref_name' => 'Спонсор',
            'surname' => 'Имя',
            'forename' => 'Фамилия',
            'city' => 'Город',
            'phone' => 'Телефон',
            'perfect' => $perfect,
            'wallet_perfect' => $perfect,
            'wallet_nix' => Yii::t('app', 'Евро кошелек NixMoney'),
            'wallet_advcash' => Yii::t('app', 'AdvCash email'),
            'blocked' => 'Заблокирование деньги',
            'pin' => 'Транзакционний пароль',
            'image' => 'Аватар',
            'delete_avatar' => 'удалить аватар',
        ];
    }

    public function behaviors() {
        return [
            Journal::class
        ];
    }

    public function init() {
        parent::init();
        if (isset(Yii::$app->user)
            && Yii::$app->user instanceof WebUser
            && !Yii::$app->user->getIsGuest()
            && static::ADMIN == Yii::$app->user->identity->status) {
            $this->scenario = 'admin';
        }
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id) {
        return parent::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new Exception('Not implemented findIdentityByAccessToken');
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth;
    }

    public function validateAuthKey($authKey) {
        return $authKey == $this->auth;
    }

    public function validatePassword($password) {
        return password_verify($password, $this->hash);
    }

    public function getPassword() {
        return $this->_password;
    }

    public function setPassword($value) {
        $this->hash = password_hash($value, PASSWORD_DEFAULT);
        $this->_password = $value;
    }

    public function generateAuthKey() {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function generateCode() {
        $this->code = Yii::$app->security->generateRandomString(64);
    }

    public function isTeam() {
        return static::TEAM == $this->status || static::ADMIN == $this->status || static::MANAGER == $this->status;
    }

    public function isManager() {
        return static::ADMIN == $this->status || static::MANAGER == $this->status;
    }

    public function isAdmin() {
        return static::ADMIN == $this->status;
    }

    public function __toString() {
        return $this->name;
    }

    public function sendEmail($params, $template = 'basic') {
        return God::mail($this->email, $template, $params);
    }

    public function canLogin() {
        return Record::find()->andWhere([
            'object_id' => $this->id,
            'event' => 'login_fail'
        ])
            ->andWhere('(NOW() - "time") < interval \'5 minutes\'')
            ->count() < Settings::get('common', 'login_fails');
    }

    /**
     * @return ActiveRecord
     */
    public static function online() {
        return static::find()
            ->andWhere('(NOW() - "last_access") < interval \'15 minutes\'');
    }

    public function getBundle() {
        if (!$this->_info) {
            $this->_info = unserialize(stream_get_contents($this->data));
        }
        return $this->_info;
    }

    public function setBundle($value) {
        $value = empty($value) ? null : serialize($value);
        $this->data = $value;
        $this->_info = $value;
    }

    public function setBundleFromAttributes($names, $restore = false) {
        $bundle = [];
        foreach($names as $name) {
            $bundle[$name] = $this->$name;
            if ($restore) {
                $this->$name = $this->getOldAttribute($name);
            }
        }
        $this->setBundle($bundle);
    }

    public function journalView() {
        return __DIR__ . '/../views/user/journal.php';
    }

    public function getReferral() {
        return $this->hasOne(User::class, ['name' => 'ref_name']);
    }

    public function hasSponsors() {
        return true;
        /*
        $count = SQL::queryCell('SELECT count(*) FROM matrix_node JOIN "user" ON user_name = "name"
          WHERE ref_name = :name', [
            ':name' => $this->name
        ]);
        if ($count >= 3) {
            return true;
        }
        $count = SQL::queryCell('SELECT count(*) FROM matrix_node JOUN WHERE user_name = :name AND type_id = 3
            AND reinvest_from is null
           AND now() - time < interval \'1 month\'', [
            ':name' => $this->name
        ]);
        return $count > 0;
        */
    }

    public function countSponsors() {
        return SQL::queryCell('SELECT count(*) FROM referral_matrix WHERE root = :name AND level <= 5 AND level > 0', [
            ':name' => $this->name
        ]);
    }

    public function getAvatarPath() {
        return Yii::getAlias("@app/web/avatars/$this->name.$this->avatar");
    }
}
