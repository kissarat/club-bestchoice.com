<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Visit extends ActiveRecord {
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'time' => Yii::t('app', 'Time'),
            'spend' => Yii::t('app', 'Spend'),
            'user_name' => Yii::t('app', 'Username'),
            'path' => Yii::t('app', 'Path'),
            'ip' => 'IP',
            'agent' => Yii::t('app', 'Browser'),
        ];
    }

    public static function getStrongUserAgent($agent) {
        $agent = str_replace('Mozilla/5.0', '', $agent);
        $agent = preg_replace('/AppleWebKit\/\d+\.\d+ \(KHTML, like Gecko\)/', '', $agent);
        return preg_replace('/(Windows|Linux|Android|Firefox|Chrome)/', '<strong>$1</strong>', $agent);
    }
}
