<?php
/**
 * @link http://zenothing.com/
 */

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if ('page' == $model->scenario) {
        echo Html::tag('div', 'http://' . substr($_SERVER['HTTP_HOST'], 6) . '/page/' .
        Html::activeTextInput($model, 'name'), ['class' => 'form-group']);
    }
    ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'keywords') ?>
    <?= $form->field($model, 'summary')->textarea() ?>
    <?= $form->field($model, 'content')->widget(CKEditor::class, [
        'preset' => 'full'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord
            ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
