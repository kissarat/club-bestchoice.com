<?php
/**
 * @link http://zenothing.com/
 */

use app\modules\article\models\Article;
use app\widgets\Ext;
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\article\models\Article */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
?>
<div class="article-index middle">
    <?= Ext::stamp() ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php /*echo $this->render('_search', ['model' => $searchModel]);*/
    if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager()) {
        echo Html::tag('p', implode("\n", [
            Html::a(Yii::t('app', 'Create'), ['/article/article/create'], ['class' => 'btn btn-success']),
            Html::a(Yii::t('app', 'Export'), ['/article/article/export'], ['class' => 'btn btn-primary'])
        ]));
    }
    ?>

    <div class="list-group">
        <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => function(Article $model) {
                echo Html::tag('div',
                    Html::a($model->title, ['view', 'id' => $model->id])
                    . Html::tag('p', $model->summary),
                    ['class' => 'list-group-item']);
            }
        ]);
        ?>
    </div>

</div>
