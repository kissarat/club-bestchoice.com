<?php
/**
 * @link http://zenothing.com/
 */

use app\widgets\Ext;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $models \app\modules\faq\models\Faq [] */

$this->title = Yii::t('app', 'FAQ');
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
?>
<div class="faq-index middle">
    <?php
    echo Ext::stamp();

    if ('main' == Yii::$app->layout):
    ?>
    <div class="black">
        <div>Вопрос-Ответ</div>
        <div>
            Перед обращением в службу поддержки мы настоятельно рекомендуем ознакомиться
            с часто задаваемыми вопросами — вполне вероятно, среди них найдется решение
            и вашей проблемы.
        </div>
    </div>
    <?php
    endif;

    if ($manager): ?>
        <p class="form-group">
            <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Export'), ['/faq/faq/export'], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif ?>

    <div class="contents">
        <?php foreach($models as $model): ?>
            <a href="#<?= $model->id ?>"><?= $model->question ?></a>
        <?php endforeach ?>
    </div>

    <dl>
        <?php foreach($models as $model): ?>
            <dt id="<?= $model->id ?>">
                <?= $model->question ?>
                <?php
                if ($manager) {
                    echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) . ' ';
                    echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger btn-sm',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]);
                }
                ?>
            </dt>
            <dd><?= $model->answer ?></dd>
        <?php endforeach ?>
    </dl>

</div>
