<?php
/**
 * @link http://zenothing.com/
 */

use app\modules\feedback\models\Feedback;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\feedback\models\search\Feedback */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedbacks');
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?php
        if (Yii::$app->user->getIsGuest() || !Yii::$app->user->identity->isManager()) {
            echo Html::a('Создать тикет', ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    <form id="ticket">
        <input type="text" size="24" maxlength="24" />
        <button type="button">Показать тикет</button>
    </form>
    </p>

    <?php
    if (Yii::$app->user->getIsGuest()) {
        if (isset($_COOKIE['ticket'])) {
            echo Html::a('Последний тикет', ['ticket', 'ticket' => $_COOKIE['ticket']]);
        }
    }
    else {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['class' => 'id']
                ],
                [
                    'attribute' => 'id',
                    'label' => 'Пользователь',
                    'format' => 'html',
                    'value' => function (Feedback $model) {
                        $first = $model->getFirst();
                        return $first->user_name ? Html::a($first->user_name) : null;
                    }
                ],
                [
                    'attribute' => 'email',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->email) {
                            return Html::a($model->email, 'mailto:' . $model->email);
                        } else {
                            return Yii::t('app', 'registered');
                        }
                    }
                ],

                'subject:ntext',

                [
                    'label' => 'Действия',
                    'format' => 'html',
                    'value' => function (Feedback $model) {
                        return Html::a('Просмотр', ['ticket', 'ticket' => $model->ticket]);
                    }
                ]
            ],
        ]);
    }
    ?>

</div>
