<?php

namespace app\modules\invoice\components;


use app\helpers\Soap;

class Advcash extends Soap {
    const URL = 'https://wallet.advcash.com:8443/wsm/merchantWebService?wsdl';
    const EMAIL = 'zhmurko90@gmail.com';
    const SECRET = 'UT|uCai4lie2feirooX)oph5';
    const USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36';

    public function init() {
        $this->namespace = 'http://wsm.advcash/';
        $this->url = 'https://wallet.advcash.com:8443/wsm/merchantWebService';
    }

    public function getResponse() {
        $args = func_get_args();
        $name = $args[0];
        $args[0] = [
            'apiName' => 'default',
            'accountEmail' => static::EMAIL,
            'authenticationToken' => $this->getToken()
        ];
        return static::request($name, $args);
    }

    public function sendMoney($email, $amount) {
        return $this->getResponse('sendMoney', [
            'amount' => $amount,
            'currency' => 'EUR',
            'email' => $email,
            'savePaymentTemplate' => 'false'
        ]);
    }

    public function getToken() {
        return hash("sha256", static::SECRET . ':' . gmdate('Ymd:H'));
    }
}
