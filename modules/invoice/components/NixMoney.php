<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\invoice\components;


use app\models\Settings;
use Yii;
use yii\base\Component;
use yii\web\Response;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * Class PerfectMoney
 * @package app\components
 * @property int id
 * @property string password
 * @property string wallet
 * @property string merchant
 * @property string alternateSecret
 */
class NixMoney extends Component {
    public function buildQuery($params, $end = null, $start = null) {
        if (!$end)
            $end = time();
        if (!$start)
            $start = $end - 3600 * 24;
        $start = explode('-', gmdate('Y-m-d', $start));
        $end = explode('-', gmdate('Y-m-d', $end));
        $nix = Settings::getCategory('nix');
        $headers = null;
        $params = array_merge([
            'ACCOUNTID' => $nix['email'],
            'PASSPHRASE' => $nix['password'],
            'STARTYEAR' => $start[0],
            'STARTMONTH' => $start[1],
            'STARTDAY' => $start[2],
            'ENDYEAR' => $end[0],
            'ENDMONTH' => $end[1],
            'ENDDAY' => $end[2],
        ], $params);
        return $nix['server'] . 'history?' . http_build_query($params);
    }

    public function findPayment($params, $end = null, $start = null) {
        $file = fopen($this->buildQuery($params, $end, $start), 'r');
        $rows = [];
        $json = [];
        while($record = fgetcsv($file)) {
            if (empty($headers)) {
                if (count($record) <= 1) {
                    $json['error'] = trim(str_replace('ERROR:', '', $record[0]));
                    break;
                }
                $json['count'] = $record;
                $headers = $record;
            }
            else {
                $row = [];
                foreach($record as $i => $value) {
                    $row[$headers[$i]] = $value;
                }
                $rows[$row['Payment ID']] = &$row;
                unset($row['Payment ID']);
            }
        }
        fclose($file);
        $json['count'] = count($rows);
        $json['invoices'] = $rows;
        if (YII_DEBUG) {
            $json['query'] = $params;
        }
        return $json;
    }
}
