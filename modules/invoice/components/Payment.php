<?php
namespace app\modules\invoice\components;


use yii\base\Model;

class Payment extends Model {
    public $id;
    public $batch;
    private $_time;
    public $amount;
    public $fee;
    public $sender;
    public $receiver;
    public $memo;

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'batch' => 'Транзакция',
            'time' => 'Время',
            'amount' => 'Количество',
            'fee' => 'Комиссия',
            'sender' => 'Отправитель',
            'receiver' => 'Получатель',
            'memo' => 'Заметка',
        ];
    }

    public static function labels() {
        return [
            'id' => 'Payment ID',
            'batch' => 'Batch',
            'time' => 'Time',
            'amount' => 'Amount',
            'fee' => 'Fee',
            'sender' => 'Payee Account',
            'receiver' => 'Payer Account',
            'memo' => 'Memo',
        ];
    }

    public static function fromArray(array $array) {
        $record = new static();
        foreach($array as $key => $value)
            $record->$key = $value;
        return $record;
    }

    public function setTime($value) {
        if (is_string($value))
            $value = strtotime($value);
        $this->_time = $value;
    }

    public function getTime() {
        return $this->_time;
    }
}
