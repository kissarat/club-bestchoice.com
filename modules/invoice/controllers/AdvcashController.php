<?php

namespace app\modules\invoice\controllers;


use app\behaviors\Access;
use app\behaviors\NoTokenValidation;
use app\models\Settings;
use app\modules\invoice\models\Invoice;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class AdvcashController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'success' => ['post'],
                    'status' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'manager' => ['balances']
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::class,
                'only' => ['success', 'status', 'fail', 'sha256'],
            ]
        ];
    }

    public function actionBalances() {
        $wallets = [];
        $list = Yii::$app->advcash->getResponse('getBalances');
        for($i = 0; $i < $list->length; $i++) {
            $wallet = $list->item($i);
            $wallets[$wallet->childNodes->item(1)->firstChild->data] =
                (float) $wallet->childNodes->item(0)->firstChild->data;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $wallets;
    }

    public function actionHash() {
        $invoice = static::loadModel($_GET['id']);
        $invoice->user_name = Yii::$app->user->identity->name;
        $invoice->amount = (float) $_GET['amount'];
        if ($invoice->save()) {
            $string = implode(':', [
                Settings::get('advcash', 'email'),
                Yii::$app->name,
                $_REQUEST['amount'],
                'EUR',
                Settings::get('advcash', 'secret'),
                $_REQUEST['id'],
            ]);
            return hash('sha256', $string);
        }
        else {
            http_response_code(400);
            return $invoice->__debuginfo();
        }
    }

    public function actionSha256() {
        return hash('sha256', file_get_contents('php://input'));
    }

    /**
     * @param string $id
     * @return Invoice
     */
    protected static function loadModel($id = null) {
        $_id = $id ? $id : $_POST['ac_order_id'];
        $invoice = Invoice::findOne($_id);
        if (empty($invoice)) {
            $invoice = new Invoice([
                'id' => $_id,
                'type' => 'advcash'
            ]);
        }
        if (isset($_POST['USER_NAME'])) {
            $invoice->user_name = $_POST['USER_NAME'];
        }
        if (empty($id)) {
            $invoice->logRequest();
        }
        return $invoice;
    }

    public function actionStatus() {
        $invoice = static::loadModel();
        if (in_array($invoice->status, ['verified', 'success'])) {
            http_response_code(302);
            return '';
        }
        if ('EUR' == $_POST['ac_merchant_currency']
            && Settings::get('advcash', 'wallet') == $_POST['ac_dest_wallet']
            && 'COMPLETED' == $_POST['ac_transaction_status']) {

            $string = implode(':', [
                $_POST['ac_transfer'],
                $_POST['ac_start_date'],
                $_POST['ac_sci_name'],
                $_POST['ac_src_wallet'],
                $_POST['ac_dest_wallet'],
                $_POST['ac_order_id'],
                $_POST['ac_amount'],
                $_POST['ac_merchant_currency'],
                Settings::get('advcash', 'secret'),
            ]);

            if (hash('sha256', $string) == $_POST['ac_hash']) {
                $invoice->batch = $_POST['ac_transfer'];
                $invoice->wallet = $_POST['ac_src_wallet'];
                $invoice->amount = (float) $_POST['ac_merchant_amount'];
                if ($invoice->saveStatus('verified')) {
                    http_response_code(201);
                } else {
                    http_response_code(502);
                    return $invoice->__debuginfo();
                }
            }
        }
        http_response_code(400);
        return '';
    }

    public function actionSuccess() {
        $invoice = static::loadModel();
        if ('verified' == $invoice->status) {
            $invoice->user->account += $invoice->amount;
            if ($invoice->saveStatus('success') && $invoice->user->save(true, ['account'])) {
                Yii::$app->session->addFlash('success', "Платеж осуществлен");
                if (isset($_POST['TYPE_ID'])) {
                    return $this->redirect(['/matrix/matrix/open', 'id' => (int) $_POST['TYPE_ID']]);
                }
                else {
                    return $this->redirect(['/user/view', 'name' => $invoice->user_name]);
                }
            }
            else {
                Yii::$app->session->addFlash('error', $invoice->__debuginfo());
            }
        }
        elseif ('success' == $invoice->status) {
            Yii::$app->session->addFlash('error', "Платеж уже осуществлялся");
        }
        else {
            Yii::$app->session->addFlash('error', "Платеж не подтвержден");
        }
        return $this->redirect(['/invoice/invoice/view', 'id' => $invoice->id]);
    }

    public function actionFail() {
        return file_get_contents('php://input');
    }
}
