<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\invoice\controllers;

use app\behaviors\Access;
use app\behaviors\Journal;
use app\behaviors\NoTokenValidation;
use app\helpers\JournalException;
use app\helpers\SQL;
use app\models\Record;
use app\models\Settings;
use app\models\User;
use app\modules\invoice\models\InvoiceSum;
use app\modules\invoice\models\Transfer;
use app\modules\invoice\models\Withdrawal;
use app\modules\invoice\models\Invoice;
use app\modules\invoice\models\search\Invoice as InvoiceSearch;
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class InvoiceController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'success' => ['post'],
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['fail', 'index', 'withdraw', 'create'],
                'manager' => ['summary', 'update', 'delete']
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::class,
                'only' => ['success', 'fail'],
            ]
        ];
    }

    public function actionIndex($user = null)
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        if (!$identity->isManager() && $user != $identity->name) {
            Yii::$app->session->addFlash('error', 'Вы можете видеть только свои платежы');
            return $this->redirect(['index', 'user' => $identity->name]);
        }
        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user
        ]);
    }

    public function actionSummary($user = null)
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        if (!$identity->isManager() && $user != $identity->name) {
            Yii::$app->session->addFlash('error', 'Вы можете видеть только свои платежы');
            return $this->redirect(['index', 'user' => $identity->name]);
        }

        return $this->render('summary', [
            'dataProvider' => new ActiveDataProvider([
                'query' => InvoiceSum::find(),
                'sort' => ['defaultOrder' => ['withdraw' => SORT_DESC]]
            ]),
            'totals' => SQL::querySingle(
                'SELECT sum(payment) AS payment, sum(withdraw) AS withdraw, sum(balance) AS balance FROM invoice_sum'),
            'user' => $user
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager() && Settings::get('common', 'withdrawal_confirmation')) {
            Yii::$app->session->addFlash('warning', 'Вывод будет осуществлен после подтверждения администрацией');
        }
        return $this->render('view', [
            'model' => $model,
            'user' => $this->restrictUser(),
            'journal' => new ActiveDataProvider([
                'query' => Record::find()->where([
                    'type' => 'invoice',
                    'object_id' => $id
                ]),
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_DESC
                    ]
                ]
            ])
        ]);
    }

    public function actionCreate($scenario = 'payment', $amount = null)
    {
        $model = new Invoice([
            'user_name' => Yii::$app->user->identity->name,
            'scenario' => $scenario,
            'status' => 'create'
        ]);

        if ($amount) {
            $model->amount = $amount;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->id = uuid_create();
            if ('withdraw' == $model->scenario) {
                $model->amount = -abs($model->amount);
                if (abs($model->amount) > $model->user->account) {
                    Yii::$app->session->addFlash('error', Yii::t('app', Invoice::$statuses['insufficient_funds']));
                    $model->amount = $model->user->account;
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'user' => $this->restrictUser()
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'manage';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $invoice = $this->findModel($id);
        if ('success' == $invoice->status) {
            $invoice->saveStatus('delete');
        } else {
            $invoice->delete();
        }

        return $this->redirect(['index']);
    }

    public function actionSuccess($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = Invoice::findOne($id);
        if (!$invoice) {
            $invoice = new Invoice([
                'id' => $id
            ]);
        }

        if (!$invoice->isNewRecord && 'success' == $invoice->status) {
            $transaction->rollBack();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Payment has already done previously'));
            return $this->redirect(['view', 'id' => $invoice->id]);
        } else {
//            $message = null;
//            if (static::checkPerfect()) {
//                if (!Settings::get('common', 'perfect')) {
//                    $message = 'Perfect Money отключен';
//                } elseif (!static::perfect($invoice)) {
//                    $message = 'Неизвестная ошибка во время проведения платежа Perfect Money';
//                }
//            } elseif (static::checkNix()) {
//                if (!Settings::get('common', 'nix')) {
//                    $message = 'NixMoney отключен';
//                } elseif (!static::nix($invoice)) {
//                    $message = 'Неизвестная ошибка во время проведения платежа NixMoney';
//                }
//            } else {
//                $message = 'Неизвестная платежная система';
//            }

            if (static::perfect($invoice)) {
                return $this->success($transaction, $invoice);
            }
//            if (empty(Yii::$app->session->getFlash('error'))) {
//                Yii::$app->session->addFlash('error', $message);
//            }
            return $this->render('@app/views/home/index', [
                'data' => array_merge($invoice->getErrors(), $_POST)
            ]);
        }
    }

    public static function checkPerfect()
    {
        return isset($_POST['PAYMENT_ID'])
        && isset($_POST['PAYEE_ACCOUNT'])
        && isset($_POST['PAYMENT_AMOUNT'])
        && isset($_POST['PAYMENT_UNITS'])
        && isset($_POST['PAYMENT_BATCH_NUM'])
        && isset($_POST['PAYER_ACCOUNT'])
        && isset($_POST['TIMESTAMPGMT']);
    }

    public static function checkNix()
    {
        return isset($_POST['PAYEE_ACCOUNT'])
        && isset($_POST['PAYMENT_AMOUNT'])
        && isset($_POST['PAYMENT_UNITS'])
        && isset($_POST['PAYMENT_BATCH_NUM'])
        && isset($_POST['PAYER_ACCOUNT']);
    }

    public static function nix(Invoice $invoice)
    {
        if ('verified' != $invoice->status) {
            Yii::$app->session->addFlash('error', 'Платеж не подтвержден');
        } elseif (!($invoice->type == 'nix'
            && $invoice->batch == $_POST['PAYMENT_BATCH_NUM']
            && $invoice->wallet == $_POST['PAYER_ACCOUNT']
            && $invoice->amount == $_POST['PAYMENT_AMOUNT'])
        ) {
            Yii::$app->session->addFlash('error', 'Неправильние данние');
        } else {
            return true;
        }
        return false;
    }

    public static function verifyPerfect($hash, $wallet)
    {
        $string = implode(':', [
            $_POST['PAYMENT_ID'],
            $_POST['PAYEE_ACCOUNT'],
            $_POST['PAYMENT_AMOUNT'],
            $_POST['PAYMENT_UNITS'],
            $_POST['PAYMENT_BATCH_NUM'],
            $_POST['PAYER_ACCOUNT'],
            $hash,
            $_POST['TIMESTAMPGMT']
        ]);
        return strtoupper(md5($string)) == $_POST['V2_HASH']
        && $wallet == $_POST['PAYEE_ACCOUNT']
        && 'EUR' == $_POST['PAYMENT_UNITS'];
    }

    public static function perfect(Invoice $invoice)
    {
        if (!static::verifyPerfect(Yii::$app->perfect->hashAlternateSecret(), Settings::get('perfect', 'wallet'))) {
            return false;
        }
        $invoice->batch = $_POST['PAYMENT_BATCH_NUM'];
        $invoice->type = 'perfect';
        $invoice->wallet = $_POST['PAYER_ACCOUNT'];
        $invoice->amount = $_POST['PAYMENT_AMOUNT'];
        $name = isset($_POST['USER_NAME'])
            ? $_POST['USER_NAME']
            : User::findOne(['wallet_perfect' => $_POST['PAYER_ACCOUNT']]);
        if ($name) {
            $invoice->user_name = $name;
        } else {
            throw new ForbiddenHttpException('Пользователь не найден, вы не авторизовались');
        }
        if ($invoice->id != $_POST['PAYMENT_ID']) {
            $invoice->saveStatus('invalid_id');
            Yii::$app->session->addFlasyiih('error', Yii::t('app', Invoice::$statuses['invalid_id']));
        } elseif (!Yii::$app->user->getIsGuest() && $invoice->user_name != Yii::$app->user->identity->name) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can only change the status of your payments'));
        } else {
            $payment = Yii::$app->perfect->findPayment($invoice->id);
            if ($payment && $payment->sender == $invoice->wallet && $payment->batch == $invoice->batch &&
                $invoice->amount == $payment->amount
            ) {
                return true;
            } else {
                Yii::$app->session->addFlash('error', 'Платеж не потверджен');
            }
        }
        return false;
    }

    public function success(Transaction $transaction, Invoice $invoice)
    {
        /** @var Type $type */
        $type = null;
        if (isset($_GET['type_id'])) {
            $type = Type::findOne($_GET['type_id']);
            if ($type) {
                $remain = ($invoice->user->account + $invoice->amount) - $type->price;
                if ($remain >= 0) {
                    if ($type->open($invoice->user_name)) {
                        if ($invoice->saveStatus('success')) {
                            $transaction->commit();
                            Yii::$app->session->addFlash('success', "Пакет $type->name открыт");
                        } else {
                            Yii::$app->session->addFlash('error', 'Невозможно сохранить платеж');
                            return $this->render('@app/views/home/index', ['data' => $invoice->getErrors()]);
                        }
                    } else {
                        Yii::$app->session->addFlash('error', "Пакет $type->name не открыт");
                    }
                } else {
                    $remain = $invoice->amount;
                    Yii::$app->session->addFlash('error', 'Недостаточная сумма для оплаты плана');
                }
                if ($remain > 0) {
                    $invoice->user->account += $remain;
                    if ($invoice->user->save(true, ['account'])) {
                        Yii::$app->session->addFlash('info', "На ваш счет начислено " . $remain);
                    } else {
                        Yii::$app->session->addFlash('error', 'Невозможно сохранить счет пользователя');
                    }
                }
                if ($transaction->getIsActive()) {
                    $transaction->commit();
                }
                return $this->redirect(['/matrix/matrix/plans', 'user' => $invoice->user_name]);
            } else {
                Yii::$app->session->addFlash('error', 'План не найден');
            }
        }

        $invoice->user->account += $invoice->amount;
        if ($invoice->saveStatus('success') && $invoice->user->save(true, ['account'])) {
            $transaction->commit();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Payment {id} completed', [
                'id' => $invoice->id,
            ]));
        } else {
            $transaction->rollBack();
            Yii::$app->session->addFlash('error', 'Невозможно сохранить платеж');
            return $this->render('@app/views/home/index', [
                'data' => [
                    'invoice' => $invoice->getErrors(),
                    'user' => $invoice->user->getErrors(),
                    'user_name' => $invoice->user_name
                ]
            ]);
        }
        if ($transaction->getIsActive()) {
            $transaction->commit();
        }
        return $this->redirect(['view', 'id' => $invoice->id]);
    }

    public function actionNix($id)
    {
        if (!Settings::get('common', 'nix')) {
            return 'NixMoney is turn off';
        }
        if (!static::verifyPerfect(
            strtoupper(md5(Settings::get('perfect', 'wallet'))),
            Settings::get('perfect', 'wallet'))
        ) {
            return 'Error';
        }

        $transaction = Yii::$app->db->beginTransaction();
        $invoice = Invoice::findOne($id);
        if (!$invoice) {
            $invoice = new Invoice([
                'id' => $id
            ]);
        }

        if (!$invoice->isNewRecord && 'success' == $invoice->status) {
            $transaction->rollBack();
            return 'Already done';
        }

        $invoice->batch = $_POST['PAYMENT_BATCH_NUM'];
        $invoice->type = 'nix';
        $invoice->wallet = $_POST['PAYER_ACCOUNT'];
        $invoice->amount = $_POST['PAYMENT_AMOUNT'];
        $name = isset($_POST['USER_NAME'])
            ? $_POST['USER_NAME']
            : User::findOne(['wallet_perfect' => $_POST['PAYER_ACCOUNT']]);
        if ($name) {
            $invoice->user_name = $name;
        } else {
            return 'User not found';
        }
        if ($invoice->saveStatus('verified')) {
            $transaction->commit();
            return 'Success';
        } else {
            return 'Cannot save payment: ' . $invoice->__debuginfo();
        }
    }

    public function actionFail($id = null)
    {
        $invoice = null;
        if ($id) {
            $invoice = $this->findModel($id);
        }

        if ($invoice && Yii::$app->user->identity->name != $invoice->user_name) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can only change the status of your payments'));
        } else {
            if ($invoice) {
                $invoice->saveStatus('cancel');
            }
            Yii::$app->session->addFlash('error', Yii::t('app', 'You cancel payment'));
        }

        if ($invoice) {
            return $this->render('view', [
                'model' => $invoice
            ]);
        } else {
            return $this->render('@app/views/home/index');
        }
    }


    public function actionWithdraw($id)
    {
        if (!Yii::$app->user->identity->isManager() && Settings::get('common', 'withdrawal_confirmation')) {
            throw new ForbiddenHttpException('Подтвердить вывод может только администрация');
        }
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->findModel($id);
        try {
            $invoice->scenario = 'withdraw';
            if ('success' == $invoice->status) {
                $transaction->rollBack();
                Yii::$app->session->addFlash('info', Yii::t('app', 'Payment has already done previously'));
            } elseif (abs($invoice->amount) > $invoice->user->account) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Insufficient funds'));
            } else {
                if (!Settings::get('perfect', 'id')) {
                    static::withdrawSuccessful($invoice, $transaction);
                    return $this->redirect(['view', 'id' => $invoice->id]);
                } else {
                    $withdrawal = Withdrawal::fromInvoice($invoice);
                    $response = file_get_contents('https://perfectmoney.is/acct/confirm.asp?' . $withdrawal);
                    if (!preg_match('/<h1>(.*)<\/h1>/', $response, $result)) {
                        $invoice->throwJournalException($response);
                    } elseif ('Spend' != $result[1]) {
                        $invoice->throwJournalException($result[1]);
                    } elseif (!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/",
                        $response, $result, PREG_SET_ORDER)
                    ) {
                        $invoice->throwJournalException($response);
                    } else {
                        $info = [];
                        foreach ($result as $row) {
                            $info[$row[1]] = $row[2];
                        }
                        if (isset($info["ERROR"])) {
                            $invoice->throwJournalException($info["ERROR"]);
                        } elseif ($info['PAYMENT_AMOUNT'] != abs($invoice->amount)) {
                            $invoice->throwJournalException(Yii::t('app', 'Invalid amount') . ' ' . $info['PAYMENT_AMOUNT']);
                        } else {
                            $invoice->batch = $info['PAYMENT_BATCH_NUM'];
                            $invoice->wallet = $info['Payee_Account'];
                            static::withdrawSuccessful($invoice, $transaction);
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            $transaction->rollBack();
            if ($ex instanceof JournalException) {
                Journal::write($ex->type, $ex->event, $ex->object_id, $ex->getMessage());
            }
            Yii::$app->session->addFlash('error', $ex->getMessage());
        }
        return $this->redirect(['view', 'id' => $invoice->id]);
    }

    protected static function withdrawSuccessful(Invoice $invoice, Transaction $transaction)
    {
        $invoice->status = 'success';
        $invoice->user->account -= abs($invoice->amount);
        if ($invoice->user->save(true, ['account']) && $invoice->save(false)) {
            $transaction->commit();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Payment #{id} completed', [
                'id' => $invoice->number,
            ]));
        } else {
            Yii::$app->session->addFlash('error', json_encode(
                array_merge($invoice->user->errors, $invoice->errors),
                JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        }
    }

    public function actionPay($id = null)
    {
        $vars = [];
        if ($id && ($model = is_numeric($id) ? Type::findOne($id) : $this->findModel($id))) {
            $vars['model'] = $model;
        }
        return $this->render('pay', $vars);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            $model->scenario = $model->amount < 0 ? 'withdraw' : 'payment';
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function restrictUser($default = null)
    {
        return !Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()
            ? Yii::$app->user->identity->name
            : $default;
    }
}
