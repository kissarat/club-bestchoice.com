<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\invoice\controllers;

use app\behaviors\Access;
use app\behaviors\Journal;
use app\behaviors\NoTokenValidation;
use app\helpers\JournalException;
use app\models\Record;
use app\models\Settings;
use app\models\User;
use app\modules\invoice\models\Withdrawal;
use app\modules\invoice\models\Invoice;
use app\modules\invoice\models\search\Invoice as InvoiceSearch;
use app\modules\matrix\models\Type;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ServerErrorHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class InvoiceController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'success' => ['post'],
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['fail', 'index', 'withdraw', 'create', 'batch', 'pay'],
                'manager' => ['update', 'delete']
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::class,
                'only' => ['success', 'fail', 'nix', 'batch'],
            ]
        ];
    }

    public function actionIndex($user = null) {
        $identity = Yii::$app->user->identity;
        if (!$identity->isManager() && $user != $identity->name) {
            Yii::$app->session->addFlash('error', 'Вы можете видеть только свои платежы');
            return $this->redirect(['index', 'user' => $identity->name]);
        }
        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager() && Settings::get('common', 'withdrawal_confirmation')) {
            Yii::$app->session->addFlash('warning', 'Вывод будет осуществлен после подтверждения администрацией');
        }
        return $this->render('view', [
            'model' => $model,
            'user' => $this->restrictUser(),
            'journal' => new ActiveDataProvider([
                'query' => Record::find()->where([
                    'type' => 'invoice',
                    'object_id' => $id
                ]),
                'sort' =>  [
                    'defaultOrder' => [
                        'time' => SORT_DESC
                    ]
                ]
            ])
        ]);
    }

    public function actionCreate($scenario = 'payment', $amount = null) {
        $model = new Invoice([
            'user_name' => Yii::$app->user->identity->name,
            'scenario' => $scenario,
            'status' => 'create'
        ]);

        if ($amount) {
            $model->amount = $amount;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->id = uuid_create();
            if ('withdraw' == $model->scenario) {
                if (!Yii::$app->user->identity->isManager() && $model->user->pin != $model->pin) {
                    $model->addError('pin', 'Неправильный пароль');
                }
                else {
                    $model->amount = -abs($model->amount);
                    if (abs($model->amount) > $model->user->account) {
                        Yii::$app->session->addFlash('error', Yii::t('app', Invoice::$statuses['insufficient_funds']));
                        $model->amount = $model->user->account;
                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                }
            }
            if (!$model->hasErrors() && $model->save()) {
                if (Yii::$app->user->identity->isManager()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else {
                    return $this->redirect(['withdraw', 'id' => $model->id]);
                }
            }
        }
        return $this->render('create', [
            'model' => $model,
            'user' => $this->restrictUser()
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = 'manage';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $invoice = $this->findModel($id);
        if ('success' == $invoice->status) {
            $invoice->saveStatus('delete');
        }
        else {
            $invoice->delete();
        }

        return $this->redirect(['index']);
    }

    public function actionSuccess($id) {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = Invoice::findOne($id);
        if (!$invoice) {
            $invoice = new Invoice([
                'id' => $id
            ]);
        }

        if (!$invoice->isNewRecord && 'success' == $invoice->status) {
            $transaction->rollBack();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Payment has already done previously'));
            return $this->redirect(['view', 'id' => $invoice->id]);
        }
        else {
            $message = null;
            if (static::checkPerfect()) {
                if (!Settings::get('common', 'perfect')) {
                    $message = 'Perfect Money отключен';
                }
                elseif (!static::perfect($invoice)) {
                    $message = 'Неизвестная ошибка во время проведения платежа Perfect Money';
                }
            }
            elseif (static::checkNix()) {
                if (!Settings::get('common', 'nix')) {
                    $message = 'NixMoney отключен';
                }
                elseif (!static::nix($invoice)) {
                    $message = 'Неизвестная ошибка во время проведения платежа NixMoney';
                }
            }
            else {
                $message = 'Неизвестная платежная система';
            }

            if (!$message) {
                return $this->success($transaction, $invoice);
            }
            if (empty(Yii::$app->session->getFlash('error'))) {
                Yii::$app->session->addFlash('error', $message);
            }
            return $this->render('@app/views/home/index', ['data' => array_merge($invoice->getErrors(), $_REQUEST)]);
        }
    }

    public function actionBatch($batch) {
        /** @var Invoice $invoice */
        $invoice = Invoice::findOne(['batch' => $batch]);
        if (Yii::$app->user->identity->name != $invoice->user_name) {
            throw new ForbiddenHttpException('Вам доступны только вашы платежы');
        }
        return $this->redirect(['success', 'id' => $invoice->id]);
    }

    protected static function checkPerfect() {
        return isset($_POST['PAYMENT_ID'])
        && isset($_POST['PAYEE_ACCOUNT'])
        && isset($_POST['PAYMENT_AMOUNT'])
        && isset($_POST['PAYMENT_UNITS'])
        && isset($_POST['PAYMENT_BATCH_NUM'])
        && isset($_POST['PAYER_ACCOUNT'])
        && isset($_POST['TIMESTAMPGMT']);
    }

    protected static function checkNix() {
        return true;
//        $data = static::getNixData();
//        return isset($data['PAYEE_ACCOUNT'])
//        && isset($data['PAYMENT_AMOUNT'])
//        && isset($data['PAYMENT_UNITS'])
//        && isset($data['PAYMENT_BATCH_NUM'])
//        && isset($data['PAYER_ACCOUNT']);
    }


    protected static function nix(Invoice $invoice) {
//        $verified = 'verified' == $invoice->status;
        $verified = false;
        if (empty($invoice->user_name)) {
            $invoice->user_name = $_POST['USER_NAME'];
            $invoice->type = 'nix';
            $invoice->wallet = $_POST['PAYER_ACCOUNT'];
            $invoice->batch = $_POST['PAYMENT_BATCH_NUM'];
            $invoice->amount = (float) $_POST['PAYMENT_AMOUNT'];
        }
        static::logPayment($invoice, 'nix', 'success');

        if (Settings::get('nix', 'verify')) {
            $info = Yii::$app->nix->findPayment(['PAYMENT_ID' => $invoice->id]);
            $verified = $info['count'] == 1;
            $info = $info['invoices'][$invoice->id];
            $verified = $verified
                && $invoice->amount == (float) $info['Amount']
                && $invoice->wallet == $info['Payer Account'];
        }
        if (!$verified) {
            Yii::$app->session->addFlash('error', 'Платеж не подтвержден');
        }
        return $verified;
    }

    protected static function logPayment(Invoice $invoice, $type, $action) {
        $invoice_data = [];
        foreach($invoice->attributes as $key => $value) {
            if (null !== $value) {
                $invoice_data[$key] = $value;
            }
        }
        $data = [
            'get' => $_GET,
            'post' => $_POST,
            'invoice' => $invoice_data
        ];
        $date = date("y-m-d_H-i-s");
        file_put_contents(Yii::getAlias("@app/log/$type/$action/$date.json"),
            json_encode($data, JSON_PRETTY_PRINT));
    }

    protected static function verifyPerfect($hash, $wallet) {
        $param_keys = [
            'PAYMENT_ID', 'PAYEE_ACCOUNT', 'PAYMENT_AMOUNT',
            'PAYMENT_UNITS', 'PAYMENT_BATCH_NUM', 'PAYER_ACCOUNT',
            'TIMESTAMPGMT', 'V2_HASH'];
        foreach($param_keys as $key) {
            if (empty($_POST[$key])) {
                return [$key => 'Not set'];
            }
        }
        $string = implode(':', [
            $_POST['PAYMENT_ID'],
            $_POST['PAYEE_ACCOUNT'],
            $_POST['PAYMENT_AMOUNT'],
            $_POST['PAYMENT_UNITS'],
            $_POST['PAYMENT_BATCH_NUM'],
            $_POST['PAYER_ACCOUNT'],
            $hash,
            $_POST['TIMESTAMPGMT']
        ]);

        if (strtoupper(md5($string)) != $_POST['V2_HASH']) {
            return ['Invalid hash'];
        }
        if ($wallet != $_POST['PAYEE_ACCOUNT']) {
            return ['Invalid payee'];
        }
        if ('EUR' != $_POST['PAYMENT_UNITS']) {
            return ['Invalid currency'];
        }
        return false;
    }

    protected static function perfect(Invoice $invoice) {
        static::logPayment($invoice, 'perfect', 'success');

        $error = static::verifyPerfect(Yii::$app->perfect->hashAlternateSecret(), Settings::get('perfect', 'wallet'));
	if ($error) {
            return false;
        }
        $invoice->batch = $_POST['PAYMENT_BATCH_NUM'];
        $invoice->type = 'perfect';
        $invoice->wallet = $_POST['PAYER_ACCOUNT'];
        $invoice->amount = $_POST['PAYMENT_AMOUNT'];
        $name = isset($_POST['USER_NAME'])
            ? $_POST['USER_NAME']
            : User::findOne(['wallet_perfect' => $_POST['PAYER_ACCOUNT']]);
        if ($name) {
            $invoice->user_name = $name;
        }
        else {
            throw new ForbiddenHttpException('Пользователь не найден, вы не авторизовались');
        }

        if ($invoice->id != $_POST['PAYMENT_ID']) {
            $invoice->saveStatus('invalid_id');
            Yii::$app->session->addFlash('error', Yii::t('app', Invoice::$statuses['invalid_id']));
        }
        elseif (!Yii::$app->user->getIsGuest() && $invoice->user_name != Yii::$app->user->identity->name) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can only change the status of your payments'));
        }
        else {
            if (!Settings::get('perfect', 'verify')) {
                return true;
            }
            $payment = Yii::$app->perfect->findPayment($invoice->id);
            if ($payment && $payment->sender == $invoice->wallet && $payment->batch == $invoice->batch &&
                $invoice->amount == $payment->amount) {
                return true;
            }
            else {
                Yii::$app->session->addFlash('error', 'Платеж не потверджен');
            }
        }
        return false;
    }

    protected function success(Transaction $transaction, Invoice $invoice) {
        /** @var Type $type */
        $type = null;

//        $invoice->amount = floor($invoice->amount);
        $invoice->user->account += $invoice->amount;
        if ($invoice->saveStatus('success') && $invoice->user->save(true, ['account'])) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Payment {id} completed', [
                'id' => $invoice->id,
            ]));
        }
        else {
            $transaction->rollBack();
            Yii::$app->session->addFlash('error', 'Невозможно сохранить платеж');
            return $this->render('@app/views/home/index', ['data' => [
                'invoice' => [
                    'model' => $invoice->attributes,
                    'errors' => $invoice->getErrors()
                ],
                'user' => $invoice->user->getErrors(),
                'user_name' => $invoice->user_name
            ]]);
        }

        if (isset($_GET['type_id'])) {
            $type = Type::findOne($_GET['type_id']);
            if ($type) {
                if ($invoice->user->account >= $type->price) {
                    if ($type->open($invoice->user_name)) {
                        if ($invoice->saveStatus('success')) {
                            $transaction->commit();
                            Yii::$app->session->addFlash('success', "Пакет $type->name открыт");
                        }
                        else {
                            Yii::$app->session->addFlash('error', 'Невозможно сохранить платеж');
                            return $this->render('@app/views/home/index', ['data' => $invoice->getErrors()]);
                        }
                    } else {
                        Yii::$app->session->addFlash('error', "Пакет $type->name не открыт");
                    }
                }
                else {
                    Yii::$app->session->addFlash('error', 'Недостаточная сумма для оплаты плана');
                }
                if ($transaction->getIsActive()) {
                    $transaction->commit();
                }
                return $this->redirect(['/matrix/matrix/plans', 'user' => $invoice->user_name]);
            } else {
                Yii::$app->session->addFlash('error', 'План не найден');
            }
        }

        if ($transaction->getIsActive()) {
            $transaction->commit();
        }
        return $this->redirect(['/user/view', 'name' => $invoice->user_name]);
    }

    public function actionNix($id, $receiver) {
        $date = date("y-m-d_H-i-s");
        $params = [
            'get' => $_GET,
            'count' => count($_REQUEST)
        ];
        if (!empty($_POST)) {
            $params['post'] = $_POST;
        }
        if (!Settings::get('common', 'nix')) {
            return 'NixMoney is turn off';
        }
        if ($errors = static::verifyPerfect(
            strtoupper(md5(Settings::get('nix', 'password'))),
            Settings::get('nix', 'wallet'))) {
            $params['errors'] = $errors;
        }
        file_put_contents(Yii::getAlias("@app/log/nix/b2b/$date.json"),
            json_encode($params, JSON_PRETTY_PRINT));
        if ($errors) {
            http_response_code(400);
            return json_encode($errors);
        }

        $transaction = Yii::$app->db->beginTransaction();
        $invoice = Invoice::findOne($id);
        if (!$invoice) {
            $invoice = new Invoice([
                'id' => $id
            ]);
        }
        if (!$invoice->isNewRecord && 'success' == $invoice->status) {
            $transaction->rollBack();
            return 'Already done';
        }

        $invoice->batch = $_POST['PAYMENT_BATCH_NUM'];
        $invoice->type = 'nix';
        $invoice->wallet = $_POST['PAYER_ACCOUNT'];
        $invoice->amount = floor($_POST['PAYMENT_AMOUNT']);
        $invoice->user_name = $receiver;
        if ($invoice->saveStatus('verified')) {
            $transaction->commit();
            return 'Success';
        }
        else {
            return 'Cannot save payment: ' . $invoice->__debuginfo();
        }
    }

    public function actionFail($id = null) {
        $invoice = null;
        if ($id) {
            $invoice = Invoice::findOne($id);
        }

        if ($invoice && Yii::$app->user->identity->name != $invoice->user_name) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can only change the status of your payments'));
        }
        else {
            if ($invoice) {
                $invoice->saveStatus('cancel');
            }
            Yii::$app->session->addFlash('error', Yii::t('app', 'You cancel payment'));
        }

        if ($invoice) {
            return $this->render('view', [
                'model' => $invoice
            ]);
        }
        else {
            return $this->redirect('/user/view');
        }
    }


    public function actionWithdraw($id) {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!Yii::$app->user->identity->isManager()) {
            if (Settings::get('common', 'withdrawal_confirmation')) {
                throw new ForbiddenHttpException('Подтвердить вывод может только администрация');
            }
            if (!$user->hasSponsors()) {
                throw new ForbiddenHttpException('Вы должны запросить трох спонсоров');
            }
        }
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->findModel($id);
        if (false === Settings::is('common', $invoice->type)) {
            throw new ForbiddenHttpException('Переводна на ' . Invoice::$types[$invoice->type] . ' отключен');
        }
        try {
            $invoice->scenario = 'withdraw';
            if ('success' == $invoice->status) {
                $transaction->rollBack();
                Yii::$app->session->addFlash('info', Yii::t('app', 'Payment has already done previously'));
            }
            elseif (abs($invoice->amount) > $invoice->user->account) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Insufficient funds'));
            }
            else {
                $result = false;
                switch ($invoice->type) {
                    case 'perfect':
                        $result = static::perfectWithdraw($invoice);
                        break;
                    case 'nix':
                        $result = static::nixWithdraw($invoice);
                        break;
                    case 'advcash':
                        $invoice->batch = Yii::$app->advcash->sendMoney($invoice->user->wallet_advcash,
                            abs($invoice->amount));
                        if ($invoice->batch) {
                            $result = true;
                        }
                        break;
                    default:
                        throw new ServerErrorHttpException('Неизвесний тип платежа ' . $invoice->type);
                }
                if ($result) {
                    static::withdrawSuccessful($invoice, $transaction);
                }
            }
        }
        catch(Exception $ex) {
            $transaction->rollBack();
            if ($ex instanceof JournalException) {
                Journal::write($ex->type, $ex->event, $ex->object_id, $ex->getMessage());
            }
            Yii::$app->session->addFlash('error', $ex->getMessage());
        }
        return $this->redirect(['/user/view', 'name' => $invoice->user_name]);
    }

    protected static function perfectWithdraw(Invoice $invoice) {
        if (!Settings::get('perfect', 'id')) {
            return true;
        }
        else {
            $withdrawal = Withdrawal::fromInvoice($invoice);
            $response = file_get_contents('https://perfectmoney.is/acct/confirm.asp?' . $withdrawal);
            if (!preg_match('/<h1>(.*)<\/h1>/', $response, $result)) {
                $invoice->throwJournalException($response);
            } elseif ('Spend' != $result[1]) {
                $invoice->throwJournalException($result[1]);
            } elseif (!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/",
                $response, $result, PREG_SET_ORDER)
            ) {
                $invoice->throwJournalException($response);
            } else {
                $info = [];
                foreach ($result as $row) {
                    $info[$row[1]] = $row[2];
                }
                if (isset($info["ERROR"])) {
                    $invoice->throwJournalException($info["ERROR"]);
                } elseif ($info['PAYMENT_AMOUNT'] != abs($invoice->amount)) {
                    $invoice->throwJournalException(Yii::t('app', 'Invalid amount') . ' ' . $info['PAYMENT_AMOUNT']);
                } else {
                    $invoice->batch = $info['PAYMENT_BATCH_NUM'];
                    $invoice->wallet = $info['Payee_Account'];
                    return true;
                }
            }
        }
        return false;
    }

    protected static function parsePerfectForm($response) {
        preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $response, $result, PREG_SET_ORDER);
        $info = [];
        foreach ($result as $row) {
            $info[$row[1]] = $row[2];
        }
        return $info;
    }

    protected static function nixWithdraw(Invoice $invoice) {
        $nix = Settings::getCategory('nix');
        $url = $nix['server'] . 'send?' . http_build_query([
                'PASSPHRASE' => $nix['password'],
                'PAYER_ACCOUNT' => $nix['wallet'],
                'PAYEE_ACCOUNT' => Yii::$app->user->identity->wallet_nix,
                'AMOUNT' => abs($invoice->amount),
                'MEMO' => $invoice->id
            ]);
        $info = static::parsePerfectForm(file_get_contents($url));
        if (isset($info['ERROR'])) {
            Yii::$app->session->addFlash('error', $info['ERROR']);
            return false;
        }
        $invoice->wallet = $info['PAYEE_ACCOUNT'];
        $invoice->amount = $info['PAYMENT_AMOUNT'];
        $invoice->batch = $info['PAYMENT_BATCH_NUM'];
        return true;
    }

    protected static function withdrawSuccessful(Invoice $invoice, Transaction $transaction) {
        $invoice->status = 'success';
        $invoice->amount = -abs($invoice->amount);
        $invoice->user->account -= abs($invoice->amount);
        if ($invoice->user->save(true, ['account']) && $invoice->save(false)) {
            $transaction->commit();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Payment #{id} completed', [
                'id' => $invoice->number,
            ]));
        }
        else {
            Yii::$app->session->addFlash('error', json_encode(
                array_merge($invoice->user->errors, $invoice->errors),
                JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        }
    }

    public function actionPay($id = null) {
        $vars = [];
        if ($id && ($model = is_numeric($id) ? Type::findOne($id) : $this->findModel($id))) {
            $vars['model'] = $model;
        }
        return $this->render('pay', $vars);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            $model->scenario = $model->amount < 0 ? 'withdraw' : 'payment';
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function restrictUser($default = null) {
        return !Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()
            ? Yii::$app->user->identity->name
            : $default;
    }
}
