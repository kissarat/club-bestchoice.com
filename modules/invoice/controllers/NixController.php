<?php

namespace app\modules\invoice\controllers;


use app\behaviors\Access;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class NixController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'manager' => ['raw', 'get', 'encode']
            ]
        ];
    }

    public function actionEncode() {
        $rows = [];
        $json = Yii::$app->nix->findPayment($_GET);
        foreach($json['invoices'] as $id => $invoice) {
            $rows[] = http_build_query([
                'PAYEE_ACCOUNT' => $invoice['Payee Account'],
                'PAYMENT_AMOUNT' => $invoice['Amount'],
                'PAYMENT_UNITS' => $invoice['Currency'],
                'PAYMENT_BATCH_NUM' => $invoice['Time'],
                'PAYER_ACCOUNT' => $invoice['Payer Account'],
            ]);
        }
        $r = Yii::$app->response;
        $r->format = Response::FORMAT_RAW;
        $r->getHeaders()->add('Content-Type', 'text/plain');
        return implode("\n", $rows);
    }

    public function actionRaw() {
        $r = Yii::$app->response;
        $r->format = Response::FORMAT_RAW;
        $r->getHeaders()->add('Content-Type', 'text/plain');
        return file_get_contents(Yii::$app->nix->buildQuery($_GET));
    }

    public function actionGet() {
        $r = Yii::$app->response;
        $r->format = Response::FORMAT_JSON;
        return Yii::$app->nix->findPayment($_GET);
    }
}
