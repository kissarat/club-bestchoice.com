<?php

namespace app\modules\invoice\controllers;


use app\behaviors\Access;
use Yii;

class PerfectController extends InvoiceController {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'manager' => ['history']
            ]
        ];
    }

    public function actionHistory($end = null, $start = null) {
        if (isset($_GET['days'])) {
            $end = time();
            $start = $end - 3600 * 24 * $_GET['days'];
        }
        return $this->render('@app/views/home/table', [
            'rows' => Yii::$app->perfect->queryHistory($end, $start)
        ]);
    }
}
