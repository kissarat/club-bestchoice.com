<?php

namespace app\modules\invoice\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\modules\invoice\models\search\IncomeSearch;
use app\modules\invoice\models\Transfer;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class TransferController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'create']
            ]
        ];
    }

    public function actionIndex($mode = null, $user = null) {
        $identity = Yii::$app->user->identity;
        if (!$identity->isManager() && $user != $identity->name) {
            Yii::$app->session->addFlash('error', 'Вы можете видеть только свои переводы');
            return $this->redirect(['index', 'user' => $identity->name]);
        }

        if (!Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('index', static::index($mode, $user));
    }

    public static function index($mode = null, $user = null) {
        $query = Transfer::find();
        $query->andWhere('income' == $mode
            ? "event <> 'transfer'"
            : "event = 'transfer'");
        $query->with('node')->with('object');
        if ($user) {
            if ('income' == $mode) {
                $query->andWhere('receiver_name = :name OR (sender_name = :name AND receiver_name is null)', [
                    ':name' => $user
                ]);
            }
            else {
                $query->andWhere('sender_name = :name OR receiver_name = :name', [
                    ':name' => $user
                ]);
            }
        }

        $vars = [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_DESC,
                        'id' => SORT_DESC
                    ]
                ]
            ]),
            'mode' => $mode,
            'user' => $user
        ];
        if ('income' == $mode) {
            if ($user) {
                $vars['total'] = (float)SQL::queryCell("SELECT sum(amount) FROM transfer_journal
                WHERE type = 'account' AND (sender_name = :name OR receiver_name = :name)", [
                    ':name' => $user
                ]);
            }
            $searchModel = new IncomeSearch();
            if ($searchModel->load(Yii::$app->request->post())) {
                if (0 != $searchModel->level) {
                    $query->andWhere(['level' => $searchModel->level]);
                }
            }
            $vars['searchModel'] = $searchModel;
        }
        return $vars;
    }

    public function actionCreate() {
        $model = new Transfer(['scenario' => 'transfer']);
        if (!Yii::$app->user->identity->hasSponsors()) {
            throw new ForbiddenHttpException('Вы сможете перевести деньги после приглашения 3-х рефералов');
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sender_name = Yii::$app->user->identity->name;
            if ($model->sender->account >= $model->amount) {
                if (!Yii::$app->user->identity->isManager() && $model->sender->pin != $model->pin) {
                    $model->addError('pin', 'Неправильный пароль');
                }
                else {
                    $transaction = Yii::$app->db->beginTransaction();
                    SQL::execute("SELECT transfer(:sender_name, :receiver_name, :amount, :memo,
                            :ip, null, true, 'account', 'transfer')", [
                        ':sender_name' => $model->sender_name,
                        ':receiver_name' => $model->receiver_name,
                        ':amount' => $model->amount,
                        ':memo' => $model->memo,
                        ':ip' => $_SERVER['REMOTE_ADDR']
                    ]);
                    $transaction->commit();
                    Yii::$app->session->addFlash('success',
                        "Пользователю $model->receiver_name начислено €$model->amount");
                    return $this->redirect(['index', 'user' => $model->sender_name]);
                }
            }
            else {
                $model->addError('amount', 'На вашем счету доступно €' . $model->sender->account);
            }
        }

        if (!Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }
}
