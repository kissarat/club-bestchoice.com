<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\invoice\models;

use app\behaviors\Journal;
use app\helpers\JournalException;
use app\models\Record;
use app\models\Settings;
use app\models\User;
use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $type
 * @property number $amount
 * @property integer $number
 * @property integer $batch
 * @property string $status
 * @property string $wallet
 *
 * @property User $user
 */
class Invoice extends ActiveRecord
{
    public $pin;

    public static $statuses = [
        'create' => 'Created',
        'invalid_amount' => 'Invalid amount',
        'invalid_receiver' => 'Invalid receiver',
        'invalid_batch' => 'Transaction ID does not match',
        'invalid_response' => 'Unknown server response',
        'invalid_id' => 'Invalid ID',
        'invalid_hash' => 'Invalid hash',
        'invalid_currency' => 'Invalid currency',
        'insufficient_funds' => 'Insufficient funds in the account user',
        'no_qualification' => 'User does not qualify',
        'cancel' => 'Cancel',
        'fail' => 'Error',
        'success' => 'Completed',
        'verified' => 'Проверен',
    ];

    public static $types = [
        'perfect' => 'Perfect Money',
        'nix' => 'NixMoney',
        'advcash' => 'AdvCash'
    ];

    public static function listTypes() {
        $types = static::$types;
        $common = Settings::getCategory('common');
        foreach($types as $key => $value) {
            if (isset($types[$key]) && !$common[$key]) {
                unset($types[$key]);
            }
        }
        return $types;
    }

    public static function tableName() {
        return 'invoice';
    }

    public function traceable() {
        return ['status'];
    }

    public function behaviors() {
        return [
            Journal::class
        ];
    }

    public function rules() {
        return [
            [['user_name', 'amount', 'type'], 'required'],
            [['user_name', 'status'], 'string', 'max' => 16],
            ['amount', 'number'],
//            ['amount', 'integer', 'on' => 'payment'],
//            ['amount', 'integer', 'on' => 'withdraw'],
            ['batch', 'string'],
            ['wallet', 'string'],
            ['pin', 'match', 'pattern' => '/^\d{4}$/', 'message' => 'Введите 4 цифры'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find() {
        return parent::find()->andWhere('"status" <> \'delete\'');
    }

    public function scenarios() {
        return [
            'default' => ['user_name', 'amount', 'status', 'type', 'wallet', 'number', 'batch', 'id'],
            'payment' => ['user_name', 'amount', 'type'],
            'withdraw' => ['user_name', 'amount', 'type', 'pin'],
            'manage' => ['user_name', 'amount', 'status', 'type'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'Username'),
            'amount' => Yii::t('app', 'Amount'),
            'status' => Yii::t('app', 'Status'),
            'wallet' => 'Кошелек',
            'batch' => '№ транзакции',
            'number' => 'Номер',
            'type' => 'Тип',
            'pin' => 'Транзакционний пароль',
        ];
    }

    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }

    public function saveStatus($status) {
        $this->status = $status;
        return $this->save();
    }

    public function __toString() {
        return ($this->amount < 0 ? "Выплата" : "Оплата") . " №$this->number на €" . abs($this->amount);
    }

    public function throwJournalException($message) {
        throw new JournalException(static::tableName(), $this->id, 'fail', $message);
    }

    public function journalView() {
        return __DIR__ . '/../views/invoice/journal.php';
    }

    public function __debuginfo() {
        $info = [
            'model' => $this->attributes,
            'errors' => $this->getErrors()
        ];
        return json_encode($info, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    public function getRecords() {
        return $this->hasMany(Record::class, [
            'type' => 'invoice',
            'object_id' => 'id'
        ]);
    }


    public function logRequest($data = null) {
        if (empty($data)) {
            $data = $_POST;
        }
        if (is_array($data)) {
            $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }
        $action = Yii::$app->controller->action->id;
        $date = date("y-m-d_H-i-s");
        file_put_contents(Yii::getAlias("@app/log/$this->type/$action/$date.json"), $data);
        return $data;
    }
}
