<?php

namespace app\modules\invoice\models;


use yii\db\ActiveRecord;

class InvoiceSum extends ActiveRecord {
    public static function tableName() {
        return 'invoice_sum';
    }

    public function attributeLabels() {
        return [
            'user_name' => 'Пользователь',
            'payment' => 'Оплачено',
            'withdraw' => 'Выведено',
            'balance' => 'Сальдо',
        ];
    }
}
