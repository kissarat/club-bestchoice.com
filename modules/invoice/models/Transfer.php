<?php

namespace app\modules\invoice\models;


use app\helpers\SQL;
use app\models\User;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Referral;
use yii\db\ActiveRecord;

/**
 * @property string sender_name
 * @property string receiver_name
 * @property number amount
 * @property string memo
 * @property string time
 * @property string type
 * @property string event
 * @property string ip
 * @property integer level
 * @property integer node_id
 * @property integer object_id
 *
 * @property Node node
 * @property Node object
 * @property User sender
 * @property User receiver
 * @property Referral referral
 * @property Matrix matrix
 */
class Transfer extends ActiveRecord {
    public $pin;

    public static function tableName() {
        return 'transfer_journal';
    }

    public function rules() {
        return [
            [['receiver_name', 'amount'], 'required'],
            ['receiver_name', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'name',
                'message' => 'Пользователь не существует'],
            ['amount', 'integer', 'min' => 1],
            ['memo', 'string'],
            ['level', 'integer'],
            [['type', 'event'], 'string'],
            ['pin', 'match', 'pattern' => '/^\d{4}$/', 'message' => 'Введите 4 цифры'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'sender_name' => 'Отправитель',
            'receiver_name' => 'Получатель',
            'amount' => 'Сумма',
            'memo' => 'Заметка',
            'summary' => 'Заметка',
            'time' => 'Время',
            'ip' => 'IP',
            'node_id' => 'ID матрицы',
            'level' => 'Уровень',
            'account' => 'Счет',
            'pin' => 'Транзакционний пароль',
        ];
    }

    public function scenarios() {
        return [
            'default' => ['sender_name', 'receiver_name', 'amount', 'memo', 'level',
            'type', 'event'],
            'transfer' => ['sender_name', 'receiver_name', 'amount', 'memo', 'pin']
        ];
    }

    public function __toString() {
        if ($this->memo) {
            return $this->getSummary();
        }
        else {
            return "€$this->amount $this->sender_name -> $this->receiver_name";
        }
    }

    public function getSummary() {
        return $this->memo && strlen($this->memo) > 40 ? substr($this->memo, 0, 40) . '…' : $this->memo;
    }

    public function getNode() {
        return $this->hasOne(Node::class, ['id' => 'node_id']);
    }

    public function getObject() {
        return $this->hasOne(Node::class, ['id' => 'object_id']);
    }

    public function getSender() {
        return $this->hasOne(User::class, ['name' => 'sender_name']);
    }

    public function getReceiver() {
        return $this->hasOne(User::class, ['name' => 'receiver_name']);
    }

    public function getReferral() {
        return $this->hasOne(Referral::class, [
            'root_id' => 'node_id',
            'user_name' => 'receiver_name'
        ]);
    }

    public function getMatrix() {
        return $this->hasMany(Matrix::class, [
            'root_id' => 'node_id'
        ]);
    }

    public static function transfer(Node $node, $referral) {
        SQL::execute('SELECT transfer(:sender, :receiver, :interest, null, :ip::INET, :id, false)', [
            ':sender' => $node->user_name,
            ':receiver' => $referral->user_name,
            ':interest' => $referral->interest,
            ':ip' => $_SERVER['REMOTE_ADDR'],
            ':id' => $node->id
        ]);
    }

    public function getAccount() {
        switch($this->type) {
            case 'account':
                return 'Основной';
            case 'blocked':
                return 'Заблокирован';
            default:
                return 'Ошибка';
        }
    }

    public function getInfo($default = null) {
        $info = json_decode($this->memo, true);
        if (JSON_ERROR_NONE == json_last_error()) {
            return $info;
        }
        else {
            return $default;
        }
    }

    public function getInfoAttribute($name, $default = null) {
        $info = $this->getInfo();
        if ($info && isset($info[$name])) {
            return $info[$name];
        }
        return $default;
    }
}
