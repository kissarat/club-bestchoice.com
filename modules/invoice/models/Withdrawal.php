<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\invoice\models;


use app\models\Settings;
use Yii;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * Class Withdrawal
 * @package frontend\models
 * @property int AccountID
 * @property string PassPhrase
 * @property string Payer_Account
 * @property string Payee_Account
 * @property number Amount
 * @property int PAY_IN
 * @property int PAYMENT_ID
 */

class Withdrawal {
    public $AccountID;
    public $PassPhrase;
    public $Payer_Account;
    public $Payee_Account;
    public $Amount;
    public $PAY_IN = 1;
    public $PAYMENT_ID;

    public static function fromInvoice(Invoice $invoice) {
        $withdrawal = new Withdrawal();
        $withdrawal->AccountID = Settings::get('perfect', 'id');
        $withdrawal->PassPhrase = Settings::get('perfect', 'password');
        $withdrawal->PAYMENT_ID = $invoice->id;
        $withdrawal->Payer_Account = Settings::get('perfect', 'wallet');
        $withdrawal->Payee_Account = $invoice->user->wallet_perfect;
        $withdrawal->Amount = abs($invoice->amount);
        return $withdrawal;
    }

    public function __toString() {
        $params = [];
        foreach($this as $key => $value)
            $params[$key] = $value;
        return http_build_query($params);
    }
}
