<?php

namespace app\modules\invoice\models\search;


use app\helpers\SQL;
use yii\base\Model;

class IncomeSearch extends Model {
    public $level;

    public static function levels() {
        $levels = SQL::queryColumn('SELECT id FROM matrix_level ORDER BY id');
        $levels = array_combine($levels, $levels);
        $levels[0] = 'Все';
        return $levels;
    }

    public function rules() {
        return [
            ['level', 'integer']
        ];
    }

    public function attributeLabels() {
        return [
            'level' => 'Уровень'
        ];
    }
}
