<?php
/**
 * @link http://zenothing.com/
 */

use app\models\Settings;
use app\models\User;
use app\modules\invoice\models\Invoice;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\invoice\models\search\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежи';

$me = !Yii::$app->user->getIsGuest() && isset($_GET['user']) && $_GET['user'] == Yii::$app->user->identity->name;
if ($me) {
    $this->params['breadcrumbs'][] = ['label' => $_GET['user'],
        'url' => ['/user/view', 'name' => $_GET['user']]];
    $this->params['breadcrumbs'][] = $this->title;
}

$actions = ['class' => 'yii\grid\ActionColumn'];
if (!Yii::$app->user->identity->isManager()) {
    $actions['template'] = '{view}';
}

$columns = ['id', 'number', 'batch', 'wallet'];

if (empty($_GET['user'])) {
    $columns[] =
        [
            'attribute' => 'user_name',
            'format' => 'html',
            'value' => function(Invoice $model) {
                return Html::a($model->user_name, ['index', 'user' => $model->user_name]);
            }
        ];
}

$columns[] = [
    'attribute' => 'amount',
    'value' => function(Invoice $model) {
        return (float)$model->amount;
    }
];
$columns[] = [
    'attribute' => 'status',
    'format' => 'html',
    'value' => function(Invoice $model) {
        $status = Yii::t('app', Invoice::$statuses[$model->status]);
        if ($model->amount < 0
            and 'success' != $model->status
            and (Yii::$app->user->identity->isManager() || !Settings::get('common', 'withdrawal_confirmation'))) {
            $status .= ' ' .Html::a(Yii::t('app', 'Withdraw'),
                    ['withdraw', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs']);
        }
        return $status;
    }
];
$columns[] = $actions;
$payment_url = ['index', 'scenario' => 'payment'];
$withdraw_url = ['index', 'scenario' => 'withdraw'];
$index_url = ['index'];
if (isset($_GET['user'])) {
    $payment_url['user'] = $_GET['user'];
    $withdraw_url['user'] = $_GET['user'];
    $index_url['user'] = $_GET['user'];
}

$options = [
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'rowOptions' => function(Invoice $model, $index, $widget, $grid) {
        return [
            'class' => $model->amount < 0 ? 'withdraw' : 'payment'
        ];
    }
];

if (Yii::$app->user->identity->isManager()) {
    $options['filterModel'] = $searchModel;
}
?>
<div class="invoice-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="form-group">
        <?php if (!Yii::$app->user->identity->isManager()): ?>
            <?= Html::a(Yii::t('app', 'Pay'), ['pay'], ['class' => 'btn btn-success']); ?>
            <?php
            if (Yii::$app->user->identity->account > 0) {
                echo Html::a(Yii::t('app', 'Withdraw'),
                    ['create', 'scenario' => 'withdraw'], ['class' => 'btn btn-primary']);
            }
            ?>
        <?php endif ?>
    </div>

    <div class="form-group">
        <?= Yii::t('app', 'Show') ?>:
        <?= empty($_GET['scenario']) ? 'все' : Html::a('все', $index_url) ?>
        <?= isset($_GET['scenario']) && 'payment' == $_GET['scenario'] ? Yii::t('app', 'payment')
            : Html::a(Yii::t('app', 'payment'), $payment_url) ?>
        <?= isset($_GET['scenario']) && 'withdraw' == $_GET['scenario'] ? Yii::t('app', 'withdraw')
            : Html::a(Yii::t('app', 'withdraw'), $withdraw_url) ?>
    </div>

    <?= GridView::widget($options); ?>
</div>
