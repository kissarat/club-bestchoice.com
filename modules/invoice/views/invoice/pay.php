<?php
use app\models\Settings;
use app\modules\matrix\models\Type;
use yii\helpers\Html;
use yii\helpers\Url;

$success_url = ['/invoice/invoice/success'];
$fail_url = ['/invoice/invoice/fail'];
$nix_status_url = ['/invoice/invoice/nix'];
$type_id = null;
$name = isset($_GET['user'])
    ? $_GET['user']
    : (Yii::$app->user->getIsGuest() ? null : Yii::$app->user->identity->name);
if ($name) {
    $nix_status_url['receiver'] = $name;
}
if (isset($model)) {
    if ($model instanceof Type) {
        $id = uuid_create();
        $amount = $model->price;
        $success_url['type_id'] = $model->id;
        $fail_url['type_id'] = $model->id;
        $type_id = $model->id;
    } else {
        $id = $model->id;
        $amount = $model->amount;
    }
//    echo Html::script("addEventListener('load', function() {pay('perfect')})");
} else {
    $id = uuid_create();
    $amount = 0;
}

$success_url['id'] = $id;
$fail_url['id'] = $id;
$nix_status_url['id'] = $id;

$success_url = Url::to($success_url, true);
$fail_url = Url::to($fail_url, true);
$nix_status_url = Url::to($nix_status_url, true);
?>
<div class="invoice-pay">
    <h1 class="title">Оплата</h1>

    <div class="summa">
        <label for="amount">Введите сумму:</label>
        <input id="amount" type="text" value="<?= $amount ?>"/>
    </div>

    <div class="pay12">
        <?php if (Settings::get('common', 'perfect')): ?>
            <p>Выберите платежную систему:</p>

            <form id="perfect" action="https://perfectmoney.is/api/step1.asp" method="post">
                <input name="PAYMENT_ID" type="hidden" value="<?= $id ?>"/>
                <input name="PAYEE_ACCOUNT" value="<?= Settings::get('perfect', 'wallet') ?>" type="hidden"/>
                <input name="PAYMENT_AMOUNT" type="hidden" value="<?= $amount ?>"/>
                <input name="PAYEE_NAME" value="<?= Yii::$app->name ?>" type="hidden"/>
                <input name="PAYMENT_UNITS" value="EUR" type="hidden"/>
                <input name="PAYMENT_URL" value="<?= $success_url ?>" type="hidden"/>
                <input name="NOPAYMENT_URL" value="<?= $fail_url ?>" type="hidden"/>
                <input name="BAGGAGE_FIELDS" value="USER_NAME" type="hidden"/>
                <?php if ($name): ?>
                    <input name="USER_NAME" value="<?= $name ?>" type="hidden"/>
                <?php endif ?>
                <input name="PAYMENT_METHOD" type="submit" value="Perfect Money"/>
            </form>
            <?php
        endif;

        if (Settings::get('common', 'nix')):
            ?>
            <form id="nix" action="<?= Settings::get('nix', 'server') ?>merchant.jsp" method="post">
                <input name="PAYMENT_ID" type="hidden" value="<?= $id ?>"/>
                <input name="PAYEE_ACCOUNT" value="<?= Settings::get('nix', 'wallet') ?>" type="hidden"/>
                <input name="PAYMENT_AMOUNT" type="hidden" value="<?= $amount ?>"/>
                <input name="PAYEE_NAME" value="<?= Yii::$app->name ?>" type="hidden"/>
                <input name="PAYMENT_UNITS" value="EUR" type="hidden"/>
                <input name="STATUS_URL" value="<?= $nix_status_url ?>" type="hidden"/>
                <input name="PAYMENT_URL" value="<?= $success_url ?>" type="hidden"/>
                <input name="NOPAYMENT_URL" value="<?= $fail_url ?>" type="hidden"/>
                <input name="BAGGAGE_FIELDS" value="USER_NAME" type="hidden"/>
                <?php if ($name): ?>
                    <input name="USER_NAME" value="<?= $name ?>" type="hidden"/>
                <?php endif ?>
                <input name="PAYMENT_METHOD" type="submit" value="NixMoney"/>
            </form>
            <?php
        endif;

        if (Settings::get('common', 'advcash')):
            ?>
            <form id="advcash" method="POST" action="https://wallet.advcash.com/sci/">
                <input type="hidden" name="ac_account_name"
                       value="<?= Settings::get('advcash', 'email') ?>">
                <input type="hidden" name="ac_sci_name" value="<?= Yii::$app->name ?>">
                <input type="hidden" name="ac_amount" value="<?= $amount ?>">
                <input type="hidden" name="ac_currency" value="EUR">
                <input type="hidden" name="ac_order_id" value="<?= $id ?>">
                <input type="hidden" name="ac_account_email"
                       value="<?= Settings::get('advcash', 'email') ?>" />
                <input type="hidden" name="ac_sign" value="">
                <?php
                if ($name) {
                    echo Html::hiddenInput('USER_NAME', $name);
                }
                if ($type_id) {
                    echo Html::hiddenInput('TYPE_ID', $type_id);
                }
                ?>
                <button type="button">AdvCash</button>
            </form>
        <?php endif ?>
    </div>
</div>
