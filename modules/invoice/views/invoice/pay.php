<?php
use app\models\Settings;
use app\modules\matrix\models\Type;
use yii\helpers\Html;
use yii\helpers\Url;

$success_url = ['/invoice/invoice/success'];
$fail_url = ['/invoice/invoice/fail'];
$name = isset($_GET['user'])
    ? $_GET['user']
    : (Yii::$app->user->getIsGuest() ? null : Yii::$app->user->identity->name);
if (isset($model)) {
    if ($model instanceof Type) {
        $id = uuid_create();
        $amount = $model->price;
        $success_url['type_id'] = $model->id;
        $fail_url['type_id'] = $model->id;
    }
    else {
        $id = $model->id;
        $amount = $model->amount;
    }
    echo Html::script("addEventListener('load', function() {pay('perfect')})");
}
else {
    $id = uuid_create();
    $amount = 0;
}

$success_url['id'] = $id;
$fail_url['id'] = $id;

$success_url = Url::to($success_url, true);
$fail_url = Url::to($fail_url, true);
$amount = ceil($amount);
?>
<div class="invoice-pay">
    <div style="<?= isset($model) ? '' : 'display: none' ?>">
        Подождите...
    </div>
    <div style="<?= isset($model) ? 'display: none' : '' ?>">
        <div>
            <label for="amount">Сумма:</label>
            <input id="amount" type="text" value="<?= $amount ?>" />
        </div>
        <form id="perfect" action="https://perfectmoney.is/api/step1.asp" method="post">
            <input name="PAYMENT_ID" type="hidden" value="<?= $id ?>" />
            <input name="PAYEE_ACCOUNT" value="<?= Settings::get('perfect', 'wallet') ?>" type="hidden" />
            <input name="PAYMENT_AMOUNT" type="hidden" value="<?= $amount ?>" />
            <input name="PAYEE_NAME" value="<?= Yii::$app->name ?>" type="hidden" />
            <input name="PAYMENT_UNITS" value="EUR" type="hidden" />
            <input name="PAYMENT_URL" value="<?= $success_url ?>" type="hidden" />
            <input name="NOPAYMENT_URL" value="<?= $fail_url ?>" type="hidden" />
            <input name="BAGGAGE_FIELDS" value="USER_NAME" type="hidden" />
            <?php if ($name): ?>
                <input name="USER_NAME" value="<?= $name ?>" type="hidden" />
            <?php endif ?>
            <input name="PAYMENT_METHOD" type="submit" value="Perfect Money" />
        </form>
    </div>
</div>
