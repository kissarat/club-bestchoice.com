<?php
/** @var \yii\web\View $this */
/** @var array $totals */
/** @var \yii\data\ActiveDataProvider $dataProvider */
use yii\grid\GridView;

$this->title = 'Сводка платежей';
?>
<div class="invoice-summary">
    <h1><?= $this->title ?></h1>
    <p>
    <table class="totals">
        <tbody>
        <tr>
            <td>Вложено</td>
            <td><?= $totals['payment'] ?></td>
        </tr>
        <tr>
            <td>Выведено</td>
            <td><?= $totals['withdraw'] ?></td>
        </tr>
        <tr>
            <td>Сальдо</td>
            <td><?= $totals['balance'] ?></td>
        </tr>
        </tbody>
    </table>
    </p>
    <?= GridView::widget(['dataProvider' => $dataProvider]); ?>
</div>
