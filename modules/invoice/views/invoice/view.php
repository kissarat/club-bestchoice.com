<?php
/**
 * @link http://zenothing.com/
 */

use app\models\Settings;
use app\modules\invoice\models\Invoice;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model app\modules\invoice\models\Invoice
 * @var $journal \yii\data\ActiveDataProvider
 */

$this->title = $model;
$invoices_url = ['index'];
$guest = Yii::$app->user->getIsGuest();
$manager = !$guest && Yii::$app->user->identity->isManager();
if (!$guest && !$manager) {
    $invoices_url['user'] = Yii::$app->user->identity->name;
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invoices'), 'url' => $invoices_url];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if ($manager): ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?php
        elseif ('success' != $model->status):
            if ($model->amount > 0) {
                echo Html::a(Yii::t('app', 'Pay'), ['pay', 'id' => $model->id], ['class' => 'btn btn-primary']);
            }
            else {
                if ($manager || !Settings::get('common', 'withdrawal_confirmation')) {
                    echo Html::a(Yii::t('app', 'Withdraw'),
                        ['withdraw', 'id' => $model->id],
                        ['class' => 'btn btn-warning']);
                }
            }
        endif;
        ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'batch',
            'wallet',
            [
                'attribute' => 'user_name',
                'format' => 'html',
                'value' => Html::a($model->user_name, ['/user/view', 'name' => $model->user_name])
            ],
            [
                'attribute' => 'amount',
                'value' => '$' . abs($model->amount)
            ],
            [
                'attribute' => 'status',
                'value' => Yii::t('app', Invoice::$statuses[$model->status])
            ],
            [
                'attribute' => 'type',
                'value' => Invoice::$types[$model->type]
            ]
        ],
    ]) ?>

    <?= Yii::$app->view->render('@app/views/journal/object', [
        'dataProvider' => $journal
    ]) ?>
</div>
