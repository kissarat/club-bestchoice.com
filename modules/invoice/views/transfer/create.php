<?php
use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Перевести';
?>
<div class="transfer-create">
    <h2><?= $this->title ?></h2>
    <?php
    $form = ActiveForm::begin();
    $fields = [
        $form->field($model, 'receiver_name')->widget(AjaxComplete::class, [
            'route' => ['/user/complete']
        ]),
        $form->field($model, 'amount'),
    ];
    if (!Yii::$app->user->identity->isManager()) {
        $fields[] = $form->field($model, 'pin');
    }
    $fields[] = $form->field($model, 'memo')->textarea();
    $fields[] = Html::submitButton('Перевести', ['class' => 'btn btn-primary']);
    echo implode("\n", $fields);
    ActiveForm::end();
    ?>
</div>
