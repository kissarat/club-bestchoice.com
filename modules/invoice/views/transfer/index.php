<?php
/** @var Node[] $nodes */
/** @var string $mode */
/** @var number $total */
/** @var \yii\data\ActiveDataProvider $dataProvider */

use app\modules\invoice\models\Transfer;
use app\modules\matrix\models\Node;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'income' == $mode ? 'Прибыль' : 'Переводы';
/*
if (isset($total)) {
    $this->title .= ' €' . $total;
}
*/

$accountColumn = [
    'attribute' => 'type',
    'label' => 'Счет',
    'value' => function(Transfer $transfer) {
        return $transfer->getAccount();
    }
];

if ($user) {
    if ('income' == $mode) {
        $columns = [
            [
                'attribute' => 'node_id',
                'label' => 'От кого',
                'value' => function(Transfer $transfer) {
                    return $transfer->node;
                }
            ],
            [
                'attribute' => 'object_id',
                'label' => 'Кому',
                'value' => function(Transfer $transfer) {
                    return $transfer->object;
                }
            ],
            [
                'attribute' => 'object_id',
                'label' => 'Уровень',
                'value' => function(Transfer $transfer) {
                    return $transfer->getInfoAttribute('level');
                }
            ],
            [
                'attribute' => 'amount',
                'label' => 'Прибыль',
                'value' => function(Transfer $transfer) {
                    $amount = (float)$transfer->amount;
                    if ('-' == $transfer->event) {
                        $amount = - $amount;
                    }
                    return $amount;
                }
            ],
            [
                'attribute' => 'object_id',
                'label' => 'Действия',
                'value' => function(Transfer $transfer) {
                    if ('fix' == $transfer->event) {
                        return 'Зафиксировано';
                    }
                    if ('remainder' == $transfer->event) {
                        return 'Начисление остатка';
                    }
                    $object = $transfer->object;
                    if (!$object) {
                        return null;
                    }
                    if ('-' == $transfer->event) {
                        return 'Открытия ' . $object->type->name;
                    }
                    elseif ('+' == $transfer->event) {
                        return 'Начисления ' . $object->type->name;
                    }
                    return null;
                }
            ],
            $accountColumn,
            'time:datetime'
        ];
    }
    else {
        $columns = [
            'id',
            'sender_name',
            'receiver_name',
            'amount',
            'memo',
            'time:datetime'
        ];
    }
}
else {
    $columns = [
        [
            'attribute' => 'sender_name',
            'format' => 'html',
            'value' => function(Transfer $transfer) use ($mode) {
                return Html::a($transfer->sender_name, [
                    '/user/view', 'name' => $transfer->sender_name]);
            }
        ],
        [
            'attribute' => 'receiver_name',
            'format' => 'html',
            'value' => function(Transfer $transfer) use ($mode) {
                return Html::a($transfer->receiver_name, [
                    'index', 'user' => $transfer->receiver_name, 'mode' => $mode]);
            }
        ],
        'amount',
        'time:datetime',
        'ip',
        'memo'
    ];

    if ('income' == $mode) {
        array_pop($columns);
        $columns[] = [
            'attribute' => 'node_id',
            'label' => 'ID матрицы'
        ];
        $columns[] = [
            'attribute' => 'node_id',
            'label' => 'Пакет',
            'value' => function(Transfer $transfer) {
                return $transfer->node
                    ? $transfer->node->type->name : null;
            }
        ];
        $columns[] = [
            'attribute' => 'node_id',
            'label' => 'Уровень',
            'value' => function(Transfer $transfer) {
                return is_numeric($transfer->memo) ? (int) $transfer->memo : null;
            }
        ];
        $columns[] = $accountColumn;
    }
}

if (YII_DEBUG) {
    array_unshift($columns, 'id');
}

?>
<div class="transfer-index">
    <h2><?= $this->title ?></h2>
    <?php

    $buttons = [];
    if ('income' != $mode) {
        $buttons[] = Html::a('Перевести', ['create'], ['class' => 'btn btn-primary']);
    }
    if (Yii::$app->user->identity->isManager()) {
        $buttons[] = 'income' == $mode
            ? Html::a('Переводы', ['/invoice/transfer/index', 'user' => $user],
                ['class' => 'btn btn-default'])
            : Html::a('Прибыль', ['index', 'mode' => 'income', 'user' => $user],
                ['class' => 'btn btn-default']);
    }
    echo Html::tag('p', implode("\n", $buttons));
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'rowOptions' => function(Transfer $model, $index, $widget, $grid) {
            $class = $model->type;
            if ('-' == $model->event) {
                $class = 'debit';
            }
            elseif ('+' != $model->event) {
                $class = $model->event;
            }
            $options = [
                'id' => $model->id,
                'class' => $class
            ];
#            $info = $model->getInfo();
 #           if ($info) {
  #              foreach($info as $key => $value) {
   #                 $options['data-' . $key] = $value;
    #            }
     #       }
            if ($model->object_id) {
                $options['data-object_id'] = $model->object_id;
            }
            return $options;
        }
    ]); ?>
</div>
<?php
