<?php

namespace app\modules\matrix\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\User;
use app\modules\article\models\Article;
use app\modules\matrix\models\NodeGenerationForm;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class MatrixController extends Controller {
    public function behaviors() {
        return [
//            'verbs' => [
//                'class' => VerbFilter::class,
//                'actions' => [
//                    'open' => ['post'],
//                ],
//            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'data', 'plans', 'open', 'grapth'],
                'manager' => ['reinvest', 'nodes', 'reinvest']
            ]
        ];
    }

    public function actionIndex($id) {
        return $this->render('index', static::index($id));
    }

    public function actionData($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Node $node */
        $node = Node::findOne($id);
        $income = SQL::queryCell('SELECT amount FROM income WHERE root_id = :id', [
            ':id' => $id
        ]);

        if (!$node) {
            throw new NotFoundHttpException();
        }

        if (!Yii::$app->user->identity->isManager() && $node->user_name != Yii::$app->user->identity->name) {
            throw new ForbiddenHttpException();
        }

        $invests = [];

        $query = Matrix::find()->where([
            'root_id' => $id
        ]);
        $levels = SQL::queryAll('SELECT * FROM level_income WHERE root_id = :id', [
            ':id' => $id
        ]);
        foreach($levels as $level) {
            $invests[$level['level']] = [
                'income' => (float) $level['amount'],
                'count' => (int) $level['count'],
                'nodes' => []
            ];
        }
        foreach($query->all() as $row) {
            if ($row['level'] > 0 && isset($invests[$row['level']])) {
                $info = [
                    'number' => (int) $row['number'],
                    'user_name' => $row['user_name'],
                    'interest' => (float) $row['interest'],
                    'time' => strtotime($row['time'])
                ];
                if ($row['reinvest_from']) {
                    $info['reinvest_from'] = (int) $row['reinvest_from'];
                }
                $invests[$row['level']]['nodes'][$row['id']] = $info;
            }
        }
        return [
            'income' => (float) $income,
            'levels' => $invests
        ];
    }

    public static function index($id) {
        /** @var Node $node */
        $node = Node::findOne($id);

        if (!$node) {
            throw new NotFoundHttpException();
        }

        if (!Yii::$app->user->identity->isManager() && Yii::$app->user->identity->name != $node->user_name) {
            throw new ForbiddenHttpException();
        }

        return [
            'dataProvider' => new ActiveDataProvider([
                'query' => Matrix::find()->where(['root_id' => $id]),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ]),
            'types' => Type::all()
        ];
    }

    public function actionPlans($user) {
        $plans = SQL::queryAll('
            SELECT t.name, t.price, n.id, n.user_name, n.number, n.type_id, n.reinvest_from, i.amount
            FROM matrix_node n JOIN matrix_type t ON n.type_id = t.id
            JOIN income i ON n.id = i.root_id
            WHERE n.user_name = :user_name ORDER BY n.type_id, n.number, n.time ASC', [
            ':user_name' => $user
        ]);
        if (!Yii::$app->user->identity->isManager()) {
            $this->layout = '@app/views/layouts/cabinet';
        }
        return $this->render('plans', [
            'plans' => $plans,
            'types' => Type::all(),
            'marketing' => Yii::$app->view->render('@app/modules/article/views/article/view', [
                'model' => Article::find()->where(['name' => 'marketing'])->one()
            ]),
            'user_name' => $user
        ]);
    }

    public function actionOpen($id) {
        $id = (int) $id;
        /** @var Type $type */
        $type = Type::findOne($id);
        if (!$type) {
            throw new NotFoundHttpException();
        }
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->account >= $type->price) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($user->save(true, ['account']) && $type->open($user->name)) {
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Вы успешно открыли ' . $type->name);
                return $this->redirect(['/user/view',
                    'id' => $id
                ]);
            } else {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Something wrong happened'));
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
        }
        return $this->redirect(['/user/view']);
    }

    public function actionReinvest($type_id, $user_name) {
        $transaction = Yii::$app->db->beginTransaction();
        $type = Type::findOne($type_id);
        SQL::execute('UPDATE "user" set account = account + :price WHERE "name" = :name', [
            ':name' => $user_name,
            ':price' => $type->price
        ]);
        if ($type->open($user_name)) {
            $transaction->commit();
        }
        return $this->redirect(['plans']);
    }

    public function actionMarketing() {
        $plans = SQL::queryAll('SELECT * FROM matrix_type WHERE enabled ORDER BY id');
        return $this->render('marketing', [
            'plans' => $plans,
            'marketing' => Yii::$app->view->render('@app/modules/article/views/article/view', [
                'model' => Article::find()->where(['name' => 'marketing'])->one()
            ])
        ]);
    }

    public function actionNodes($mode = null) {
        $query = Node::find();
        switch($mode) {
            case 'reinvest':
                $query->where('reinvest_from is not null');
                break;
        }
        return $this->render('reinvest', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionGraph($id, $depth = 15) {
        /** @var Node $model */
        $model = Node::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $this->render('graph', [
            'model' => $model,
            'root' => $model->tree($depth),
            'depth' => $depth
        ]);
    }
}
