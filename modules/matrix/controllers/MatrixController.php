<?php

namespace app\modules\matrix\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\User;
use app\modules\article\models\Article;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\Type;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class MatrixController extends Controller {
    public function behaviors() {
        return [
//            'verbs' => [
//                'class' => VerbFilter::class,
//                'actions' => [
//                    'open' => ['post'],
//                ],
//            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'data', 'plans', 'open']
            ]
        ];
    }

    public function actionIndex($name, $user = null) {
        if ($name && empty(Type::$friendlyURLs[$name])) {
            throw new NotFoundHttpException();
        }

        if (!$user) {
            $user = Yii::$app->user->identity->name;
        }

        if (!Yii::$app->user->identity->isManager() && $user != Yii::$app->user->identity->name) {
            throw new ForbiddenHttpException();
        }

        return $this->render('index', static::index($user, Type::$friendlyURLs[$name]));
    }

    public function actionData($name, $user = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!$user) {
            $user = Yii::$app->user->identity->name;
        }

        if (!Yii::$app->user->identity->isManager() && $user != Yii::$app->user->identity->name) {
            throw new ForbiddenHttpException();
        }

        $json = [];

        $query = Matrix::find()->where([
            'type_id' => Type::$friendlyURLs[$name],
            'root' => $user
        ]);
        foreach($query->all() as $row) {
            $json[$row['level']][$row['id']] = [
                'number' => $row['number'],
                'user_name' => $row['user_name'],
                'interest' => $row['interest'],
                'time' => $row['time']
            ];
        }
        return $json;
    }

    public static function index($user = null, $type_id = false) {
        $query = Matrix::find();
        $vars = [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ]),
            'types' => Type::find()
                ->where(['enabled' => true])
                ->orderBy(['id' => SORT_ASC])
                ->all()
        ];

        if ($user) {
            $query->andWhere(['root' => $user]);
        }
        elseif (!Yii::$app->user->identity->isManager()) {
            $query->andWhere(['ref_name' => Yii::$app->user->identity->name]);
        }

        if ($type_id) {
            $query->andWhere(['type_id' => $type_id]);
            $vars['type_id'] = $type_id;
        }
        else {
            $vars['types'] = SQL::queryAll('SELECT id, "name" FROM matrix_type WHERE enabled ORDER BY id',
                null, PDO::FETCH_KEY_PAIR);
        }
        return $vars;
    }

    public function actionPlans($user) {
        $params = [':user_name' => $user];
        $result = SQL::queryAll('SELECT t.id
          FROM matrix_type t
          JOIN matrix_node n ON t.id = n.type_id
          WHERE n.user_name = :user_name
          GROUP BY t.id ORDER BY t.id', $params);

        $interests = SQL::queryAll('SELECT type_id, sum(interest)
          FROM matrix WHERE root = :user_name GROUP BY type_id', $params, PDO::FETCH_KEY_PAIR);
        $plans = [];
        foreach($result as $plan) {
            $plan['interest'] = isset($interests[$plan['id']]) ? $interests[$plan['id']] : 0;
            $plans[$plan['id']] = $plan;
        }
        return $this->render('plans', [
            'plans' => $plans,
            'marketing' => Yii::$app->view->render('@app/modules/article/views/article/view', [
                'model' => Article::find()->where(['name' => 'marketing'])->one()
            ])
        ]);
    }

    public function actionOpen($id) {
        $id = (int) $id;
        /** @var Type $type */
        $type = Type::findOne($id);
        if (!$type) {
            throw new NotFoundHttpException();
        }
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->account >= $type->price) {
            $transaction = Yii::$app->db->beginTransaction();
            $user->account -= $type->price;
            if ($user->save(true, ['account']) && $type->open($user->name)) {
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Вы успешно открыли ' . $type->name);
                return $this->redirect(['/matrix/matrix/index',
                    'name' => Type::findFriendlyURL($id),
                    'user' => $user->name
                ]);
            } else {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Something wrong happened'));
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
        }
        return $this->redirect(['/matrix/matrix/plans']);
    }

    public function actionMarketing() {
        $plans = SQL::queryAll('SELECT * FROM matrix_type WHERE enabled ORDER BY id');
        return $this->render('marketing', [
            'plans' => $plans,
            'marketing' => Yii::$app->view->render('@app/modules/article/views/article/view', [
                'model' => Article::find()->where(['name' => 'marketing'])->one()
            ])
        ]);
    }
}
