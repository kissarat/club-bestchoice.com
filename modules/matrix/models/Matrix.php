<?php

namespace app\modules\matrix\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer number
 * @property integer level
 * @property integer root_id
 * @property string user_name
 * @property number interest
 * @property string time
 */
class Matrix extends ActiveRecord {
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'level' => 'Уровень',
            'user_name' => 'Пользователь',
            'interest' => 'Прибыль',
            'time' => 'Время',
        ];
    }

    /**
     * @param $root_id
     * @param $level
     * @return ActiveQuery
     */
    public static function getLevel($root_id, $level) {
        return static::find()->where([
            'root_id' => $root_id,
            'level' => $level
        ]);
    }

    public function getChildren() {
        return $this->hasMany(Matrix::class, [
            'root_id' => $this->root_id,
            'parent_id' => $this->id
        ]);
    }
}
