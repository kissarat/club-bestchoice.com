<?php

namespace app\modules\matrix\models;


use yii\db\ActiveRecord;

class Matrix extends ActiveRecord {
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'level' => 'Уровень',
            'user_name' => 'Пользователь',
            'interest' => 'Прибыль',
            'time' => 'Время',
        ];
    }
}
