<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use app\models\User;
use yii\db\ActiveRecord;

/**
 * @property integer type_id
 * @property string user_name
 * @property string time
 *
 * @property Type type
 * @property User user
 */
class Node extends ActiveRecord {
    public static function tableName() {
        return 'matrix_node';
    }

    public function getType() {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }
}
