<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use app\models\User;
use PDO;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer type_id
 * @property integer parent_id
 * @property integer reinvest_from
 * @property string user_name
 * @property string time
 *
 * @property Type type
 * @property User user
 * @property Node parent
 * @property Node source
 * @property Node[] children
 */
class Node extends ActiveRecord {
    public static $_cache;
    public static function tableName() {
        return 'matrix_node';
    }

    public function getType() {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }

    public function getParent() {
        return $this->hasOne(Node::class, ['id' => 'parent_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChildren() {
        return $this->hasMany(Node::class, [
            'parent_id' => 'id'
        ]);
    }

    public function findFree() {
        $parent_id = SQL::queryCell('SELECT descend(:root_id)', [
            ':root_id' => $this->id
        ]);

        return $parent_id;

    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'parent_id' => 'Реферал',
            'type_id' => 'Пакет',
            'reinvest_from' => 'Реинфвестиция с',
            'user_name' => 'Пользователь',
            'time' => 'Время',
        ];
    }

    public function tree($depth) {
        $nodes = [];
        if ($depth > 0) {
            foreach ($this->getChildren()->orderBy(['time'=>SORT_ASC])->all() as $n) {
                $nodes[] = $n->tree($depth - 1);
            }
        }

        $this_node = [
            'name' => $this->getName()
        ];
        if (count($nodes) > 0) {
            $this_node['children'] = $nodes;
        }
        if ($this->reinvest_from) {
            $this_node['reinvest_from'] = $this->reinvest_from;
        }
        return $this_node;
    }

    public function __toString() {
        $out = $this->user_name;
        if ($this->number > 0) {
            $out .= '-' . $this->number;
        }
        return $out;
    }

    public function getName() {
        return $this->__toString() . ' (' . $this->id . ')';
    }

    public function getSource() {
        return $this->hasOne(static::class, ['id' => 'reinvest_from']);
    }

    public static function getCached($id) {
        if (isset(static::$_cache[$id])) {
            return static::$_cache[$id];
        }
        $node = static::findOne($id);
        if ($node) {
            static::$_cache[$id] = $node;
            return $node;
        }
        return null;
    }
}
