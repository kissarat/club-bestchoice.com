<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use app\models\User;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer type_id
 * @property number amount
 * @property string user_name
 *
 * @property Type type
 * @property Type reinvestType
 * @property User user
 */
class Reinvest extends ActiveRecord {
    public static function tableName() {
        return 'matrix_reinvest';
    }

    public function getType() {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    public function getReinvestType() {
        return $this->hasOne(Type::class, ['id' => 'reinvest_type_id']);
    }

    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }
}
