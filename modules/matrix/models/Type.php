<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use app\models\User;
use app\modules\invoice\models\Transfer;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\mssql\PDO;
use yii\db\Query;
use yii\web\NotFoundHttpException;


/**
 * @property integer id
 * @property number price
 * @property string name
 * @property boolean enabled
 */
class Type extends ActiveRecord
{
    private static $_all;
    const SILVER = 1;
    const GOLD = 2;
    const DIAMOND = 3;

    public static function tableName()
    {
        return 'matrix_type';
    }

    public static function all()
    {
        if (!static::$_all) {
            static::$_all = [];
            foreach (Type::find()
                         ->where(['enabled' => true])
                         ->orderBy(['id' => SORT_ASC])->all() as $type) {
                static::$_all[$type->id] = $type;
            }
        }
        return static::$_all;
    }

    /**
     * @param $id
     * @return Type
     */
    public static function get($id)
    {
        return static::all()[$id];
    }

    public static function enum() {
        $list = [];
        foreach(static::all() as $type) {
            $list[$type->id] = $type->name;
        }
        return $list;
    }

    /**
     * @param $user_name
     * @param Node $reinvest_from
     * @param Node $source_node
     * @return Node
     * @throws NotFoundHttpException
     */
    public function open($user_name, Node $reinvest_from = null, Node $source_node = null)
    {
        /** @var User $user */
        $user = User::findOne(['name' => $user_name]);
        /** @var Node $node */
        $node = null;
        if (static::SILVER == $this->id || static::GOLD == $this->id) {
            if ($user->ref_number > 0) {
                $node = Node::find()->where([
                    'type_id' => $this->id,
                    'user_name' => $user->ref_name,
                    'number' => $user->ref_number
                ])->one();
            }

            if (empty($node)) {
                $is_clone = $reinvest_from && $reinvest_from->type_id == $this->id;
                do {
                    $query = Node::find()
                        ->where(['type_id' => $this->id, 'user_name' => $user->name])
                        ->orderBy(['number' => $is_clone ? SORT_DESC : SORT_ASC]);
                    if ($is_clone) {
                        $query->where('number > 0');
                    }
                    $node = $query->one();
                    if ($node) {
                        break;
                    }
                }
                while($user = $user->referral);
            }
        }

        if (empty($node)) {
            $node = Node::find()->where(['type_id' => $this->id])
                ->orderBy(['id' => SORT_ASC]);
        }

        if ($node instanceof Query) {
            $node = $node->one();
        }
        $parent_id = $node->findFree();

        $id = SQL::queryCell('SELECT enter(:user_name, :type_id, :reinvest_from, :parent_id, :ip)', [
            ':user_name' => $user_name,
            ':type_id' => $this->id,
            ':ip' => $_SERVER['REMOTE_ADDR'],
            ':parent_id' => $parent_id,
            ':reinvest_from' => $reinvest_from ? $reinvest_from->id : null
        ]);

        $transfer_id = SQL::queryCell("SELECT id FROM transfer_journal
          WHERE \"type\" IN ('account', 'blocked') AND object_id = :id AND \"event\" = '-'", [
            ':id' => $id
        ]);
        if ($transfer_id) {
            $level = SQL::queryCell('SELECT level FROM matrix WHERE root_id = :root_id AND id = :id', [
                ':root_id' => $node->id,
                ':id' => $id
            ]);
            SQL::execute('UPDATE transfer SET memo = :memo, node_id = :node_id WHERE id = :transfer_id', [
                ':transfer_id' => $transfer_id,
                ':node_id' => $source_node ? $source_node->id : null,
                ':memo' => json_encode([
                    'level' => $level,
                    'id' => $node->id,
                    'type_id' => $node->type_id
                ])
            ]);
        }

        $reinvests = SQL::queryAll('SELECT * FROM reinvest', null, PDO::FETCH_OBJ);
        foreach($reinvests as $reinvest) {
            $count = SQL::queryCell('SELECT count(*) FROM matrix_node WHERE reinvest_from = :r AND type_id = :tid', [
                ':r' => $reinvest->id,
                ':tid' => $reinvest->type_id,
            ]);
            $type = Type::get($reinvest->type_id);
            SQL::execute('DELETE FROM reinvest WHERE id = :id', [
                ':id' => $reinvest->id
            ]);

            file_put_contents(Yii::getAlias('@app/log/clone.log'), "$reinvest->reinvest_from\t$reinvest->node_id\t$reinvest->type_id\n", FILE_APPEND);
            $reinvest_from = $reinvest->reinvest_from ? Node::findOne($reinvest->reinvest_from) : null;
            $source_node = $reinvest->node_id ? Node::findOne($reinvest->node_id) : null;
            $type->open($reinvest->user_name, $reinvest_from, $source_node);
        }

        return Node::findOne($id);
    }
}
