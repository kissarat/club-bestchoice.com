<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use yii\db\ActiveRecord;

/**
 * @property number price
 * @property string name
 * @property boolean enabled
 */
class Type extends ActiveRecord {
    public static $friendlyURLs = [
        'fast-start' => 1,
        'energy' => 2,
        'super-energy' => 3,
    ];

    public static function findFriendlyURL($id) {
        foreach(static::$friendlyURLs as $url => $val) {
            if ($id == $val) {
                return $url;
            }
        }
        return false;
    }

    public static function tableName() {
        return 'matrix_type';
    }

    /**
     * @param $user_name
     * @return \yii\db\ActiveQuery
     */
    public function open($user_name) {
        return SQL::execute('SELECT enter(:user_name, :type_id, :ip)', [
            ':user_name' => $user_name,
            ':type_id' => $this->id,
            ':ip' => $_SERVER['REMOTE_ADDR']
        ]);
    }
}
