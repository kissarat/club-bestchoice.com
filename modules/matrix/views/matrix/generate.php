<?php
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="matrix-generate">
    <h1>Генерация инвестиций</h1>
    <?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'first')->widget(AjaxComplete::class, [
        'route' => '/user/complete'
    ]);
    echo $form->field($model, 'count');
    echo $form->field($model, 'type_id')->dropDownList(Type::enum());
//    echo $form->field($model, 'reset')->checkbox();
    echo Html::submitButton('Сгенерировать');
    ActiveForm::end();

    /** @var Node[] $nodes */
    if ($nodes) {
        foreach($nodes as $node) {
            echo Html::tag('div', $node->user_name . ' ' . Html::a($node->id, ['/tree.php',
                    'id' => $node->id, 'depth' => 5]));
        }
    }
    ?>
</div>
