<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\matrix\models\Type $types */

use app\modules\matrix\models\Matrix;
use yii\grid\GridView;

$columns = ['id'];
if (empty($type_id)) {
    $columns[] = [
        'attribute' => 'type_id',
        'label' => 'Матрица',
        'value' => function(Matrix $model) use ($types) {
            return $types[$model->type_id];
        }
    ];
}
?>
<div class="matrix-index">
    <h1>Прибыль</h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => array_merge($columns, [
            'level',
            'user_name',
            'interest',
            'time:datetime',
        ])
    ]) ?>
</div>
