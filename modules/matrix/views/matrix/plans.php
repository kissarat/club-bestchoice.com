<?php
/** @var array $plans */
use app\models\User;
use app\modules\matrix\models\Type;
use yii\helpers\Html;

$this->title = 'Пакеты';
$me = !Yii::$app->user->getIsGuest() && isset($_GET['user']) && $_GET['user'] == Yii::$app->user->identity->name;
if ($me) {
    $this->params['breadcrumbs'][] = ['label' => $_GET['user'],
        'url' => ['/user/view', 'name' => $_GET['user']]];
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="matrix-plans">
    <?= $marketing ?>

    <?php
    if ($me) {
        echo Yii::$app->view->render('@app/views/user/panel', [
            'model' => User::findOne(['name' => $_GET['user']])
        ]);
    }
    ?>

    <table id="plans" class="table">
        <thead>
        <tr>
            <th>Пакет</th>
            <th>Стоимость</th>
            <th>Прибыль</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach(Type::find()->orderBy(['id' => SORT_ASC])->andWhere('enabled')->all() as $plan) {
            if (isset($plans[$plan->id])) {
                $p = $plans[$plan->id];
                echo Html::tag('tr', implode('', [
                    Html::tag('td', $plan->name),
                    Html::tag('td', $plan->price),
                    Html::tag('td', $p['interest']),
                    Html::tag('td', Html::button('Просмотр'))
                ]), ['data-name' => Type::findFriendlyURL($p['id'])]);
            }
            else {
                $columns = [
                    Html::tag('td', $plan->name),
                    Html::tag('td', $plan->price),
                    Html::tag('td', 0),
                ];
                if (Yii::$app->user->getIsGuest()) {
                    $columns[] = Html::tag('td', Html::a('Зарегистрироваться', ['/user/signup']));
                }
                else {
                    if (Yii::$app->user->identity->account >= $plan->price) {
                        $columns[] = Html::tag('td', Html::a('Открыть', ['/matrix/matrix/open',
                            'id' => $plan->id]));
                    }
                    else {
                        $columns[] = Html::tag('td', Html::a('Купить', ['/invoice/invoice/pay',
                            'id' => $plan->id]));
                    }
                }
                echo Html::tag('tr', implode('', $columns), [
                    'data-id' => $plan->id,
                    'data-name' => Type::findFriendlyURL($plan->id)
                ]);
            }
        }
        ?>
        </tbody>
    </table>

    <table id="plan" class="table" style="display: none">
        <thead>
        <tr>
            <th>Уровень</th>
            <th>Количество рефералов</th>
            <!--            <th>Прибыль со спонсора</th>-->
            <th>Прибыль</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <table id="members" class="table" style="display: none">
        <thead>
        <tr>
            <th>ID</th>
            <th>Реферал</th>
            <th>Прибыль</th>
            <th>Время</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
