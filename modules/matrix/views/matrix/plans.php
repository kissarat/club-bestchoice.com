<?php
/** @var array $plans */
use app\models\User;
use app\modules\matrix\models\Type;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Пакеты';
$me = !Yii::$app->user->getIsGuest() && isset($_GET['user']) && $_GET['user'] == Yii::$app->user->identity->name;
if ($me) {
    $this->params['breadcrumbs'][] = ['label' => $_GET['user'],
        'url' => ['/user/view', 'name' => $_GET['user']]];
    $this->params['breadcrumbs'][] = $this->title;
}

function row(array $cells, $options = []) {
    $row = [];
    foreach($cells as $cell) {
        $row[] = Html::tag('td', $cell);
    }
    return Html::tag('tr', implode("\t", $row), $options);
}
?>
<div class="matrix-plans">
    <?= $marketing ?>

    <table id="plans" class="table">
        <thead>
        <tr>
            <th>Пакет</th>
            <th>Реферальная ссылка</th>
            <th>Стоимость</th>
            <th>Прибыль</th>
            <th>Структура</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $rows = [];
        foreach($plans as $plan) {
            unset($types[$plan['type_id']]);
            $name = $plan['name'];
            if ($plan['reinvest_from']) {
                $name .= ' (клон)';
            }
            $amount = (float) $plan['amount'];
            $ref_name = $plan['user_name'];
            if (2 == $plan['type_id'] && $plan['number'] > 0) {
                $ref_name .=  '-' . $plan['number'];
            }
            $href = Url::to(['/user/signup', 'ref_name' => $ref_name], true);
            $href = str_replace('admin.', '', $href);
            $href = Html::a($href, $href);
            $columns = [$name, $href, $plan['price'], $amount,
                Html::a('Просмотр', ['graph', 'id' => $plan['id']]),
                $amount > 0 ? Html::button('Просмотр') : 'Нет прибыли'];
            $rows[] = row($columns, [
                'id' => $plan['id'],
                'data-type-id' => $plan['type_id']
            ]);
        }
        foreach($types as $id => $type) {
            $columns = [$type->name, '', $type->price, '', ''];
            if (Yii::$app->user->getIsGuest()) {
                $columns[] = Html::a('Зарегистрироваться', ['/user/signup']);
            }
            elseif (Yii::$app->user->identity->account >= $type['price']) {
                $columns[] = Html::a('Открыть', ['/matrix/matrix/open', 'id' => $type['id']], [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'user' => Yii::$app->user->identity->name
                        ]
                    ]
                ]);
            }
            elseif (Yii::$app->user->identity->isManager()) {
                $columns[] = Html::a('Открыть', ['/matrix/matrix/reinvest',
                    'type_id' => $type['id'], 'user_name' => $user_name]);
            }
            else {
                $columns[] = Html::a('Купить', ['/invoice/invoice/pay', 'id' => $type['id']]);
            }
            $rows[] = row($columns);
        }
        echo implode("\t", $rows);
        ?>
        </tbody>
    </table>

    <table id="plan" class="table" style="display: none">
        <thead>
        <tr>
            <th>Уровень</th>
            <th>Количество рефералов</th>
            <!--            <th>Прибыль со спонсора</th>-->
            <th>Прибыль</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <table id="members" class="table" style="display: none">
        <thead>
        <tr>
            <th>ID</th>
            <th>Реферал</th>
            <th>Прибыль</th>
            <th>Время</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-ru.js"></script>
</div>
