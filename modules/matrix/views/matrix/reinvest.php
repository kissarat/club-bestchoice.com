<?php
use app\modules\matrix\models\Node;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\data\ActiveDataProvider $dataProvider */
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function(Node $model) {
                return Html::a($model->getName(), ['/matrix/matrix/index', 'id' => $model->id]);
            }
        ],
        [
            'attribute' => 'parent_id',
            'format' => 'html',
            'value' => function(Node $model) {
                if ($model->parent_id) {
                    return Html::a($model->parent->getName(),
                        ['/matrix/matrix/index', 'id' => $model->id]);
                }
                return '';
            }
        ],
        [
            'attribute' => 'type_id',
            'value' => function(Node $model) {
                return $model->type->name;
            }
        ],
        [
            'attribute' => 'reinvest_from',
            'format' => 'html',
            'value' => function(Node $model) {
                if ($model->reinvest_from) {
                    return Html::a($model->source->getName(),
                        ['/matrix/matrix/index', 'id' => $model->reinvest_from]);
                }
                return '';
            }
        ],
        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function(Node $model) {
                return Html::a('Дерево', ['graph', 'id' => $model->id]);
            }
        ],
        'time:datetime'
    ]
]);
