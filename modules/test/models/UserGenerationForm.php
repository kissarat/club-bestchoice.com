<?php

namespace app\modules\test\models;


use yii\base\Model;

class UserGenerationForm extends Model {
    public $count;
    public $referrals;
    public $account;

    public function rules() {
        return [
            ['count', 'required'],
            ['count', 'default', 'value' => 100],
            ['account', 'default', 'value' => 0],
            ['referrals', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'count' => 'Количество',
            'referrals' => 'Рефералы',
            'account' => 'Баланс'
        ];
    }
}
