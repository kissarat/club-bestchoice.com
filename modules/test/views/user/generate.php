<?php
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="matrix-generate">
    <h1>Генерация пользователей</h1>
    <?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'count')->widget(AjaxComplete::class, [
        'route' => '/user/complete'
    ]);
    echo $form->field($model, 'referrals');
    echo $form->field($model, 'account');
    echo Html::submitButton('Сгенерировать');
    echo '<br/>';
    echo Html::a('Почистить базу', ['/test/user/delete-all']);
    echo '<br/>';
    echo '<br/>';
    ActiveForm::end();

    /** @var Node[] $nodes */
    if ($users) {
        foreach($users as $name => $ref_name) {
            echo Html::tag('div', Html::a($name, ['/user/view', 'name' => $name])
                . ' споснор ' . Html::a($ref_name, ['/user/view', 'name' => $ref_name]));
        }
    }
    ?>
</div>
