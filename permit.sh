#!/bin/bash

mkdir runtime
chmod 777 runtime
mkdir web/assets
chmod 777 web/assets
mkdir web/export
chmod 777 web/export
mkdir web/output
chmod 777 web/output
mkdir web/feedback
chmod 777 web/feedback

chown :www-data runtime
chown :www-data web/assets
chown :www-data web/export
chown :www-data web/feedback
