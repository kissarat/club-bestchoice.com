<?php
/**
 * @link http://zenothing.com/
 */
use app\helpers\SQL;

require_once __DIR__ . '/../config/boot.php';
$app = new \yii\console\Application($config);

$users = SQL::queryColumn('SELECT "name" FROM "user" WHERE ref_name IS NOT NULL ORDER BY id');

$transaction = Yii::$app->db->beginTransaction();
SQL::execute("insert into matrix_node(user_name, type_id) values ('admin', 2::SMALLINT, NULL, NULL)");
foreach($users as $user) {
    SQL::execute('SELECT enter(:name, 2::SMALLINT, NULL, NULL)', [
        ':name' => $user
    ]);
}
$transaction->commit();
