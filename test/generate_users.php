<?php
/**
 * @link http://zenothing.com/
*/
use app\helpers\SQL;
use yii\console\Application;

require_once __DIR__ . '/../config/boot.php';

$app = new Application($config);

$length = (int) $argv[1];
//$users = json_decode(file_get_contents('http://test.club-bestchoice.com/user/list'));
$transaction = Yii::$app->db->beginTransaction();
//$app->db->createCommand(
//    'INSERT INTO "user"("name", "email", "hash", "status")
//VALUES (\'admin\', \'admin@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 1)')->execute();
//foreach($users as $name => $ref_name) {
//    $app->db->createCommand('INSERT INTO "user"("name", email, ref_name) VALUES (:name, :email, :ref_name)', [
//        ':name' => $name,
//        ':email' => $name . '@yopmail.com',
//        ':ref_name' => $ref_name
//    ])->execute();
//}

SQL::execute('CLUSTER user_name ON "user"');
#SQL::execute("insert into matrix_node(user_name, type_id) values ('admin', 1::SMALLINT)");

$transaction->commit();
