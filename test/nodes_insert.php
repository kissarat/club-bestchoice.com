<?php
/**
 * @link http://zenothing.com/
 */
use app\helpers\SQL;
use yii\console\Application;

require_once __DIR__ . '/../config/boot.php';

$app = new Application($config);

$length = (int) $argv[1];
$type_id = (int) $argv[2];
$users = SQL::queryColumn('SELECT "name" FROM "user" ORDER BY RANDOM() LIMIT :length', [
    ':length' => $length
]);

$transaction = Yii::$app->db->beginTransaction();
define('PERIOD', 3600 * 24 * 3 * 30);
$time = time() - PERIOD;
function descend($parent_id = null) {
    global $users, $type_id, $time, $length, $transaction;
    $transaction->begin();
    if ($parent_id) {
        $parent = SQL::querySingle('SELECT * FROM matrix_node WHERE id = :id', [
            ':id' => $parent_id
        ]);
    }
    else {
        $parent = ['user_name' => array_pop($users)];
        $parent['id'] = SQL::queryCell('INSERT INTO matrix_node(user_name, type_id, time)
            VALUES (:user_name, :type_id, TIMESTAMP \'epoch\' + :time * INTERVAL \'1 second\') RETURNING id', [
            ':user_name' => $parent['user_name'],
            ':type_id' => $type_id,
            ':time' => $time,
        ]);
    }

    $i = 0;
    $children = [];
    while($user = array_pop($users)) {
        $time += rand(1, 1.5 * PERIOD / $length);
        SQL::execute('UPDATE "user" SET "ref_name" = :ref_name,
          "last_access" = (TIMESTAMP \'epoch\' + :last_access * INTERVAL \'1 second\') WHERE "name" = :name', [
            ':name' => $user,
            ':ref_name' => $parent['user_name'],
            ':last_access' => $time - rand(20, 3600)
        ]);
        $children[] = [
            'user_name' => $user,
            'id' => SQL::queryCell("INSERT INTO matrix_node(user_name, type_id, parent_id, \"time\")
            VALUES (:user_name, :type_id, :parent_id, TIMESTAMP 'epoch' + :time * INTERVAL '1 second') RETURNING id", [
                ':user_name' => $user,
                ':type_id' => $type_id,
                ':parent_id' => $parent['id'],
                ':time' => $time
            ])
        ];
        if (++$i >= 4) {
            break;
        }
    }

    foreach($children as $child) {
        descend($child['id']);
    }
    $transaction->commit();
}

descend();
$transaction->commit();
