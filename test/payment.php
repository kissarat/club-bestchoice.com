<?php
/**
 * @link http://zenothing.com/
*/

use app\models\Settings;
use app\models\User;
use yii\console\Application;

require_once __DIR__ . '/../config/boot.php';
require_once 'send.php';

$app = new Application($config);

$user = $argv[1];
$type_id = count($argv) > 2 ? (int) $argv[2] : false;

$user = User::findOne(['name' => $user]);
if (!$user) {
    die('User not found');
}

$id = uuid_create();
echo "http://admin.extravaganza.systems/invoice/invoice/view?id=$id\n";
$data = [
    'PAYMENT_ID' => $id,
    'PAYEE_ACCOUNT' => Settings::get('perfect', 'wallet'),
    'PAYMENT_AMOUNT' => 10,
    'PAYMENT_UNITS' => 'EUR',
    'PAYMENT_BATCH_NUM' => rand(1, 10000),
    'PAYER_ACCOUNT' => $user->wallet_perfect,
    'TIMESTAMPGMT' => time(),
    'USER_NAME' => $user->name
];
$data['V2_HASH'] = strtoupper(md5(implode(':', [
    $data['PAYMENT_ID'],
    $data['PAYEE_ACCOUNT'],
    $data['PAYMENT_AMOUNT'],
    $data['PAYMENT_UNITS'],
    $data['PAYMENT_BATCH_NUM'],
    $data['PAYER_ACCOUNT'],
    Yii::$app->perfect->hashAlternateSecret(),
    $data['TIMESTAMPGMT']
])));

$url = ['invoice/invoice/success', 'id' => $id];
if ($type_id) {
    $url['type_id'] = $type_id;
}
$response = send($url, $data);

file_put_contents(__DIR__ . "/../web/output/$id.html", $response);
echo "$id\n";
