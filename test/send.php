<?php

function send($url, $data, $headers = []) {
    if (is_array($url)) {
        $base = $url[0];
        if (false == strpos($base, 'http://')) {
            $base = 'http://test.club-bestchoice.com/' . $base;
        }
        unset($url[0]);
        if (!empty($url)) {
            $url = $base . '?' . http_build_query($url);
        }
    }
    echo $url . "\n";
    if (is_array($data)) {
        $data = http_build_query($data);
    }
    echo $data . "\n";
    $headers = array_merge([
        'Accept-Language' => 'ru,en-US;q=0.8,en;q=0.6',
        'Accept' => 'text/html',
//        'User-Agent' => 'wget/1.16',
        'Origin' => 'http://test.club-bestchoice.com',
        'Connection' => 'close',
        'Cache-Control' => 'max-age=0',
        'Content-Type' => 'application/x-www-form-urlencoded',
    ], $headers);
    $headers['Content-Length'] = strlen($data);
    $headers_array = [];
    foreach($headers as $key => $value) {
        $headers_array[] = "$key: $value";
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}
