<?php
$doc = new DOMDocument();
$envelop = $doc->createElementNS('http://schemas.xmlsoap.org/soap/envelope/', 'soapenv:Envelope');
$doc->appendChild($envelop);
$envelop->appendChild($doc->createElement('soapenv:Header'));
$method = $doc->createElementNS('http://wsm.advcash/', 'wsm:getBalances');
$envelop->appendChild($method);
echo $doc->saveXML();
