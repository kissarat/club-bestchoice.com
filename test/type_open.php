<?php

use app\helpers\SQL;
use app\modules\matrix\models\Type;
use yii\console\Application;

require_once __DIR__ . '/../config/boot.php';

$app = new Application($config);
//$transaction = $app->db->beginTransaction();
/** @var Type $type */
//$type1 = Type::findOne(1);
$type2 = Type::findOne(2);
/** @var string[] $users */
$users = SQL::queryColumn('SELECT "name" FROM user_referral ORDER BY id');
define('MAX_IP', pow(2, 30));
foreach($users as $user) {
    $_SERVER['REMOTE_ADDR'] = long2ip(rand(1, MAX_IP));
//    $type1->open($user);
    $type2->open($user);
}
//$transaction->commit();
