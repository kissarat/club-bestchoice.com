<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) ?>
    <?= Html::submitButton(Yii::t('app', 'Upload')) ?>
<?php
ActiveForm::end();

if ($result):
?>
    <br/>
<pre>
    <?= $result ?>
</pre>
<?php endif; ?>
