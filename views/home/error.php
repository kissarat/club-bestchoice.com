<?php
/**
 * @link http://zenothing.com/
*/

use yii\helpers\Html;

/* @var $exception \yii\console\Exception */

$this->title = $exception->getMessage();
?>
<div class="site-error">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
