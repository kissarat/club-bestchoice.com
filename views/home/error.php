<?php
/**
 * @link http://zenothing.com/
*/

use yii\helpers\Html;

/* @var $exception \yii\console\Exception */

?>
<div class="site-error">
    <?php if (\yii\helpers\Url::to('', true) != $_SERVER['HTTP_REFERER']): ?>
        <a href="<?= $_SERVER['HTTP_REFERER'] ?>">Вренуться назад</a>
    <?php endif ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <div><?= $exception->getMessage() ?></div>
</div>
