<div class="home-index">
    Если вы видите эту страницу, пожалуста сообщите об этом в поддержку
    <div class="error">
        <?php
        use yii\helpers\Html;

        if (!empty($data)) {
            if (is_array($data)) {
                echo Html::tag('pre', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            }
            else {
                print_r($data);
            }
        }
        ?>
    </div>
</div>
