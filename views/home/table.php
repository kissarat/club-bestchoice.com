<?php
use yii\helpers\Html;
?>
<div class="home-table">
    <?= Html::tag('h1', $this->title, ['class' => 'blue-border']); ?>
    <table class="table">
        <?php if (isset($headers)): ?>
        <thead>
        <tr>
            <?php
                /** @var array $headers */
                foreach ($headers as $header) {
                    echo Html::tag('th', $header);
                }
            ?>
        </tr>
        </thead>
        <?php endif ?>
        <tbody>
        <?php
        /** @var array $rows */
        foreach($rows as $row) {
            echo '<tr>';
            foreach($row as $cell) {
                echo Html::tag('td', $cell);
            }
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>
