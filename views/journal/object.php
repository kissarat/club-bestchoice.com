<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\models\Record;
use yii\grid\GridView;
use yii\helpers\Html;

?>
<div class="journal-object">
    <h1>История изменений</h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'event',
                'format' => 'html',
                'value' => function(Record $record) {
                    return Html::a(Yii::t('app', $record->event), ['/journal/view', 'id' => $record->id]);
                }
            ],
            'user_name',
            [
                'attribute' => 'data',
                'format' => 'html',
                'value' => function(Record $record) {
                    $view = $record->getView();
                    if ($view) {
                        return $view;
                    }
                    else {
                        return $record->data ? json_encode($record->info) : '';
                    }
                }
            ],
            [
                'attribute' => 'time',
                'format' => 'datetime',
                'contentOptions' => [
                    'class' => 'moment'
                ]
            ],
            'ip'
        ]
    ]) ?>
</div>
