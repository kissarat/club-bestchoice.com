<?php
use app\models\User;
use app\modules\matrix\models\Node;
use yii\helpers\Html;
use yii\helpers\Url;

if (empty($model)) {
    $model = isset($_GET['user']) ? User::findOne(['name' => $_GET['user']]) : Yii::$app->user->identity;
}

$items = [
    Html::a('Прибыль',  ['/user/view', 'name' => $model->name],   ['class' => 'btn btn-primary']),
    Html::a('Пакеты',  ['/matrix/matrix/plans', 'user' => $model->name],   ['class' => 'btn btn-primary']),
    Html::a('Платежи', ['/invoice/invoice/index', 'user' => $model->name], ['class' => 'btn btn-primary'])
];

if ($model->name == Yii::$app->user->identity->name && $model->hasSponsors()) {
    $items[] = Html::a('Переводы',
        ['/invoice/transfer/index', 'user' => $model->name],
        ['class' => 'btn btn-primary']);
}

if ($model->name == Yii::$app->user->identity->name || Yii::$app->user->identity->isAdmin()) {
    $items[] = Html::a(Yii::t('app', 'Change Password'),
        ['/user/password', 'name' => $model->name], ['class' => 'btn btn-warning']);
}

if (User::find()->where(['ref_name' => $model->name])->count() > 0) {
    $items[] = Html::a('Рефералы',
        ['/user/index', 'ref_name' => $model->name], ['class' => 'btn btn-primary']);
}

$items[] = Html::a('Выйти полностью',
    ['/user/clear'], ['class' => 'btn btn-danger']);

if (Yii::$app->user->identity->isAdmin()) {
    $items[] = Html::a(Yii::t('app', 'Journal'),
        ['/journal/index', 'user' => $model->name], ['class' => 'btn btn-primary']);
    $items[] = Html::a(Yii::t('app', 'Update'),
        ['/user/update', 'name' => $model->name], ['class' => 'btn btn-primary']);
    if (empty($model->hash)) {
        $items[] = Html::a(Yii::t('app', 'Activate'),
            ['/user/email', 'code' => $model->name], ['class' => 'btn btn-primary']);
    }
    $items[] = Html::a(Yii::t('app', 'Delete'), ['/user/delete', 'name' => $model->name
    ], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]);
}

$referral_info = '';
$user_info = Yii::$app->view->render('@app/views/user/view', [
    'model' => $model,
    'self' => true
]);
if ($model->ref_name) {
    $referral_info = Yii::$app->view->render('@app/views/user/view', [
        'model' => $model->referral
    ]);
}

$has_referral_url = Node::find()->where(['user_name' => $model->name])->count() > 0;

$this->beginContent('@app/views/layouts/main.php');
?>
<div id="cabinet">
    <!--<h1 class="title"><?= $this->title ?></h1>-->
    <h1 class="title">Личный кабинет</h1>
    <div class="row">
        <?= Html::tag('div', Html::ul($items, ['encode' => false]),
            ['id' => 'user-panel', 'class' => 'col-lg-2 menuleft']); ?>
        <div class="content col-lg-8">
            <?php
            if ($has_referral_url):
                $referral = Url::to(['/user/signup', 'ref_name' => $model->name], true);
                ?>
                <div id="referral_url">
                    <?= Html::a(Yii::t('app', 'Referral Link'), $referral, ['class' => 'form-label']); ?>

                    <div><?= Url::to($referral, true); ?></div>
                </div>
            <?php endif ?>

            <?= $content ?>
        </div>
        <?php if ($user_info): ?>
            <div class="right col-lg-2">
                <?= Yii::$app->view->render('@app/views/user/account', [
                    'model' => $model
                ]); ?>
                <div class="user-info-list">
                    <div>
                        <h4>Профиль</h4>
                        <?= $user_info ?>
                    </div>
                    <?php if ($model->ref_name): ?>
                        <div>
                            <h4>Cпонсор</h4>
                            <?= $referral_info ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="sponsors-count">Партнеров: <?= $model->countSponsors() ?></div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php
$this->endContent();
