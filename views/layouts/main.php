<?php

use app\helpers\MainAsset;
use app\models\Settings;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];
$body = $route;
if (rand(1, 2) == 2) {
    $body[] = 'background';
    $background = '';
}
else {
    $background = Html::tag('div', '', ['id' => 'background']);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    $this->head();
    if (!Yii::$app->user->getIsGuest()) {
        $user = Yii::$app->user->identity->name;
        echo Html::script("var user = '$user'");
    }
    ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $body) ?>">
<?= $background ?>
<?php $this->beginBody(); ?>

<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/">Главная</a>
                        </li>
                        <li>
                            <a href="/marketing.php">Маркетинг</a>
                        </li>
                        <li>
                            <a href="/documents.php">Документы</a>
                        </li>
                        <li>
                            <a href="/feedback/feedback/create">Поддержка</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (Yii::$app->user->getIsGuest()) {
                            echo Html::tag('li', Html::a(Yii::t('app', 'Signup'), ['/user/signup']));
                            echo Html::tag('li', Html::a(Yii::t('app', 'Login'), ['/user/login']));
                        } else {
                            echo Html::tag('li', Html::a(Yii::t('app', 'Profile'), ['/user/view']));
                            echo Html::tag('li', Html::a(Yii::t('app', 'Logout'), ['/user/logout']));
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

</div>



<a href="/feedback/feedback/create">
    <div id="feedback"></div>
</a>

<div id="image-view" style="display: none">
    <img src="/images/placeholder.png" alt="Image View" />
</div>

<div class="wrap <?= $login ?>">

    <div class="container">
        <?php if (!Yii::$app->user->getIsGuest()): ?>
            <!--<div class="cabinet">
                <?= Html::a(Html::img('/images/cabinet.png', ['alt' => 'Личний кабинет']), ['/user/view']) ?>
                <a href="/">Главная</a>
            </div>-->
        <?php endif ?>

        <?= Alert::widget() ?>

        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>

<?php if (Settings::get('common', 'google')): ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?= Settings::get('common', 'google') ?>', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
<?php endif ?>

<?php if (Settings::get('common', 'yandex')):
    $yandex = Settings::get('common', 'yandex');
    ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter<?= $yandex ?> = new Ya.Metrika({
                        id: <?= $yandex ?>,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });
            var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
                n.parentNode.insertBefore(s, n);
            };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");</script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/<?= $yandex ?>"
                  style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript><!-- /Yandex.Metrika counter -->
<?php endif ?>

<?php if (Settings::get('common', 'heart')): ?>
    <script>
        (function () {
            var widget_id = <?= Settings::get('common', 'heart') ?>;
            _shcp = [{widget_id: widget_id}];
            var lang = (navigator.language || navigator.systemLanguage
            || navigator.userLanguage || "en")
                .substr(0, 2).toLowerCase();
            var url = "widget.siteheart.com/widget/sh/" + widget_id + "/" + lang + "/widget.js";
            var hcc = document.createElement("script");
            hcc.type = "text/javascript";
            hcc.async = true;
            hcc.src = ("https:" == document.location.protocol ? "https" : "http")
                + "://" + url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
    </script>
<?php endif ?>
</body>
</html>
<?php $this->endPage() ?>
