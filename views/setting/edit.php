<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var \app\models\Settings $model */
$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/setting/index']];
echo Html::tag('h1', $this->title);
$form = ActiveForm::begin();
foreach($model->attributes() as $name) {
    $field = $form->field($model, $name);
    $type = $model->getType($name);
    switch($type) {
        case 'bool':
            echo $field->checkbox();
            break;
        default;
            echo $field->textInput();
            break;
    }
}
echo Html::submitButton(Yii::t('app', 'Save'));
ActiveForm::end();
