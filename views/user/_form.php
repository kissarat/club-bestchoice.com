<?php
/* @var $this yii\web\View */
/* @var $model app\models\User */

use app\models\Settings;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="user-edit">
<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'surname') ?>
<?= $form->field($model, 'forename') ?>
<?= $form->field($model, 'city') ?>
<?= $form->field($model, 'phone') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'skype') ?>
<?php
foreach($model->getAttributes() as $key => $value) {
    if (0 === strpos($key, 'wallet_')) {
        if (Settings::get('common', str_replace('wallet_', '', $key))) {
            echo $form->field($model, $key);
        }
    }
}
?>
<?php
if ('admin' == $model->getScenario()) {
    echo $form->field($model, 'account');
    echo $form->field($model, 'status')->dropDownList(User::statuses());
}
?>

<?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update')) ?>
<?php ActiveForm::end() ?>
</div>
