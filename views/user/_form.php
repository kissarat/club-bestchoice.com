<?php
/* @var $this yii\web\View */
/* @var $model app\models\User */

use app\models\Settings;
use app\models\User;
use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="user-edit">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'surname') ?>
<?= $form->field($model, 'forename') ?>
<?= $form->field($model, 'city') ?>
<?= $form->field($model, 'phone') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'skype') ?>
<?= $form->field($model, 'pin') ?>
<?= $form->field($model, 'image')->fileInput([
    'accept' => 'image/*',
    'class' => 'preview'
]) ?>
<div id="avatar" style="display: none">Невозмно загрузить аватар</div>
<?php
if ($model->avatar) {
    echo $form->field($model, 'delete_avatar')->checkbox();
}

foreach($model->getAttributes() as $key => $value) {
    if (0 === strpos($key, 'wallet_')) {
        if (Settings::get('common', str_replace('wallet_', '', $key))) {
            echo $form->field($model, $key);
        }
    }
}

if ('admin' == $model->getScenario()) {
    echo $form->field($model, 'account');
    echo $form->field($model, 'blocked');
    echo $form->field($model, 'status')->dropDownList(User::statuses());
    echo $form->field($model, 'ref_name')->widget(AjaxComplete::class, [
        'route' => '/user/complete'
    ]);
}
?>

<?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update')) ?>
<?php ActiveForm::end() ?>
</div>
