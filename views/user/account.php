<?php
/**
 * @link http://zenothing.com/
 */
use yii\helpers\Html;

/** @var app\models\User $model */
$account = Html::tag('span', (float)$model->account) . ' ';
if ($model->account > 0) {
    if ($model->hasSponsors()) {
        $account .= Html::a(Yii::t('app', 'Withdraw'),
            ['/invoice/invoice/create', 'amount' => (int)$model->account, 'scenario' => 'withdraw'],
            ['class' => 'btn btn-success btn-xs']);
    } else {
        $account .= Html::tag('strong', 'Вы сможете вывести деньги только если запросите 3-х рефералов');
    }
}
?>
<div class="account">
    <div> Баланс <?= $account ?></div>
    <div> Заблокировано <span><?= (float) $model->blocked ?></span></div>
</div>
