<?php
/**
 * @link http://zenothing.com/
*/

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::$app->user->isGuest ? 'Регистрация' : Yii::t('app', 'Create User');
?>
<h2 class="blue-border"><?= Html::encode($this->title) ?></h2>
<div class="user-create userform">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<img src="/images/shadow.png" />
