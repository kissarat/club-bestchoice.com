<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\SQL;
use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (isset($_GET['ref_name'])) {
    $this->title = 'Партнеры пользовател ' . $_GET['ref_name'];
}
else {
    $this->title = Yii::t('app', 'Users');
}

$columns = [
    [
        'attribute' => 'name',
        'format' => 'html',
        'value' => function(User $model) {
            return Html::a($model->name, ['view', 'name' => $model->name]);
        }
    ],
    'account',
    [
        'attribute' => 'last_access',
        'format' => 'datetime',
        'contentOptions' => [
            'class' => 'moment'
        ]
    ]
];

if (empty($_GET['ref_name'])) {
    $columns[] = [
        'attribute' => 'ref_name',
        'format' => 'html',
        'value' => function(User $model) {
            return $model->ref_name ? Html::a($model->ref_name, ['index', 'ref_name' => $model->ref_name]) : null;
        }
    ];
}

if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isAdmin()) {
    $columns[] = 'email:email';
}

$columns[] = [
    'format' => 'html',
    'contentOptions' => ['class' => 'action'],
    'value' => function(User $model) use ($sponsors) {
        $actions = [];
        if (Yii::$app->user->identity->isAdmin()) {
            $actions[] = Html::a('', ['update', 'name' => $model->name],
                ['class' => 'glyphicon glyphicon-pencil']);
        }
        if (isset($sponsors[$model->name])) {
            $actions[] = Html::a('Рефералов: ' . $sponsors[$model->name],
                ['/user/index', 'ref_name' => $model->name], ['class' => 'btn btn-primary btn-xs']);
        }
        return implode(' ', $actions);
    }
];

$buttons = [];
if (isset($_GET['ref_name'])) {
    $ref_name = SQL::queryCell('SELECT ref_name FROM "user" WHERE "name" = :name', [
        ':name' => $_GET['ref_name']
    ]);
    if ($ref_name) {
        $buttons[] = '<hr/>';
        $buttons[] = Html::a('Вверх', ['/user/index', 'ref_name' => $ref_name],
            ['class' => 'btn btn-primary']);
    }
}
if (Yii::$app->user->identity->isAdmin()) {
    $buttons[] = Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']);
}
?>
<div class="user-index">
    <div>
        <h1><?= Html::encode($this->title) ?></h1>
        <?php
        if (isset($_GET['ref_name']) && !Yii::$app->user->identity->isManager()) {
            $model = Yii::$app->user->identity;
            require 'panel.php';
        }

        if (count($buttons) > 0) {
            echo Html::tag('p', implode(' ', $buttons));
        }
        ?>

        <?php
        if (Yii::$app->user->identity->isManager()) {
            echo Html::tag('p', 'Баланс: €' . SQL::queryCell('SELECT sum(account) FROM "user"'));
        }
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns,
        ]); ?>
    </div>
</div>
