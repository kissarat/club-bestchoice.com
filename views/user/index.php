<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\SQL;
use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (isset($_GET['ref_name'])) {
    $this->title = 'Рефералы';
}
else {
    $this->title = Yii::t('app', 'Users');
}

$columns = [
    'id',
    Yii::$app->user->identity->isManager()
        ? [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function(User $model) {
                return Html::a($model->name, ['view', 'name' => $model->name]);
            }
        ]
        : 'name',
    'email',
    'skype',
    'forename',
    'surname',
    [
        'attribute' => 'last_access',
        'format' => 'datetime',
        'contentOptions' => [
            'class' => 'moment'
        ]
    ]
];

if (empty($_GET['ref_name'])) {
    $columns[] = [
        'attribute' => 'ref_name',
        'format' => 'html',
        'value' => function(User $model) {
            return $model->ref_name ? Html::a($model->ref_name, ['index', 'ref_name' => $model->ref_name]) : null;
        }
    ];
}

if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isAdmin()) {
    $columns[] = 'email:email';
}

$columns[] = [
    'format' => 'html',
    'contentOptions' => ['class' => 'action'],
    'value' => function(User $model) use ($sponsors) {
        $actions = [];
        if (Yii::$app->user->identity->isAdmin()) {
            $actions[] = Html::a('', ['update', 'name' => $model->name],
                ['class' => 'glyphicon glyphicon-pencil']);
        }
        if ($sponsors[$model->name] > 0) {
            $actions[] = Html::a('Рефералов: ' . $sponsors[$model->name],
                ['/user/index', 'ref_name' => $model->name], ['class' => 'btn btn-primary btn-xs']);
        }
        return implode(' ', $actions);
    }
];

$buttons = ['<hr/>'];
$referral_buttons = [];
if (isset($referrals)) {
    foreach($referrals as $level => $referral) {
        $referral_buttons[] = Html::a($referral, ['/user/index', 'ref_name' => $referral],
                ['class' => 'btn btn-primary']);
        if ($referral == $_GET['ref_name']) {
            break;
        }
    }
}

$options = [
    'dataProvider' => $dataProvider,
    'columns' => $columns,
];

if (Yii::$app->user->identity->isAdmin()) {
    $buttons[] = Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']);
    $buttons[] = Html::a('Выход всех пользователей', ['clear'], ['class' => 'btn btn-danger']);
    $options['filterModel'] = $searchModel;
}
?>
<div class="user-index">
    <div>
        <h2><?= Html::encode($this->title) ?></h2>
        <?php
        if (count($buttons) > 0) {
            echo Html::tag('p', implode(' ', $buttons));
        }

        $count = 1;
        foreach($referral_buttons as $button) {
            echo Html::tag('div', implode(' ', [
                Html::tag('strong', $count),
                $button
            ]));
            $count++;
        }
        ?>

        <?php
        if (Yii::$app->user->identity->isManager()) {
            echo Html::tag('p', 'Баланс: €' . SQL::queryCell('SELECT sum(account) FROM "user"'));
            echo Html::tag('p', 'Заблокировано: €' . SQL::queryCell('SELECT sum(blocked) FROM "user"'));
        }
        ?>

        <?= GridView::widget($options); ?>
    </div>
</div>
