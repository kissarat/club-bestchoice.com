<?php
/**
 * @link http://zenothing.com/
 */

use app\widgets\Ext;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
/* @var $form ActiveForm */

$this->title = 'Войти';
if ('admin' == Yii::$app->layout) {
    $this->title = Yii::t('app', 'Admin Panel') . ': ' . $this->title;
}
?>
<?= Ext::stamp() ?>

<h1><?= $this->title ?></h1>
<div class="user-login">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="form-group">
        <?= Html::activeCheckbox($model, 'remember') ?>
        <span id="duration" style="display: none">
            <?= Html::dropDownList('select', $model->duration, [
                60 => "час",
                60 * 24 => "день",
                60 * 24 * 7 => "неделя",
                60 * 24 * 30 => "месяц",
                0 => 'другое',
            ]) ?>
            <span id="duration_minutes" style="display: none">
                <?= Html::activeTextInput($model, 'duration') ?>
                минут
            </span>
        </span>

        <?= Html::a('Забыли пароль?', ['request'], ['class' => 'right']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Вход', ['class' => 'login']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div><!-- user-login -->
