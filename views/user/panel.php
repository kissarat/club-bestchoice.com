<?php
/** @var User $model */
use app\models\User;
use yii\helpers\Html;

if (!Yii::$app->user->getIsGuest() && !empty($model) && $model instanceof User):
    $items = [
        Html::a('Пакеты',  ['/matrix/matrix/plans', 'user' => $model->name],   ['class' => 'btn btn-primary']),
        Html::a('Платежи', ['/invoice/invoice/index', 'user' => $model->name], ['class' => 'btn btn-primary']),
        Html::a('Переводы',['/invoice/transfer/index', 'user' => $model->name],['class' => 'btn btn-primary']),
        Html::a(Yii::t('app', 'Journal'),
            ['/journal/index', 'user' => $model->name], ['class' => 'btn btn-primary']),
        Html::a(Yii::t('app', 'Update'),
            ['/user/update', 'name' => $model->name], ['class' => 'btn btn-primary'])
    ];

    if ($model->name == Yii::$app->user->identity->name || Yii::$app->user->identity->isAdmin()) {
        $items[] = Html::a(Yii::t('app', 'Change Password'),
            ['/user/password', 'name' => $model->name], ['class' => 'btn btn-warning']);
    }

    if (User::find()->where(['ref_name' => $model->name])->count() > 0) {
        $items[] = Html::a('Рефералы',
            ['/user/index', 'ref_name' => $model->name], ['class' => 'btn btn-primary']);
    }

    if (Yii::$app->user->identity->isAdmin()) {
        if (empty($model->hash)) {
            $items[] = Html::a(Yii::t('app', 'Activate'),
                ['/user/email', 'code' => $model->name], ['class' => 'btn btn-primary']);
        }
        $items[] = Html::a(Yii::t('app', 'Delete'), ['/user/delete', 'name' => $model->name
        ], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]);
    }

    echo Html::tag('div', Html::ul($items, ['encode' => false]), ['id' => 'user-panel']);
endif;
