<?php
/* @var $this yii\web\View */
/* @var \app\models\User $model */

use app\models\Settings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Форма регистрация';
?>
<div class="user-signup">


    <h1 class="title">Регистрация</h1>

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        Рекомендатель: <?= $model->ref_number > 0 ? ($model->ref_name . '-' . $model->ref_number) : $model->ref_name ?>
    </div>
    <?= Html::activeHiddenInput($model, 'ref_name') ?>

    <?= $form->field($model, 'name', [
        'template' => "{label}\n<div>{input}
<button type=\"button\" class=\"btn btn-default\">Проверить логин</button></div>\n{hint}\n{error}"
    ]) ?>
    <?= $form->field($model, 'surname') ?>
    <?= $form->field($model, 'forename') ?>
    <?= $form->field($model, 'city') ?>
    <?= $form->field($model, 'phone') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'skype') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'repeat')->passwordInput() ?>
    <?= $form->field($model, 'pin') ?>
    <?php
    foreach($model->getAttributes() as $key => $value) {
        if (0 === strpos($key, 'wallet_')) {
            if (Settings::get('common', str_replace('wallet_', '', $key))) {
                echo $form->field($model, $key);
            }
        }
    }
    ?>

    <div class="form-group">
        <label>
            <input type="checkbox" id="accept" />
            я принимаю
            <a href="/page/rules" target="_blank">пользовательское соглашение</a>
        </label>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
