<?php
/**
 * @link http://zenothing.com/
 */

use app\models\Settings;
use app\models\User;
use app\modules\bank\controllers\IncomeController;
use app\modules\invoice\controllers\TransferController;
use app\modules\matrix\controllers\MatrixController;
use app\modules\matrix\models\Node;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */


$this->title = $model->name;
if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$columns = [
    'name',
    'email:email',
    'last_access:datetime',
    [
        'attribute' => 'account',
        'format' => 'html',
        'value' => Html::tag('span', $model->account) . ' ' . ($model->account > 0 ? Html::a(Yii::t('app', 'Withdraw'),
                ['invoice/invoice/create', 'amount' => floor($model->account), 'scenario' => 'withdraw'],
                ['class' => 'btn btn-success btn-xs']) : '')
    ],
    'surname',
    'forename',
    'city',
    'phone',
    'skype',
    [
        'attribute' => 'ref_name',
        'format' => 'html',
        'value' => Html::a($model->ref_name, ['view', 'ref_name' => $model->ref_name])
    ]
];

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    $columns[] = [
        'attribute' => 'status',
        'value' => User::statuses()[$model->status]
    ];
}

foreach($model->getAttributes() as $key => $value) {
    if (0 === strpos($key, 'wallet_') && $model->$key) {
        $columns[] = $key;
    }
}
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php require 'panel.php'; ?>

    <div>
        <?php
        if (Node::find()->where(['user_name' => $model->name])->count() > 0):
            $referral = Url::to(['/user/signup', 'ref_name' => $model->name], true);
            ?>
            <div>
                <?php
                echo Html::a(Yii::t('app', 'Referral Link'), $referral, ['class' => 'form-label']);
                ?>

                <input class="form-control" value="<?= Url::to($referral, true); ?>">
            </div>
        <?php endif ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => $columns,
        ]) ?>
    </div>

    <?php
    echo Yii::$app->view->render('@app/modules/invoice/views/transfer/index',
        TransferController::index('income', $model->name));
    ?>
</div>
