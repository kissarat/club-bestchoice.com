<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var yii\web\View $this */
/* @var app\models\User $model */
/** @var app\models\User $user */


$user = Yii::$app->user->identity;
$me = $user->name == $model->name;

$this->beginBlock('info');
if (!Yii::$app->user->isGuest && $user->isManager()):
    $columns = [
        'email:email',
        'last_access:datetime',
        'surname',
        'forename',
        'city',
        'phone',
        'skype',
        [
            'attribute' => 'status',
            'value' => User::statuses()[$model->status]
        ]
    ];

    foreach ($model->getAttributes() as $key => $value) {
        if (0 === strpos($key, 'wallet_') && $model->$key) {
            $columns[] = $key;
        }
    }
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $columns,
    ]);

else:
    ?>

    <div>
        <h6>
            <?= $model->surname ?>
            <?= $model->forename ?>
        </h6>
    </div>
    <div>
        <?php
        if ($me) {
            echo Html::a('Редактировать профиль', ['/user/update', 'name' => $model->name]);
        }
        else {
            echo Html::tag('div', 'Скайп');
            echo Html::tag('div', $model->skype);
        }
        ?>
    </div>
    <?php
endif;
$this->endBlock();
$image_url = $model->avatar
    ? Yii::getAlias("@web/avatars/$model->name.$model->avatar")
    : Yii::getAlias("@web/img/ava.jpg");
?>

<div class="user-view">
    <h1><?= $model->name ?></h1>
    <div class="top">
        <?= Html::tag('div', '', [
            'class' => 'avatar',
            'style' => "background-image: url($image_url)"
        ]);
        ?>

        <?php
        if (isset($show_account)) {
            require_once 'account.php';
        } ?>
    </div>

    <?= Html::tag('div', $this->blocks['info'], ['class' => 'info']) ?>
</div>

