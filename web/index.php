<?php
/*
if (false === strpos($_SERVER['HTTP_HOST'], 'test')) {
    $ips = file_get_contents(__DIR__ . '/../config/hosts.allow');
    $ips = explode("\n", $ips);
    if (!in_array($_SERVER['REMOTE_ADDR'], $ips)) {
        header('Location: http://club-bestchoice.com/');
        exit;
    }
}
*/

$file = $_SERVER['REQUEST_URI'];
if ('/' == $file && false === strpos($_SERVER['HTTP_HOST'], 'admin')) {
        require_once 'front.php';
        exit;
}

if (0 === strpos($file, '/wp-')) {
    $content = file_get_contents('http://wordpress.org' . $file);
    foreach($http_response_header as $header) {
        header($header);
    }
    echo preg_replace('|<a([^>]+)https?://wordpress.org|', '<a$1http://' . $_SERVER['HTTP_HOST'], $content);
    exit;
}

define('CONFIG', __DIR__ . '/../config');

use yii\web\Application;

require_once CONFIG . '/boot.php';
require_once CONFIG . '/web.php';

$app = new Application($config);
$app->run();
