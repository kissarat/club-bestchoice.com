"use strict";
var start = Date.now();

function $$(selector) {
    return document.querySelector(selector)
}
function $id(id) {
    return document.getElementById(id)
}
function $all(selector) {
    return document.querySelectorAll(selector)
}
function $class(name) {
    return document.getElementsByClassName(name)
}
function $new(name) {
    return document.createElement(name)
}
function on(target, name, call) {
    if (!call) {
        call = name;
        name = 'click';
    }
    if ('string' == typeof target) {
        target = document.querySelectorAll(target);
        for (var i = 0; i < target.length; i++)
            target[i].addEventListener(name, call);
    }
    else
        target.addEventListener(name, call);
}

function $a(label, url) {
    var a = $new('a');
    a.setAttribute('href', url);
    a.innerHTML = label;
    return a;
}

function $row() {
    var row = $new('tr');
    console.log(arguments);
    for (var i = 0; i < arguments.length; i++) {
        var cell = arguments[i];
        if (cell instanceof HTMLTableCellElement) {
            row.appendChild(cell);
        }
        else if (cell instanceof Element) {
            row.insertCell(-1).appendChild(cell);
        }
        else if ('string' == typeof cell) {
            row.insertCell(-1).innerHTML = cell;
        }
        else {
            row.insertCell(-1).innerHTML = cell.toString();
        }
    }
    return row;
}

function each(array, call) {
    Array.prototype.forEach.call(array, call);
}

function array(a) {
    return Array.prototype.slice.call(a);
}

function $each(selector, call) {
    if ('string' == typeof selector) {
        selector = $all(selector);
    }
    return each(selector, call);
}

function options(select, call) {
    $each('option', call);
}

function load(sources, call, sequentially) {
    if ('string' == typeof sources) {
        sources = [sources];
    }
    var count = sources.length;
    var _call = call;
    if (sequentially) {
        call = load.bind(null, sources.slice(1), _call, true);
    }
    else if (window.Set && !(sources instanceof Set)) {
        sources = new Set(sources);
        $each('script', function (script) {
            if (sources.has(script.getAttribute('src'))) {
                sources.delete(script.getAttribute('src'));
            }
        });
    }
    sources.forEach(function (source) {
        var element = document.createElement('script');
        element.setAttribute('src', source);
        element.onload = function () {
            count--;
            if (count <= 0 && call) {
                if (sequentially && 1 == sources.length) {
                    if (_call) {
                        _call();
                    }
                }
                else {
                    if (call) {
                        call();
                    }
                }
            }
        };
        document.head.appendChild(element);
    });
}

var admin = 0 === location.hostname.indexOf('admin.');

//Feature detection
if (document.body.dataset) {
    $each('[data-selector]', function (source) {
        document.querySelector(source.dataset.selector).innerHTML = source.innerHTML;
        source.remove();
    })
}

Object.defineProperty(Element.prototype, 'visible', {
    get: function () {
        return 'none' != this.style.display
    },
    set: function (value) {
        if (value) {
            this.style.removeProperty('display');
        }
        else {
            this.style.display = 'none';
        }
    }
});

var $remember = $$('[name="LoginForm[remember]"]');

if ($remember) {
    $$('[name="LoginForm[duration]"]').setAttribute('type', 'number');
    $('[name="LoginForm[remember]"]').change(function () {
        duration.visible = !duration.visible;
    });

    $('.user-login [name=select]').change(function () {
        if (!(duration_minutes.visible = '0' == this.value)) {
            $$('[name="LoginForm[duration]"]').value = this.value;
        }
    });

    if (window.localStorage && localStorage.login) {
        $$('[name="LoginForm[name]"]').value = localStorage.login;
        $$('[name="LoginForm[password]"]').value = '1';
        if (localStorage.login_submit) {
            document.forms[0].submit();
        }
    }
}

function browser(string) {
    return navigator.userAgent.indexOf(string) >= 0;
}

var $footer = $$('.footer');
if ($footer && browser('Windows')) {
    $footer.remove();
}

function report(async) {
    var request = new XMLHttpRequest();
    var params = {spend: window.performance ? performance.now() : Date.now() - start};
    if (window.user) {
        params['user'] = user;
    }
    var url = '/visit.php?' + $.param(params);
    request.open('GET', url, async);
    request.send(null);
}

if ('classList' in HTMLElement.prototype) {
    var moments = $class('moment');
    if (moments.length > 0) {
        load(['/js/moment.js', '/js/moment-ru.js'], function () {
            moment.locale('ru-RU');
            each(moments, function (m) {
                var time = moment(m.textContent, 'DD-MM-YYYY hh:mm');
                var duration = moment.duration(-time.diff());
                if (duration.asHours() < 18) {
                    m.textContent = time.fromNow();
                    if (duration.asMinutes() < 15) {
                        m.classList.add('online');
                    }
                }
            });
        });
    }
}

if (admin) {
    $('._blank').each(function (i, a) {
        a.setAttribute('target', '_blank');
    });

    if ('false' != localStorage.reload) {
        var $d = $(document.body);
        if ($d.hasClass('index') && ($d.hasClass('user')
            || $d.hasClass('invoice') || $d.hasClass('journal')
            || $d.hasClass('income') || $d.hasClass('node'))
            || ($d.hasClass('admin') && $d.hasClass('visit'))) {
            setTimeout(function () {
                location.reload();
            }, 30000);
        }
    }
}
else {
    addEventListener('beforeunload', report);

    $each('[data-role="manage"]', function (element) {
        element.remove();
    });
}

var plan = $$('#plan tbody');
var members = $$('#members tbody');
var nodes = {};
var node_id;

function show_node(data) {
    plan.innerHTML = '';
    members.parentNode.style.display = 'none';
    for (var i in data) {
        i = parseInt(i);
        if (isNaN(i)) {
            continue;
        }
        var level = data[i];
        var plan_row = $row(i, level.count, level.income);
        plan_row.setAttribute('data-level', i);
        plan_row.addEventListener('click', function () {
            members.innerHTML = '';
            var level = nodes[node_id][this.getAttribute('data-level')].nodes;
            for (var id in level) {
                var member = level[id];
                if (member.reinvest_from) {
                    id += ' <span>реинвест ' + member.reinvest_from + '</span>';
                }
                members.appendChild($row(
                    id,
                    member.user_name,
                    member.interest,
                    new Date(member.time * 1000).toLocaleString()
                ));
            }
            members.parentNode.style.removeProperty('display');
        });
        plan.appendChild(plan_row);
    }
    plan.parentNode.style.removeProperty('display');
}

if (plan) {
    $each('#plans tr', function (plan) {
        var button = plan.querySelector('button');
        if (button) {
            button.addEventListener('click', function () {
                node_id = plan.getAttribute('id');
                if (node_id in nodes) {
                    show_node(nodes[node_id])
                }
                else {
                    $.getJSON('/matrix/matrix/data?id=' + node_id, function (data) {
                        show_node(data.levels);
                        nodes[node_id] = data.levels;
                    });
                }
            });
        }
    });
}

on('#amount', 'change', function () {
    var amount = this;
    $each('[name=PAYMENT_AMOUNT]', function (input) {
        input.value = amount.value;
    });
    $$('[name=ac_amount]').value = amount.value;
});

function pay(provider) {
    $id(provider).submit();
}

var user_name = $('.user-signup .field-user-name');
if (user_name.length > 0) {
    user_name.find('button').click(function () {
        var value = user_name.find('input').val().trim();
        if (value && user_name.hasClass('has-success')) {
            $.get('/user/exists?name=' + encodeURIComponent(value)).complete(function (xhr) {
                var exists = 302 == xhr.status;
                if (exists) {
                    user_name.addClass('has-error');
                    user_name.find('.help-block').html('Логин зарегистрирован');
                }
                else {
                    user_name.find('.help-block').html('Логин доступен');
                }
            });
        }
    });

    $('#user-wallet_perfect').on('keyup', function () {
        var value = this.value.trim();
        if (value && !/^E\d{7,8}$/.test(value)) {
            var first = value[0].toUpperCase();
            if (first < 'A' || first > 'Z') {
                var digits = [];
                for (var i = 0; i < value.length; i++) {
                    var digit = value[i];
                    if (digit >= '0' && digit <= '9') {
                        digits.push(digit);
                    }
                }
                this.value = 'E' + digits.join('');
            }
        }
    });

    $$('form').onsubmit = function (e) {
        if (!$id('accept').checked) {
            e.preventDefault();
            e.defaultPrevented = true;
            alert('Вы не прийняли пользовательское соглашение');
        }
    }
}

var ticket = $('#ticket');
ticket.find('button').click(function () {
    window.location = '/ticket/' + ticket.find('input').val();
});

$('#image-view')
    .click(function () {
        this.style.display = 'none';
    })
    .on('onkeyup', function () {
        this.style.display = 'none';
    });

$('.preview img').click(function () {
    var imageView = $$('#image-view img');
    imageView.setAttribute('src', this.getAttribute('src'));
    imageView.parentNode.style.removeProperty('display');
});

addEventListener('keypress', function (e) {
    if (10 == e.keyCode && e.ctrlKey) {
        document.forms[0].submit();
    }
});

$each('#interactive-search select', function (select) {
    select.addEventListener('change', function () {
        $id('interactive-search').submit();
    });
});

on('[type=file].preview', 'change', function () {
    var reader = new FileReader();
    var avatar = $id('avatar');
    avatar.style.removeProperty('display');
    avatar.setAttribute('class', 'avatar');
    avatar.innerHTML = 'Загрузка картинки...';
    reader.onload = function (e) {
        avatar.style.backgroundImage = 'url(' + e.target.result + ')';
        avatar.innerHTML = '';
    };
    reader.readAsDataURL(this.files[0]);
});

on('#advcash button', function() {
    var advcash = $id('advcash');
    $.get('/invoice/advcash/hash?id=' + advcash.ac_order_id.value + '&amount=' + advcash.ac_amount.value, function(hash) {
        advcash.ac_sign.value = hash;
        advcash.submit();
    });
});
