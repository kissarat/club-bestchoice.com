var start = Date.now();

function $$(selector) { return document.querySelector(selector) }
function $id(id) { return document.getElementById(id) }
function $all(selector) { return document.querySelectorAll(selector) }
function $class(name) { return document.getElementsByClassName(name) }
function $new(name) { return document.createElement(name) }
function $content(name, content) {
    var div = document.createElement(name);
    div.innerHTML = content;
    return div;
}
function $option(value, text) {
    var option = $new('option');
    option.value = value;
    option.innerHTML = text;
    return option;
}

function $a(label, url) {
    var a = $new('a');
    a.setAttribute('href', url);
    a.innerHTML = label;
    return a;
}

function $row() {
    var row = $new('tr');
    for(var i = 0; i < arguments.length; i++) {
        var cell = arguments[i];
        if (cell instanceof HTMLTableCellElement) {
            row.appendChild(cell);
        }
        else if (cell instanceof Element) {
            row.insertCell(-1).appendChild(cell);
        }
        else if ('string' == typeof cell) {
            row.insertCell(-1).innerHTML = cell;
        }
        else {
            row.insertCell(-1).innerHTML = cell.toString();
        }
    }
    return row;
}

function pg_time(string) {
    return moment(string, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY H:M');
}

function $name(model, name) {
    $$(model + '[' + name + ']');
}

function each(array, call) {
    Array.prototype.forEach.call(array, call);
}

function array(a) {
    return Array.prototype.slice.call(a);
}

function $each(selector, call) {
    if ('string' == typeof selector) {
        selector = $all(selector);
    }
    return each(selector, call);
}

function options(select, call) {
    $each('option', call);
}

function script(source) {
    var element = document.createElement('script');
    element.setAttribute('src', source);
    document.head.appendChild(element);
    return element;
}

var admin = 0 === location.hostname.indexOf('admin.');

//Feature detection
if (document.body.dataset) {
    $each('[data-selector]', function(source) {
        document.querySelector(source.dataset.selector).innerHTML = source.innerHTML;
        source.remove();
    })
}

Object.defineProperty(Element.prototype, 'visible', {
    get: function() {
        return 'none' != this.style.display
    },
    set: function(value) {
        if (value) {
            this.style.removeProperty('display');
        }
        else {
            this.style.display = 'none';
        }
    }
});

var $remember = $$('[name="LoginForm[remember]"]');

if ($remember) {
    $$('[name="LoginForm[duration]"]').setAttribute('type', 'number');
    $('[name="LoginForm[remember]"]').change(function () {
        duration.visible = !duration.visible;
    });

    $('.user-login [name=select]').change(function () {
        if (!(duration_minutes.visible = '0' == this.value)) {
            $$('[name="LoginForm[duration]"]').value = this.value;
        }
    });
}

function browser(string) {
    return navigator.userAgent.indexOf(string) >= 0;
}

var $footer = $$('.footer');
if ($footer && browser('Windows')) {
    $footer.remove();
}

function report(async) {
    var request = new XMLHttpRequest();
    var params = {spend: window.performance ? performance.now() : Date.now() - start};
    if (window.user) {
        params['user'] = user;
    }
    var url = '/visit.php?' + $.param(params);
    request.open('GET', url, async);
    request.send(null);
}

if ('classList' in HTMLElement.prototype) {
    var moments = $class('moment');
    if (moments.length > 0) {
        script('http://momentjs.com/downloads/moment-with-locales.js').onload = function() {
            moment.locale('ru-RU');
            each(moments, function(m) {
                var time = moment(m.textContent, 'DD-MM-YYYY hh:mm');
                var duration = moment.duration(- time.diff());
                if (duration.asHours() < 18) {
                    m.textContent = time.fromNow();
                    if (duration.asMinutes() < 15) {
                        m.classList.add('online');
                    }
                }
            });
        }
    }
}

if (admin) {
    $('._blank').each(function(i, a) {
        a.setAttribute('target', '_blank');
    });

    if ('false' != localStorage.reload) {
        var $d = $(document.body);
        if ($d.hasClass('index') && ($d.hasClass('user')
            || $d.hasClass('invoice') || $d.hasClass('journal')
            || $d.hasClass('income') || $d.hasClass('node'))
            || ($d.hasClass('admin') && $d.hasClass('visit'))) {
            setTimeout(function () {
                location.reload();
            }, 30000);
        }
    }
}
else {
    addEventListener('beforeunload', report);

    $each('[data-role="manage"]', function(element) {
        element.remove();
    });
}

var plan = $$('#plan tbody');
var members = $$('#members tbody');
var plans = {};
var plan_name;

function show_plan(data) {
    plan.innerHTML = '';
    members.parentNode.style.display = 'none';
    for (var i in data) {
        var level = data[i];
        var interest = 0;
        var count = 0;
        for(var id in level) {
            var member = level[id];
            interest += member.interest;
            count++;
        }
        var plan_row = $row(i, count, interest);
        plan_row.setAttribute('data-level', i);
        plan.appendChild(plan_row);
        plan_row.addEventListener('click', function() {
            members.innerHTML = '';
            var level = plans[plan_name][this.getAttribute('data-level')];
            for(var id in level) {
                var member = level[id];
                members.appendChild($row(
                    id,
                    member.user_name,
                    member.interest,
                    pg_time(member.time)
                ));
            }
            members.parentNode.style.removeProperty('display');
        });
    }
    plan.parentNode.style.removeProperty('display');
}

if (plan) {
    $each('#plans tr', function (plan) {
        var button = plan.querySelector('button');
        if (button) {
            button.addEventListener('click', function () {
                plan_name = plan.getAttribute('data-name');
                if (plan_name in plans) {
                    show_plan(plans[plan_name])
                }
                else {
                    $.getJSON('/matrix/matrix/data?name=' + plan_name, function (data) {
                        show_plan(data);
                        plans[plan_name] = data;
                    });
                }
            });
        }
    });
}

if (window.amount) {
    amount.addEventListener('change', function() {
        var value = Math.ceil(this.value);
        $$('[name=PAYMENT_AMOUNT]').value = value;
        this.value = value;
    });
}

function pay(provider) {
    $id(provider).submit();
}

var user_name = $('.user-signup .field-user-name');
if (user_name.length > 0) {
    user_name.find('button').click(function () {
        var value = user_name.find('input').val().trim();
        if (value && user_name.hasClass('has-success')) {
            $.get('/user/exists?name=' + encodeURIComponent(value)).complete(function (xhr) {
                var exists = 302 == xhr.status;
                if (exists) {
                    user_name.addClass('has-error');
                    user_name.find('.help-block').html('Логин зарегистрирован');
                }
                else {
                    user_name.find('.help-block').html('Логин доступен');
                }
            });
        }
    });

    $('#user-wallet_perfect').on('keyup', function() {
        var value = this.value.trim();
        if (value && !/^E\d{7,8}$/.test(value)) {
            var first = value[0].toUpperCase();
            if (first < 'A' || first > 'Z') {
                var digits = [];
                for (var i = 0; i < value.length; i++) {
                    var digit = value[i];
                    if (digit >= '0' && digit <= '9') {
                        digits.push(digit);
                    }
                }
                this.value = 'E' + digits.join('');
            }
        }
    });

    $$('form').onsubmit = function(e) {
        if (!$id('accept').checked) {
            e.preventDefault();
            e.defaultPrevented = true;
            alert('Вы не прийняли пользовательское соглашение');
        }
    }
}

var ticket = $('#ticket');
ticket.find('button').click(function() {
    window.location = '/ticket/' + ticket.find('input').val();
});

$('#image-view')
    .click(function() {
        this.style.display = 'none';
    })
    .on('onkeyup', function() {
        this.style.display = 'none';
    });

$('.preview img').click(function() {
    var imageView = $$('#image-view img');
    imageView.setAttribute('src',  this.getAttribute('src'));
    imageView.parentNode.style.removeProperty('display');
});
