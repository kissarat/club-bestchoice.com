<?php
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV_DEV') or define('YII_ENV_DEV', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

$start = microtime();
if (empty($_GET['spend'])
    || empty($_SERVER['HTTP_REFERER'])) {
    http_response_code(400);
    exit;
}
$ip = $_SERVER['REMOTE_ADDR'];
$host = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['HTTP_HOST'] . '/';
$path = $_SERVER['HTTP_REFERER'];
if (0 === strpos($path, $host)) {
    $path = str_replace($host, '', $path);
}
$spend = (int) $_GET['spend'];

header('Content-Type: application/json');
define('CONFIG', __DIR__ . '/../config');

require CONFIG . '/common.php';
require CONFIG . '/web.php';
require CONFIG . '/local.php';

$db = $config['components']['db'];
$pdo = new PDO($db['dsn'], $db['username'], $db['password'], [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]);

$st = $pdo->prepare('SELECT visit(:ip, :agent, :path, :spend, :data, :user_name)');
$st->execute([
    ':agent' => $_SERVER['HTTP_USER_AGENT'],
    ':ip' => $ip,
    ':path' => $path,
    ':spend' => floor($spend / 1000),
    ':data' => 'POST' == $_SERVER['REQUEST_METHOD'] ? file_get_contents('php://input') : null,
    ':user_name' => isset($_GET['user']) ? $_GET['user'] : null
]);

echo json_encode([
    'id' => $st->fetchColumn(),
    'spend' => microtime() - $start
]);
