<?php
/**
 * @link http://zenothing.com/
 */

namespace app\widgets;


use DateTime;
use Yii;
use yii\helpers\Html;

class Ext {
    public static function stamp() {
        if (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'bot') !== false) {
            return '';
        }

        return Html::tag('div', implode('', [
//            Html::a('Developed by zenothing.com', 'http://zenothing.com/'),
//            "\n\n :: zenothing.com ::",
            "\n\n ---------------------- user agent info ---------------------- \n\n  ",
            implode("\n  ", [
                implode(' ', [$_SERVER['REQUEST_METHOD'], $_SERVER['HTTP_HOST'], $_SERVER['REMOTE_ADDR']]),
                date(DateTime::RFC822, $_SERVER['REQUEST_TIME']),
                $_SERVER['HTTP_USER_AGENT']
            ]),
            "\n\n ---------------------- end user agent info ------------------ \n\n"
        ]), [
            'style' => 'display: none'
        ]);
    }

    public static function social() {
        $social_url = 'http://' . $_SERVER['HTTP_HOST'];
        return Html::tag('div', implode("\n", [
            Html::a(Html::img('/images/vkontakte.png', ['alt' => 'Вконтакте']),
                "https://vk.com/share.php?" . http_build_query(['url' => $social_url])),
            Html::a(Html::img('/images/facebook.png', ['alt' => 'Facebook']),
                'https://www.facebook.com/sharer.php?' . http_build_query([
                    's' => 100,
                    'p[url]' => $social_url
                ])),
            Html::a(Html::img('/images/twitter.png', ['alt' => 'Twitter']),
                'https://twitter.com/intent/tweet?' . http_build_query(['url' => $social_url])),
            Html::a(Html::img('/images/odnoklassniki.png', ['alt' => 'Одноклассники']),
                "http://www.addtoany.com/add_to/odnoklassniki?" . http_build_query([
                    'linkname' => Yii::$app->name,
                    'linkurl' => 'http://' . $_SERVER['HTTP_HOST']
                ])),
        ]),
            [
                'class' => 'social'
            ]);
    }
}
